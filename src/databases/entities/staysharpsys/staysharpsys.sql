BEGIN TRANSACTION;
DROP TABLE IF EXISTS `staysharpsys`;
CREATE TABLE IF NOT EXISTS `staysharpsys` (
	`params_id`	INTEGER,
	`schema_version`	TEXT NOT NULL,
	`schema_creation_date`	TEXT NOT NULL,
	PRIMARY KEY(`params_id`)
);
INSERT INTO `staysharpsys` (params_id,schema_version,schema_creation_date) VALUES (1,'0.3.0','2018-07-05');
COMMIT;
