PRAGMA encoding="UTF-8";
BEGIN TRANSACTION;
DROP TABLE IF EXISTS `workout_type_trans`;
CREATE TABLE IF NOT EXISTS `workout_type_trans` (
	`workout_type_trans_no`	INTEGER,
	`workout_type_no`	INTEGER NOT NULL,
	`long_code`	VARCHAR ( 5 ) NOT NULL,
	`short_code`	VARCHAR ( 2 ) NOT NULL,
	`name`	VARCHAR ( 100 ) NOT NULL,
	`user_defined`	INTEGER NOT NULL,
	PRIMARY KEY(`workout_type_trans_no`)
);
INSERT INTO `workout_type_trans` (workout_type_trans_no,workout_type_no,long_code,short_code,name,user_defined) VALUES (1,1,'EN_en','en','Bodybuilding',0),
 (2,2,'EN_en','en','Fitness',0),
 (3,3,'EN_en','en','Home cycling',0),
 (4,4,'EN_en','en','Martial art',0),
 (5,5,'EN_en','en','Running',0),
 (6,6,'EN_en','en','Swimming',0),
 (7,7,'EN_en','en','Walk',0),
 (8,8,'EN_en','en','Yoga',0),
 (9,1,'FR_fr','fr','Bodybuilding',0),
 (10,2,'FR_fr','fr','Fitness',0),
 (11,3,'FR_fr','fr','Vélo d''''intérieur',0),
 (12,4,'FR_fr','fr','Art martial',0),
 (13,5,'FR_fr','fr','Course à pied',0),
 (14,6,'FR_fr','fr','Natation',0),
 (15,7,'FR_fr','fr','Marche',0),
 (16,8,'FR_fr','fr','Yoga',0);
DROP TABLE IF EXISTS `workout_type`;
CREATE TABLE IF NOT EXISTS `workout_type` (
	`workout_type_no`	INTEGER,
	`name`	VARCHAR ( 100 ) NOT NULL,
	`user_defined`	INTEGER NOT NULL,
	PRIMARY KEY(`workout_type_no`)
);
COMMIT;
