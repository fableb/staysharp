BEGIN TRANSACTION;
DROP TABLE IF EXISTS `feeling_type_trans`;
CREATE TABLE IF NOT EXISTS `feeling_type_trans` (
	`feeling_type_trans_no`	INTEGER,
	`feeling_type_no`	INTEGER NOT NULL,
	`long_code`	VARCHAR ( 5 ) NOT NULL,
	`short_code`	VARCHAR ( 2 ) NOT NULL,
	`name`	VARCHAR ( 20 ) NOT NULL,
	PRIMARY KEY(`feeling_type_trans_no`)
);
INSERT INTO `feeling_type_trans` (feeling_type_trans_no,feeling_type_no,long_code,short_code,name) VALUES (1,1,'EN_en','en','Very bad'),
 (2,2,'EN_en','en','Bad'),
 (3,3,'EN_en','en','Neutral'),
 (4,4,'EN_en','en','Good'),
 (5,5,'EN_en','en','Very good'),
 (6,1,'FR_fr','fr','Très mauvais'),
 (7,2,'FR_fr','fr','Mauvais'),
 (8,3,'FR_fr','fr','Indifférent'),
 (9,4,'FR_fr','fr','Bien'),
 (10,5,'FR_fr','fr','Très bien');
DROP TABLE IF EXISTS `feeling_type`;
CREATE TABLE IF NOT EXISTS `feeling_type` (
	`feeling_type_no`	INTEGER,
	`name`	VARCHAR ( 20 ) NOT NULL,
	PRIMARY KEY(`feeling_type_no`)
);
COMMIT;
