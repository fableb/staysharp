BEGIN TRANSACTION;
DROP TABLE IF EXISTS `meal_type`;
CREATE TABLE IF NOT EXISTS `meal_type` (
	`meal_type_no`	INTEGER,
	`name`	VARCHAR(20),
	`name_fr`	VARCHAR(20),
	PRIMARY KEY(`meal_type_no`)
);
INSERT INTO `meal_type` (meal_type_no,name,name_fr) VALUES (0,'None','Aucun');
INSERT INTO `meal_type` (meal_type_no,name,name_fr) VALUES (1,'Breakfast','Petit déjeuner');
INSERT INTO `meal_type` (meal_type_no,name,name_fr) VALUES (2,'Lunch','Déjeuner');
INSERT INTO `meal_type` (meal_type_no,name,name_fr) VALUES (3,'Dinner','Diner');
INSERT INTO `meal_type` (meal_type_no,name,name_fr) VALUES (4,'Snack','En-cas');
COMMIT;
