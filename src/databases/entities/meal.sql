BEGIN TRANSACTION;
DROP TABLE IF EXISTS `meal`;
CREATE TABLE IF NOT EXISTS `meal` (
	`meal_no`	INTEGER,
	`account_no`	INTEGER,
	`meal_type_no`	INTEGER,
	`creation_date`	VARCHAR (10),
	`creation_time`	VARCHAR (8),
	PRIMARY KEY(`meal_no`),
	FOREIGN KEY(`meal_type_no`) REFERENCES `meal_type`(`meal_type_no`)
);
COMMIT;
