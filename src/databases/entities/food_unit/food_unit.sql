PRAGMA encoding="UTF-8";
BEGIN TRANSACTION;
DROP TABLE IF EXISTS `food_unit_trans`;
CREATE TABLE IF NOT EXISTS `food_unit_trans` (
	`food_unit_trans_no`	INTEGER,
	`foo_unit_no`	INTEGER NOT NULL,
	`long_code`	TEXT NOT NULL,
	`short_code`	TEXT NOT NULL,
	`name`	VARCHAR ( 50 ) NOT NULL,
	PRIMARY KEY(`food_unit_trans_no`)
);
INSERT INTO `food_unit_trans` (food_unit_trans_no,foo_unit_no,long_code,short_code,name) VALUES 
 (1,1,'EN_en','en','Grams (g)'),
 (2,2,'EN_en','en','Milligrams (mg)'),
 (3,1,'FR_fr','fr','Grammes (g)'),
 (4,2,'FR_fr','fr','Milligrammes (mg)'),
 (5,3,'EN_en','en','Teaspoon (ts)'),
 (6,3,'FR_fr','fr','Cuillère à café (cf)'),
 (7,4,'EN_en','en','Tablespoon (tas)'),
 (8,5,'EN_en','en','Tablet (tab)'),
 (9,4,'FR_fr','fr','Cuillère à soupe (cs)'),
 (10,5,'FR_fr','fr','Comprimé (comp)');
DROP TABLE IF EXISTS `food_unit`;
CREATE TABLE IF NOT EXISTS `food_unit` (
	`food_unit_no`	INTEGER,
	`name`	VARCHAR ( 50 ) NOT NULL,
	PRIMARY KEY(`food_unit_no`)
);
COMMIT;
