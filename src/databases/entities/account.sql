BEGIN TRANSACTION;
DROP TABLE IF EXISTS `account`;
CREATE TABLE IF NOT EXISTS `account` (
	`account_no`	INTEGER,
	`name`	VARCHAR ( 20 ) NOT NULL,
	`creation_date`	VARCHAR ( 10 ) NOT NULL,
	`first_name`	VARCHAR ( 20 ),
	`last_name`	VARCHAR ( 20 ),
	`birth_date`	VARCHAR ( 10 ),
	`gender`	INTEGER,
	`height`	INTEGER,
	`weight`	REAL,
	CONSTRAINT `ct_name_unique` UNIQUE(`name`),
	PRIMARY KEY(`account_no`)
);
INSERT INTO `account` (account_no,name,creation_date,first_name,last_name,birth_date,gender,height,weight) VALUES (1,'fabrice','2018-06-21','Fabrice','LEBEL','1969-03-04',2,181,87.5),
 (2,'john','2018-06-23','John','DOE','1984-05-22',2,178,75.0),
 (3,'jane','2018-07-04','Jane','DOE','1984-08-02',1,165,55.0);
COMMIT;
