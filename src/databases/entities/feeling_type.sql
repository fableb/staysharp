BEGIN TRANSACTION;
DROP TABLE IF EXISTS `feeling_type`;
CREATE TABLE IF NOT EXISTS `feeling_type` (
	`feeling_type_no`	INTEGER,
	`name` VARCHAR( 20 ) NOT NULL,
	`name_fr` VARCHAR( 20 ) NOT NULL,
	PRIMARY KEY(`feeling_type_no`)
);
INSERT INTO `feeling_type` (feeling_type_no,name,name_fr) VALUES (0,'None','Aucun');
INSERT INTO `feeling_type` (feeling_type_no,name,name_fr) VALUES (1,'Very bad','Très mauvais');
INSERT INTO `feeling_type` (feeling_type_no,name,name_fr) VALUES (2,'Bad','Mauvais');
INSERT INTO `feeling_type` (feeling_type_no,name,name_fr) VALUES (3,'Neutral','Neutre');
INSERT INTO `feeling_type` (feeling_type_no,name,name_fr) VALUES (4,'Good','Bien');
INSERT INTO `feeling_type` (feeling_type_no,name,name_fr) VALUES (5,'Very good','Très bien');
COMMIT;
