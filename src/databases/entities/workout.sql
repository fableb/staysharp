BEGIN TRANSACTION;
DROP TABLE IF EXISTS `workout`;
CREATE TABLE IF NOT EXISTS `workout` (
	`workout_no`	INTEGER,
	`account_no`	INTEGER NOT NULL,
	`workout_type`	INTEGER NOT NULL,
	`creation_date`	VARCHAR ( 10 ) NOT NULL,
	`start_time`	VARCHAR ( 8 ) NOT NULL,
	`duration`	VARCHAR ( 8 ) NOT NULL,
	`min_cf`	INTEGER,
	`mean_cf`	INTEGER,
	`max_cf`	INTEGER,
	`calories`	INTEGER,
	`feeling_type`	INTEGER NOT NULL,
	`note`	VARCHAR ( 1000 ),
	PRIMARY KEY(`workout_no`),
	FOREIGN KEY(`workout_type`) REFERENCES `workout_type`(`type_no`),
	FOREIGN KEY(`account_no`) REFERENCES `account`(`account_no`),
	FOREIGN KEY(`feeling_type`) REFERENCES `feeling_type`(`feeling_type_no`)
);
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (1,2,2,'2018-06-20','07:30:00','00:40:00',0,0,0,350,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (2,2,3,'2018-06-20','19:00:00','00:01:00',0,0,0,475,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (3,2,3,'2018-06-19','07:30:00','00:30:00',0,0,0,325,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (4,2,3,'2018-06-21','07:30:00','00:30:00',0,0,0,275,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (5,2,2,'2018-06-21','17:00:00','00:45:00',0,0,0,350,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (6,2,3,'2018-06-21','21:00:00','00:30:00',0,0,0,275,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (7,1,2,'2018-06-03','19:31:00','00:30:36',85,121,144,299,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (8,1,3,'2018-06-04','19:52:00','00:28:28',89,119,136,269,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (9,1,2,'2018-06-05','20:08:00','00:29:49',81,125,155,308,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (10,1,1,'2018-06-12','20:25:00','01:09:57',84,107,139,527,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (11,1,2,'2018-06-15','19:01:00','00:20:02',91,117,138,183,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (12,1,3,'2018-06-16','09:45:00','00:27:41',68,113,129,236,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (13,1,3,'2018-06-17','17:06:00','00:48:37',74,120,133,473,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (14,1,3,'2018-06-18','15:12:00','00:45:42',81,127,140,490,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (15,1,2,'2018-06-18','19:31:00','00:21:36',72,131,153,245,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (16,3,3,'2018-06-21','14:56:08','00:50:00',0,0,0,625,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (17,1,1,'2018-06-16','18:42:00','01:21:14',80,106,129,585,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (18,1,3,'2018-06-21','17:06:00','01:21:42',95,112,123,697,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (19,1,3,'2018-06-25','18:31:00','00:45:04',0,118,130,423,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (20,1,3,'2018-06-27','16:00:00','00:23:42',0,116,130,217,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (21,1,3,'2018-07-03','19:03:48','00:35:00',86,125,140,287,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (22,1,2,'2018-07-03','09:10:36','00:25:00',94,136,145,350,0,'');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (23,1,1,'2018-07-04','15:05:59','00:45:00',85,125,145,350,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pharetra vel turpis nunc eget lorem dolor sed viverra. Enim nunc faucibus a pellentesque sit amet. Tristique nulla aliquet enim tortor at auctor. Lectus nulla at volutpat diam ut venenatis tellus in. Eget arcu dictum varius duis at. Sit amet nisl suscipit adipiscing bibendum. Platea dictumst quisque sagittis purus sit amet volutpat. Fermentum odio eu feugiat pretium nibh. C');
INSERT INTO `workout` (workout_no,account_no,workout_type,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type,note) VALUES (24,1,3,'2018-07-04','15:56:05','00:25:00',0,0,0,0,5,'');
COMMIT;
