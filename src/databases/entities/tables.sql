/* SQLie Date Formats
TEXT as ISO8601 strings ("YYYY-MM-DD HH:MM:SS.SSS").
REAL as Julian day numbers, the number of days since noon in Greenwich on November 24, 4714 B.C. according to the proleptic Gregorian calendar.
INTEGER as Unix Time, the number of seconds since 1970-01-01 00:00:00 UTC.
*/

/* ************************************************************************** */
/* Account table                                                              */
/* ************************************************************************** */
DROP TABLE IF EXISTS account;
create table account (
    account_no INTEGER PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    creation_date VARCHAR(10) NOT NULL,
    first_name VARCHAR(20),
    last_name VARCHAR(20),
    birth_date VARCHAR(10),
    gender INTEGER,
    height INTEGER,
    weight REAL,
    CONSTRAINT ct_name_unique UNIQUE (name)
);

INSERT INTO account (account_no, name, creation_date, first_name, last_name, birth_date)
    VALUES(NULL , 'fabrice', date('2018-06-19'), 'Fabrice', 'LEBEL', '1969-03-04');
INSERT INTO account (account_no, name, creation_date, first_name, last_name, birth_date)
    VALUES(NULL , 'john', date('2018-06-19'), 'John', 'DOE', '1982-11-22');
INSERT INTO account (account_no, name, creation_date, first_name, last_name, birth_date)
    VALUES(NULL , 'jane', date('2018-06-19'), 'Jane', 'DOE', '1984-05-19');

/* ************************************************************************** */
/* Workout types table                                                        */
/* ************************************************************************** */
DROP TABLE IF EXISTS workout_type;
create table workout_type (
    type_no INTEGER PRIMARY KEY,
    name VARCHAR(20)
);

INSERT INTO workout_type (type_no, name) VALUES (1, 'Bodybuilding');
INSERT INTO workout_type (type_no, name) VALUES (2, 'Fitness');
INSERT INTO workout_type (type_no, name) VALUES (3, 'Home cycling');
INSERT INTO workout_type (type_no, name) VALUES (4, 'Martial art');
INSERT INTO workout_type (type_no, name) VALUES (5, 'Running');
INSERT INTO workout_type (type_no, name) VALUES (6, 'Swimming');
INSERT INTO workout_type (type_no, name) VALUES (7, 'Walk');
INSERT INTO workout_type (type_no, name) VALUES (8, 'Yoga');
INSERT INTO workout_type (type_no, name) VALUES (-999, 'Dummy workout 1');
INSERT INTO workout_type (type_no, name) VALUES (-998, 'Dummy workout 2');
INSERT INTO workout_type (type_no, name) VALUES (-997, 'Dummy workout 3');

/* ************************************************************************** */
/* Workout table                                                              */
/* ************************************************************************** */
DROP TABLE IF EXISTS workout;
CREATE TABLE workout (
    workout_no INTEGER PRIMARY KEY,
    account_no INTEGER NOT NULL,
    workout_type INTEGER NOT NULL,
    creation_date VARCHAR(10) NOT NULL,
    start_time VARCHAR(8) NOT NULL,
    duration VARCHAR(8) NOT NULL,
    min_cf INTEGER,
    mean_cf INTEGER,
    max_cf INTEGER,
    calories INTEGER,
    FOREIGN KEY(workout_type) REFERENCES workout_type(type_no),
    FOREIGN KEY(account_no) REFERENCES account(account_no)
);

INSERT INTO workout (workout_no, account_no, workout_type, creation_date, start_time, duration, min_cf, mean_cf, max_cf, calories)
    VALUES(NULL , 2, 1, date('2018-06-18'), time('17:05:00'), time('00:55:00'), 105, 125, 135, 450);
INSERT INTO workout (workout_no, account_no, workout_type, creation_date, start_time, duration, min_cf, mean_cf, max_cf, calories)
    VALUES(NULL, 2, 2, date('2018-06-19'), time('17:15:00'), time('01:05:00'), 95, 110, 135, 500);
INSERT INTO workout (workout_no, account_no, workout_type, creation_date, start_time, duration, min_cf, mean_cf, max_cf, calories)
    VALUES(NULL, 2, 3, date('2018-06-19'), time('17:00:00'), time('00:43:00'), 100, 112, 140, 480);

/* Calcul du total des calories des workouts pour un compte donné et un jour donné */
select sum(calories) from workout where account_no=2 and creation_date='2018-06-21';


/* ************************************************************************** */
/* Personal data table                                                        */
/* ************************************************************************** */
DROP TABLE IF EXISTS personal_data;
CREATE TABLE personal_data (
    personal_data_no INTEGER PRIMARY KEY,
    account_no INTEGER NOT NULL,
    height INTEGER,
    weight REAL,
    FOREIGN KEY(account_no) REFERENCES account(account_no)
);

/* ************************************************************************** */
/* Meal table                                                                 */
/* ************************************************************************** */
DROP TABLE IF EXISTS meal;
CREATE TABLE meal (
    meal_no INTEGER PRIMARY KEY,
    account_no INTEGER NOT NULL,
    creation_date VARCHAR(10) NOT NULL,
    creation_time VARCHAR(8) NOT NULL,
    FOREIGN KEY(account_no) REFERENCES account(account_no)
);

/* ************************************************************************** */
/* Meal items table                                                           */
/* ************************************************************************** */
DROP TABLE IF EXISTS meal_item;
CREATE TABLE meal_item (
    meal_item_no INTEGER PRIMARY KEY,
    meal_no INTEGER NOT NULL,
    account_no INTEGER NOT NULL,
    FOREIGN KEY(meal_no) REFERENCES meal(meal_no)
    FOREIGN KEY(account_no) REFERENCES account(account_no)
);

