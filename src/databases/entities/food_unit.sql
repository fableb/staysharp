BEGIN TRANSACTION;
DROP TABLE IF EXISTS `food_unit`;
CREATE TABLE IF NOT EXISTS `food_unit` (
	`food_unit_no`	INTEGER,
	`name`	VARCHAR(50),
	`name_fr`	VARCHAR(50),
	PRIMARY KEY(`food_unit_no`)
);
INSERT INTO `food_unit` (food_unit_no,name,name_fr) VALUES (1,'Grams (g)','Grammes (g)');
INSERT INTO `food_unit` (food_unit_no,name,name_fr) VALUES (2,'Milligrams (mg)','Milligrammes (mg)');
INSERT INTO `food_unit` (food_unit_no,name,name_fr) VALUES (3,'Teaspoon','Cuillère à café');
INSERT INTO `food_unit` (food_unit_no,name,name_fr) VALUES (4,'Tablespoon','Cuillère à soupe');
INSERT INTO `food_unit` (food_unit_no,name,name_fr) VALUES (5,'Tablet','Comprimé');
COMMIT;
