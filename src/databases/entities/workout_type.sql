BEGIN TRANSACTION;
DROP TABLE IF EXISTS `workout_type`;
CREATE TABLE IF NOT EXISTS `workout_type` (
	`type_no` INTEGER,
	`name` VARCHAR ( 20 ),
	`name_fr` VARCHAR ( 20 ),
	PRIMARY KEY(`type_no`)
);
INSERT INTO `workout_type` (type_no,name,name_fr) VALUES (0,'None','Aucun');
INSERT INTO `workout_type` (type_no,name,name_fr) VALUES (1,'Bodybuilding','Bodybuilding');
INSERT INTO `workout_type` (type_no,name,name_fr) VALUES (2,'Fitness','Fitness');
INSERT INTO `workout_type` (type_no,name,name_fr) VALUES (3,'Home cycling','Vélo d''intérieur');
INSERT INTO `workout_type` (type_no,name,name_fr) VALUES (4,'Martial art','Art martial');
INSERT INTO `workout_type` (type_no,name,name_fr) VALUES (5,'Running','Course à pied');
INSERT INTO `workout_type` (type_no,name,name_fr) VALUES (6,'Swimming','Natation');
INSERT INTO `workout_type` (type_no,name,name_fr) VALUES (7,'Walk','Marche');
INSERT INTO `workout_type` (type_no,name,name_fr) VALUES (8,'Yoga','Yoga');
COMMIT;
