unit exercise;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, DBGrids, SSExercise, SSPicture, JPEGLib;

resourcestring
  SExerciseTypeName_None = 'None';
  SDlgForm_NewExercise = 'New Exercise';
  SDlgForm_ModifyExercise = 'Modify Exercise';

  SDlgImageTypeError = 'This image type is not supported. Supported image type are JPEG or JPG and PNG.';

type

  { TForm_Exercise }

  TForm_Exercise = class(TForm)
    Button_Save: TButton;
    Button_AddPicture1: TButton;
    Button_AddPicture2: TButton;
    Button_Close: TButton;
    Button_DeletePicture1: TButton;
    Button_DeletePicture2: TButton;
    ComboBox_ExerciseType: TComboBox;
    Edit_ExerciseName: TEdit;
    GroupBox_Picture1: TGroupBox;
    GroupBox_Picture2: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Label_ExerciseName: TLabel;
    Label_ExerciseType: TLabel;
    OpenDialog_AddPicture: TOpenDialog;
    procedure Button_AddPicture1Click(Sender: TObject);
    procedure Button_AddPicture2Click(Sender: TObject);
    procedure Button_CloseClick(Sender: TObject);
    procedure Button_DeletePicture1Click(Sender: TObject);
    procedure Button_DeletePicture2Click(Sender: TObject);
    procedure Button_SaveClick(Sender: TObject);
    procedure ComboBox_ExerciseTypeChange(Sender: TObject);
    procedure Edit_ExerciseNameChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    CurrentExercise: TSSExercise;
    procedure Init();
    procedure UpdateControls();
    procedure AddPicture(Image: TImage; LayoutId: integer);
    procedure DeletePicture(Image: TImage; LayoutId: integer);
    procedure UpdateExercise();

  public
    CreateANewExercise: boolean;
    EditAnExercise: boolean;

  end;

var
  Form_Exercise: TForm_Exercise;

implementation

uses main, exercise_list;

{$R *.lfm}

{ TForm_Exercise }

procedure TForm_Exercise.DeletePicture(Image: TImage; LayoutId: integer);
begin
  { Ask the user if he wants to delete the picture }
  case QuestionDlg(SDlgDeletePictureTitle, SDlgDeletePictureMsg, mtCustom, [mrNo, SDlgBtnNo, 'IsDefault', mrYes, SDlgBtnYes], '') of
    mrYes:
    begin
      { Delete the picture from TImage component }
      Image.Picture.Clear;
    end;
    mrNo:
    begin
      Exit;
    end;
    mrCancel:
    begin
    end;
  end;
end;

procedure TForm_Exercise.AddPicture(Image: TImage; LayoutId: integer);
var
  FileExt: string;
begin
  { Open a picture }
  if OpenDialog_AddPicture.Execute then
  begin
    { Check for supported images }
    FileExt := LowerCase(ExtractFileExt(OpenDialog_AddPicture.FileName));

    if (FileExt <> '.jpeg') and (FileExt <> '.jpg') and (FileExt <> '.png') then
    begin
      ShowMessage(SDlgImageTypeError);
      Exit;
    end;

    { Check max. file size }
    if(FileSize(OpenDialog_AddPicture.FileName) > MaxPictureSize) then
    begin
      ShowMessage(SDlgImageSizeError1 + (MaxPictureSize / 1024).ToString() + SDlgImageSizeError2);
      Exit;
    end;

    { Fill object properties according to image type }
    case LowerCase(FileExt) of
      '.jpg', '.jpeg':
      begin
        case LayoutId of
          1: CurrentExercise.Picture1TypeNo := 1;
          2: CurrentExercise.Picture2TypeNo := 1;
        end;
      end;
      '.png':
      begin
        case LayoutId of
          1: CurrentExercise.Picture1TypeNo := 2;
          2: CurrentExercise.Picture2TypeNo := 2;
        end;
      end;
    end;

    { Load picture into TImage component }
    Image.Picture.LoadFromFile(OpenDialog_AddPicture.FileName);

    case LayoutId of
      1: Image.Picture.SaveToStream(CurrentExercise.Picture1BLOB);
      2: Image.Picture.SaveToStream(CurrentExercise.Picture2BLOB);
    end;
  end;
end;

procedure TForm_Exercise.UpdateExercise();
begin
  if CurrentExercise <> nil then
  begin
    ComboBox_ExerciseType.ItemIndex := CurrentExercise.ExerciseTypeNo;
    Edit_ExerciseName.Text := CurrentExercise.Name;
    case CurrentExercise.Picture1TypeNo of
      1:
      begin
        Image1.Picture.LoadFromStreamWithFileExt(CurrentExercise.Picture1BLOB, 'jpg');
      end;
      2:
      begin
        Image2.Picture.LoadFromStreamWithFileExt(CurrentExercise.Picture2BLOB, 'png');
      end;
    end;
  end;
end;

procedure TForm_Exercise.Init();
begin
  ComboBox_ExerciseType.Clear;
  ComboBox_ExerciseType.Items.Add(SExerciseTypeName_None); // 0
  ComboBox_ExerciseType.Items.Add(DBExerciceTypeName_Other); // 1
  ComboBox_ExerciseType.Items.Add(DBExerciceTypeName_Cardio); // 2
  ComboBox_ExerciseType.Items.Add(DBExerciceTypeName_Strength); // 3
  ComboBox_ExerciseType.Items.Add(DBExerciceTypeName_Stretching); // 3
  ComboBox_ExerciseType.ItemIndex := 0;

  Edit_ExerciseName.Text := '';

  Image1.Picture.Clear;
  Image2.Picture.Clear;
end;

procedure TForm_Exercise.UpdateControls();
begin
  if (CurrentExercise = nil) or (ComboBox_ExerciseType.ItemIndex = 0) or (Length(Trim(Edit_ExerciseName.Text)) = 0) then
  begin
    Button_AddPicture1.Enabled := False;
    Button_DeletePicture1.Enabled := False;
    Button_AddPicture2.Enabled := False;
    Button_DeletePicture2.Enabled := False;
    Button_Save.Enabled := False;
  end
  else
  begin
    Button_AddPicture1.Enabled := True;
    Button_DeletePicture1.Enabled := True;
    Button_AddPicture2.Enabled := True;
    Button_DeletePicture2.Enabled := True;
    Button_Save.Enabled := True;
  end;
end;

procedure TForm_Exercise.FormCreate(Sender: TObject);
begin
  CreateANewExercise := False;
end;

procedure TForm_Exercise.FormShow(Sender: TObject);
begin
  Init();
  if CreateANewExercise = True then
  begin
    Caption := SDlgForm_NewExercise;
    CurrentExercise := TSSExercise.Create;
  end;

  if EditAnExercise = True then
  begin
    Caption := SDlgForm_ModifyExercise;
    CurrentExercise := Form_ExerciseList.CurrentExercise;
    UpdateExercise();
  end;
  UpdateControls();
end;

procedure TForm_Exercise.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_Exercise.Button_DeletePicture1Click(Sender: TObject);
begin
  if CurrentExercise <> nil then
  begin
    DeletePicture(Image1, 1);
  end;
end;

procedure TForm_Exercise.Button_DeletePicture2Click(Sender: TObject);
begin
  if CurrentExercise <> nil then
  begin
    DeletePicture(Image2, 2);
  end;
end;

procedure TForm_Exercise.Button_AddPicture1Click(Sender: TObject);
begin
  if CurrentExercise <> nil then
  begin
    AddPicture(Image1, 1);
  end;
end;

procedure TForm_Exercise.Button_AddPicture2Click(Sender: TObject);
begin
  if CurrentExercise <> nil then
  begin
    AddPicture(Image2, 2);
  end;
end;

procedure TForm_Exercise.Button_SaveClick(Sender: TObject);
begin
  if CurrentExercise <> nil then
  begin
    CurrentExercise.DBInsertReplace(Form_Main.DBConnection, Form_Main.SQLTransaction1);
    Close;
  end;
end;

procedure TForm_Exercise.ComboBox_ExerciseTypeChange(Sender: TObject);
begin
  if ComboBox_ExerciseType.ItemIndex <> 0 then
  begin
    CurrentExercise.ExerciseTypeNo := ComboBox_ExerciseType.ItemIndex;
  end;
  UpdateControls();
end;

procedure TForm_Exercise.Edit_ExerciseNameChange(Sender: TObject);
begin
  if Length(Trim(Edit_ExerciseName.Text)) > 0 then
  begin
    CurrentExercise.Name := Trim(Edit_ExerciseName.Text);
  end;
  UpdateControls();
end;

procedure TForm_Exercise.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CreateANewExercise := False;
  if EditAnExercise = False then
  begin
    FreeAndNil(CurrentExercise);
  end;
  EditAnExercise := False;
end;

end.



