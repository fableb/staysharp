unit measurements;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin;

resourcestring

  { -------------------------------------- }
  { Body Mass Index interpretation strings }
  { -------------------------------------- }
  SBMI0 = '--';
  SBMI1 = 'BMI < 15, very severely underweight';
  SBMI2 = 'BMI in [15, 16[, severely underweight';
  SBMI3 = 'BMI in [16, 18.5[, underweight';
  SBMI4 = 'BMI in [18.5, 25[, normal (healthy weight)';
  SBMI5 = 'BMI in [25, 30[, overweight';
  SBMI6 = 'BMI in [30, 35[, moderately obese';
  SBMI7 = 'BMI in [35, 40[, severely obese';
  SBMI8 = 'BMI in [40, 45[, very severely obese';
  SBMI9 = 'BMI in [45, 50[, morbidly obese';
  SBMI10 = 'BMI in [50, 60[, super obese';
  SBMI11 = 'BMI > 60, hyper obese';

type

  { TForm_Measurements }

  TForm_Measurements = class(TForm)
    Button_Save: TButton;
    Button_Close: TButton;
    Button_ComputeBMI: TButton;
    FloatSpinEdit_Shoulders: TFloatSpinEdit;
    FloatSpinEdit_Sleep: TFloatSpinEdit;
    FloatSpinEdit_Neck: TFloatSpinEdit;
    FloatSpinEdit_CalfLeft: TFloatSpinEdit;
    FloatSpinEdit_CalfRight: TFloatSpinEdit;
    FloatSpinEdit_Weight: TFloatSpinEdit;
    FloatSpinEdit_Chest: TFloatSpinEdit;
    FloatSpinEdit_ArmLeft: TFloatSpinEdit;
    FloatSpinEdit_ArmRight: TFloatSpinEdit;
    FloatSpinEdit_ForearmLeft: TFloatSpinEdit;
    FloatSpinEdit_ForearmRight: TFloatSpinEdit;
    FloatSpinEdit_ThightLeft: TFloatSpinEdit;
    FloatSpinEdit_ThightRight: TFloatSpinEdit;
    FloatSpinEdit_MidSection: TFloatSpinEdit;
    FloatSpinEdit_Hips: TFloatSpinEdit;
    FloatSpinEdit_BMI: TFloatSpinEdit;
    GroupBox_BloodPressure: TGroupBox;
    GroupBox_Body: TGroupBox;
    GroupBox_Global: TGroupBox;
    Label_BMIInterpertation: TLabel;
    Label_Shoulders: TLabel;
    Label_Sleep: TLabel;
    Label_Right: TLabel;
    Label_Left: TLabel;
    Label_Calf: TLabel;
    Label_Systolic: TLabel;
    Label_Thigh: TLabel;
    Label_Forearm: TLabel;
    Label_Hips: TLabel;
    Label_MidSection: TLabel;
    Label_Arm: TLabel;
    Label_Chest: TLabel;
    Label_Neck: TLabel;
    Label_Diastolic: TLabel;
    Label_RestHearthRate: TLabel;
    Label_Weight: TLabel;
    Label_BMI: TLabel;
    SpinEdit_Systolic: TSpinEdit;
    SpinEdit_Diastolic: TSpinEdit;
    SpinEdit_RestHearthRate: TSpinEdit;
    procedure Button_CloseClick(Sender: TObject);
    procedure Button_ComputeBMIClick(Sender: TObject);
    procedure Button_SaveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public
    function BodyMassIndexInterpretation(BMI: double): string;

  end;

var
  Form_Measurements: TForm_Measurements;

implementation

{$R *.lfm}

uses main;

{ TForm_Measurements }

procedure TForm_Measurements.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

function TForm_Measurements.BodyMassIndexInterpretation(BMI: double): string;
begin
  if BMI < 0 then
    Result := SBMI0;
  if (BMI > 0) and (BMI < 15) then
    Result := SBMI1;
  if (BMI >= 15) and (BMI < 16) then
    Result := SBMI2;
  if (BMI >= 16) and (BMI < 18.5) then
    Result := SBMI3;
  if (BMI >= 18.5) and (BMI < 25) then
    Result := SBMI4;
  if (BMI >= 25) and (BMI < 30) then
    Result := SBMI5;
  if (BMI >= 30) and (BMI < 35) then
    Result := SBMI6;
  if (BMI >= 35) and (BMI < 40) then
    Result := SBMI7;
  if (BMI >= 40) and (BMI < 45) then
    Result := SBMI8;
  if (BMI >= 45) and (BMI < 50) then
    Result := SBMI9;
  if (BMI >= 50) and (BMI < 60) then
    Result := SBMI10;
  if BMI >= 60 then
    Result := SBMI11;
end;

procedure TForm_Measurements.Button_ComputeBMIClick(Sender: TObject);
var
  BMI: double;
begin
  { BMI = mass(kg) / (height(m))^2 }
  if Form_main.CurrentAccount.FHeight = 0 then
    BMI := 0
  else
    BMI := (FloatSpinEdit_Weight.Value / sqr(Form_main.CurrentAccount.FHeight / 100.0));

  FloatSpinEdit_BMI.Value := BMI;
  Label_BMIInterpertation.Caption := BodyMassIndexInterpretation(BMI);
end;

procedure TForm_Measurements.Button_SaveClick(Sender: TObject);
begin
  Form_main.CurrentMeasurement.CreationDate := FormatDateTime('yyyy-mm-dd', Form_Main.Calendar1.DateTime);
  Form_main.CurrentMeasurement.Weight := FloatSpinEdit_Weight.Value;
  Form_main.CurrentMeasurement.BodyMassIndex := FloatSpinEdit_BMI.Value;
  Form_main.CurrentMeasurement.Sleep := FloatSpinEdit_Sleep.Value;
  Form_main.CurrentMeasurement.RestHeartRate := SpinEdit_RestHearthRate.Value;
  Form_main.CurrentMeasurement.BloodPressureSystolic := SpinEdit_Systolic.Value;
  Form_main.CurrentMeasurement.BloodPressureDiastolic := SpinEdit_Diastolic.Value;
  Form_main.CurrentMeasurement.Neck := FloatSpinEdit_Neck.Value;
  Form_main.CurrentMeasurement.Shoulders := FloatSpinEdit_Shoulders.Value;
  Form_main.CurrentMeasurement.Chest := FloatSpinEdit_Chest.Value;
  Form_main.CurrentMeasurement.MidSection := FloatSpinEdit_MidSection.Value;
  Form_main.CurrentMeasurement.Hips := FloatSpinEdit_Hips.Value;
  Form_main.CurrentMeasurement.ArmLeft := FloatSpinEdit_ArmLeft.Value;
  Form_main.CurrentMeasurement.ArmRignt := FloatSpinEdit_ArmRight.Value;
  Form_main.CurrentMeasurement.ForearmLeft := FloatSpinEdit_ForearmLeft.Value;
  Form_main.CurrentMeasurement.ForearmRight := FloatSpinEdit_ArmRight.Value;
  Form_main.CurrentMeasurement.ThighLeft := FloatSpinEdit_ThightLeft.Value;
  Form_main.CurrentMeasurement.ThighRight := FloatSpinEdit_ThightRight.Value;
  Form_main.CurrentMeasurement.CalfLeft := FloatSpinEdit_CalfLeft.Value;
  Form_main.CurrentMeasurement.CalfRight := FloatSpinEdit_CalfRight.Value;

  Form_Main.CurrentMeasurement.DBInsertReplace(Form_Main.DBConnection, Form_Main.SQLTransaction1);
  //Form_Main.RefreshGlobalHealthPlot();
  //Form_Main.RefreshBodyMeasurementsPlot();
  Close;
end;

procedure TForm_Measurements.FormShow(Sender: TObject);
begin
  FloatSpinEdit_Weight.Value := Form_main.CurrentMeasurement.Weight;
  FloatSpinEdit_BMI.Value := Form_main.CurrentMeasurement.BodyMassIndex;
  Label_BMIInterpertation.Caption := BodyMassIndexInterpretation(Form_main.CurrentMeasurement.BodyMassIndex);
  FloatSpinEdit_Sleep.Value := Form_main.CurrentMeasurement.Sleep;
  SpinEdit_RestHearthRate.Value := Form_main.CurrentMeasurement.RestHeartRate;
  SpinEdit_Systolic.Value := Form_main.CurrentMeasurement.BloodPressureSystolic;
  SpinEdit_Diastolic.Value := Form_main.CurrentMeasurement.BloodPressureDiastolic;
  FloatSpinEdit_Neck.Value := Form_main.CurrentMeasurement.Neck;
  FloatSpinEdit_Shoulders.Value := Form_main.CurrentMeasurement.Shoulders;
  FloatSpinEdit_Chest.Value := Form_main.CurrentMeasurement.Chest;
  FloatSpinEdit_MidSection.Value := Form_main.CurrentMeasurement.MidSection;
  FloatSpinEdit_Hips.Value := Form_main.CurrentMeasurement.Hips;
  FloatSpinEdit_ArmLeft.Value := Form_main.CurrentMeasurement.ArmLeft;
  FloatSpinEdit_ArmRight.Value := Form_main.CurrentMeasurement.ArmRignt;
  FloatSpinEdit_ForearmLeft.Value := Form_main.CurrentMeasurement.ForearmLeft;
  FloatSpinEdit_ArmRight.Value := Form_main.CurrentMeasurement.ForearmRight;
  FloatSpinEdit_ThightLeft.Value := Form_main.CurrentMeasurement.ThighLeft;
  FloatSpinEdit_ThightRight.Value := Form_main.CurrentMeasurement.ThighRight;
  FloatSpinEdit_CalfLeft.Value := Form_main.CurrentMeasurement.CalfLeft;
  FloatSpinEdit_CalfRight.Value := Form_main.CurrentMeasurement.CalfRight;
end;

end.

