unit exercise_list;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DB, sqldb, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, DBGrids, ExtCtrls, DBCtrls, SSExercise, Grids;

type

  { TForm_ExerciseList }

  TForm_ExerciseList = class(TForm)
    Button_AddToWorkout: TButton;
    Button_EditExercise: TButton;
    Button_DeleteExercise: TButton;
    Button_NewExercise: TButton;
    Button_Close: TButton;
    Button_ClearSearch: TButton;
    ComboBox_ExerciseType: TComboBox;
    DataSource_Exercises: TDataSource;
    DBGrid_Exercises: TDBGrid;
    DBImage1: TDBImage;
    DBImage2: TDBImage;
    Edit_SearchInName: TEdit;
    GroupBox_Exercises: TGroupBox;
    GroupBox_Picture1: TGroupBox;
    GroupBox_Picture2: TGroupBox;
    Label_SearchInName: TLabel;
    Label_ExerciseType: TLabel;
    SQLQuery_Exercises: TSQLQuery;
    procedure Button_ClearSearchClick(Sender: TObject);
    procedure Button_DeleteExerciseClick(Sender: TObject);
    procedure Button_EditExerciseClick(Sender: TObject);
    procedure Button_NewExerciseClick(Sender: TObject);
    procedure Button_CloseClick(Sender: TObject);
    procedure ComboBox_ExerciseTypeChange(Sender: TObject);
    procedure Edit_SearchInNameChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure Init();
    procedure UpdateControls();
    procedure DBSelect(ExerciseType: integer; SearchStr: string);
    procedure DBDelete();

  public
    FromListOfExercises: boolean; // We came from Exercises menu
    CurrentExercise: TSSExercise;

  end;

var
  Form_ExerciseList: TForm_ExerciseList;

implementation

{$R *.lfm}

uses main, exercise;

{ TForm_ExerciseList }

procedure TForm_ExerciseList.DBSelect(ExerciseType: integer; SearchStr: string);
var
  SQL: string;
begin
  SQL := 'SELECT * FROM exercise WHERE 1 = 1 ';

  if ExerciseType <> 0 then
  begin
    SQL := SQL + 'AND exercise_type_no = ' + IntToStr(ExerciseType) + ' ';
    ;
  end;

  if Trim(SearchStr) <> '' then
  begin
    SQL := SQL + 'AND name LIKE ' + QuotedStr('%' + SearchStr + '%') + ' ';
  end;

  SQLQuery_Exercises.Clear;
  SQLQuery_Exercises.SQL.Text := SQL;
  SQLQuery_Exercises.Open;
end;

procedure TForm_ExerciseList.DBDelete();
var
  ExerciseNo: integer;
begin
  ExerciseNo := DBGrid_Exercises.DataSource.DataSet.FieldByName('exercise_no').AsInteger;
  SQLQuery_Exercises.Clear;
  SQLQuery_Exercises.SQL.Text := 'DELETE FROM exercise WHERE exercise_no = :ExerciseNo';
  SQLQuery_Exercises.Params.ParamByName('ExerciseNo').AsInteger := ExerciseNo;
  SQLQuery_Exercises.ExecSQL;
  Form_Main.SQLTransaction1.Commit;
end;

procedure TForm_ExerciseList.Init();
begin
  ComboBox_ExerciseType.Clear;
  ComboBox_ExerciseType.Items.Add(SExerciseTypeName_None); // 0
  ComboBox_ExerciseType.Items.Add(DBExerciceTypeName_Other); // 1
  ComboBox_ExerciseType.Items.Add(DBExerciceTypeName_Cardio); // 2
  ComboBox_ExerciseType.Items.Add(DBExerciceTypeName_Strength); // 3
  ComboBox_ExerciseType.Items.Add(DBExerciceTypeName_Stretching); // 3
  ComboBox_ExerciseType.ItemIndex := 0;
end;

procedure TForm_ExerciseList.UpdateControls();
begin
  if SQLQuery_Exercises.RecordCount = 0 then
  begin
    Button_EditExercise.Enabled := False;
    Button_DeleteExercise.Enabled := False;
  end
  else
  begin
    Button_EditExercise.Enabled := True;
    Button_DeleteExercise.Enabled := True;
  end;
end;

procedure TForm_ExerciseList.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_ExerciseList.ComboBox_ExerciseTypeChange(Sender: TObject);
begin
  DBSelect(ComboBox_ExerciseType.ItemIndex, Trim(Edit_SearchInName.Text));
end;

procedure TForm_ExerciseList.Edit_SearchInNameChange(Sender: TObject);
begin
  DBSelect(ComboBox_ExerciseType.ItemIndex, Trim(Edit_SearchInName.Text));
end;

procedure TForm_ExerciseList.FormActivate(Sender: TObject);
begin

end;

procedure TForm_ExerciseList.FormCreate(Sender: TObject);
begin
  FromListOfExercises := False;
end;

procedure TForm_ExerciseList.FormShow(Sender: TObject);
begin
  Init();
  if FromListOfExercises = True then
  begin
    Button_AddToWorkout.Visible := False;
  end;
  DBSelect(ComboBox_ExerciseType.ItemIndex, Trim(Edit_SearchInName.Text));
  UpdateControls();
end;

procedure TForm_ExerciseList.Button_NewExerciseClick(Sender: TObject);
begin
  Form_Exercise.CreateANewExercise := True;
  Form_Exercise.ShowModal;
  DBSelect(ComboBox_ExerciseType.ItemIndex, Trim(Edit_SearchInName.Text));
  UpdateControls();
end;

procedure TForm_ExerciseList.Button_DeleteExerciseClick(Sender: TObject);
begin
  DBDelete();
  DBSelect(ComboBox_ExerciseType.ItemIndex, Trim(Edit_SearchInName.Text));
  UpdateControls();
end;

procedure TForm_ExerciseList.Button_EditExerciseClick(Sender: TObject);
var
  ExerciseNo: integer;
begin
  ExerciseNo := DBGrid_Exercises.DataSource.DataSet.FieldByName('exercise_no').AsInteger; // get current line
  CurrentExercise := TSSExercise.Create;
  CurrentExercise.DBFind(Form_Main.DBConnection, ExerciseNo);
  Form_Exercise.EditAnExercise := True;
  Form_Exercise.ShowModal;
  FreeAndNil(CurrentExercise);
  DBSelect(ComboBox_ExerciseType.ItemIndex, Trim(Edit_SearchInName.Text));
end;

procedure TForm_ExerciseList.Button_ClearSearchClick(Sender: TObject);
begin
  Edit_SearchInName.Text := '';
end;

end.
