unit picture_list;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, DB, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, DBGrids, ExtCtrls, Calendar, EditBtn, DBCtrls;

type

  { TForm_PictureList }

  TForm_PictureList = class(TForm)
    Button_Close: TButton;
    Button_GotoPage: TButton;
    DataSource_Pictures: TDataSource;
    DateEdit_StartDate: TDateEdit;
    DateEdit_EndDate: TDateEdit;
    DBGrid1: TDBGrid;
    DBImage1: TDBImage;
    Label_EndDate: TLabel;
    Label_StartDate: TLabel;
    Panel_PictureList: TPanel;
    Panel_Pictures: TPanel;
    SQLQuery_Pictures: TSQLQuery;
    procedure Button_CloseClick(Sender: TObject);
    procedure Button_GotoPageClick(Sender: TObject);
    procedure DateEdit_EndDateChange(Sender: TObject);
    procedure DateEdit_StartDateChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBSelect();
  private

  public

  end;

var
  Form_PictureList: TForm_PictureList;

implementation

{$R *.lfm}

uses main;

{ TForm_PictureList }

procedure TForm_PictureList.DBSelect();
var
  SQL: string;
begin
  SQL := 'SELECT * FROM picture WHERE account_no = :AccountNo AND creation_date >= :StartDate AND creation_date <= :EndDate ORDER BY creation_date';
  SQLQuery_Pictures.Close;
  SQLQuery_Pictures.Clear;
  SQLQuery_Pictures.SQL.Text := SQL;
  SQLQuery_Pictures.Params.ParamByName('AccountNo').AsInteger :=
    Form_Main.CurrentAccount.FAccountNo;
  SQLQuery_Pictures.Params.ParamByName('StartDate').AsString :=
    FormatDateTime('yyyy-mm-dd', DateEdit_StartDate.Date);
  SQLQuery_Pictures.Params.ParamByName('EndDate').AsString :=
    FormatDateTime('yyyy-mm-dd', DateEdit_EndDate.Date);
  SQLQuery_Pictures.Open;

  if SQLQuery_Pictures.RecordCount > 0 then
    Button_GotoPage.Enabled := True
  else
    Button_GotoPage.Enabled := False;
end;

procedure TForm_PictureList.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_PictureList.Button_GotoPageClick(Sender: TObject);
begin
  Form_Main.Calendar1.DateTime :=
    StrToDate(SQLQuery_Pictures.FieldByName('creation_date').AsString, 'yyyy-mm-dd', '-');
  Form_Main.UpdatePictures();
  Form_Main.TabSheet_Pictures.Show;
end;

procedure TForm_PictureList.DateEdit_EndDateChange(Sender: TObject);
begin
  DBSelect();
end;

procedure TForm_PictureList.DateEdit_StartDateChange(Sender: TObject);
begin
  DBSelect();
end;

procedure TForm_PictureList.FormShow(Sender: TObject);
begin
  DateEdit_StartDate.Date := IncMonth(Now, -6);
  DateEdit_EndDate.Date := Now;

  { Form position relative to main form }
  Left := Form_Main.Width - (Width + 20);
  Top := Form_Main.Height - (Height + 20);

  DBSelect();
end;

end.
