{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit SSMealItem;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, SSFood;

type
  { TSSMealItem }

  TSSMealItem = class(TSSFood)
  private
    FQuantity: double;
    procedure SetQuantity(quantity: double);
  public
    FMealItemNo: longint;
    FMealNo: longint;
    property Value: double read FQuantity write SetQuantity;
    procedure DBSelect(const DBConnection: TSQLConnection; const MealItemNo: integer);
    procedure DBInsertReplace(const DBConnection: TSQLConnection;
      const DBTransaction: TSQLTransaction);
    procedure DBDelete(const DBConnection: TSQLConnection;
      const DBTransaction: TSQLTransaction; const MealItemNo: integer);
    procedure CopyFromFood(const Food: TSSFood);

  end;

implementation

procedure TSSMealItem.DBSelect(const DBConnection: TSQLConnection;
  const MealItemNo: integer);
begin

end;

procedure TSSMealItem.DBInsertReplace(const DBConnection: TSQLConnection;
  const DBTransaction: TSQLTransaction);
begin

end;

procedure TSSMealItem.DBDelete(const DBConnection: TSQLConnection;
  const DBTransaction: TSQLTransaction; const MealItemNo: integer);
begin

end;

procedure TSSMealItem.CopyFromFood(const Food: TSSFood);
begin
  FFoodNo := Food.FFoodNo;
  FName := Food.FName;
  FFoodCategory := Food.FFoodCategory;
  FFoodCategoryName := Food.FFoodCategoryName;
  FNutrients := Food.FNutrients;
  FUserDefined := Food.FUserDefined;
  FFoodUnitNo := Food.FFoodUnitNo;
  FUnitQuantity := Food.FUnitQuantity;
end;

procedure TSSMealItem.SetQuantity(quantity: double);
{ Scale all food nutrients according to Quantity }
var
  Factor: double = 0.0;
  TmpNutrients: TNutrients;
begin
  if quantity > 0 then
    Factor := FUnitQuantity / quantity;
  TmpNutrients := ScaleNutrients(Factor);
  Self.FNutrients := TmpNutrients;
end;

end.
