{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit workout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DateTimePicker, Forms, Controls, Graphics,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Spin, sqldb, sqlite3conn, SSFile;

type

  { TForm_Workout }

  TForm_Workout = class(TForm)
    Button_DeleteExercise: TButton;
    Button_AddCardioExercise: TButton;
    Button_Workout_Cancel: TButton;
    Button_Workout_Save: TButton;
    ComboBox_WorkoutType: TComboBox;
    DateTimePicker_Date: TDateTimePicker;
    DateTimePicker_Duration: TDateTimePicker;
    DateTimePicker_StartTime: TDateTimePicker;
    FloatSpinEdit_Calories: TFloatSpinEdit;
    GroupBox_Series: TGroupBox;
    GroupBox_Exercises: TGroupBox;
    Label_Calories: TLabel;
    Label_Date: TLabel;
    Label_Duration: TLabel;
    Label_MaxCF: TLabel;
    Label_MeanCF: TLabel;
    Label_MinCF: TLabel;
    Label_StartTime: TLabel;
    Label_WorkoutFeelings: TLabel;
    Label_WorkoutNote: TLabel;
    Label_WorkoutType: TLabel;
    Memo_WorkoutNote: TMemo;
    PageControl_Workout: TPageControl;
    RadioGroup_WorkoutFeelings: TRadioGroup;
    SpinEdit_MaxCF: TSpinEdit;
    SpinEdit_MeanCF: TSpinEdit;
    SpinEdit_MinCF: TSpinEdit;
    SQLQuery1: TSQLQuery;
    TabSheet_Exercises: TTabSheet;
    TabSheet_Workout: TTabSheet;
    procedure Button_AddCardioExerciseClick(Sender: TObject);
    procedure Button_Workout_CancelClick(Sender: TObject);
    procedure Button_Workout_SaveClick(Sender: TObject);
    procedure ComboBox_WorkoutTypeChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form_Workout: TForm_Workout;

implementation

{$R *.lfm}
uses main, exercise_list;

{ TForm_Workout }

procedure TForm_Workout.ComboBox_WorkoutTypeChange(Sender: TObject);
begin
  if ComboBox_WorkoutType.ItemIndex = 0 then
  begin
    // Hide save button
    Button_Workout_Save.Enabled := False;
  end
  else
  begin
    // Enable save button
    Button_Workout_Save.Enabled := True;
  end;
end;

procedure TForm_Workout.Button_Workout_CancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TForm_Workout.Button_AddCardioExerciseClick(Sender: TObject);
begin
  Form_ExerciseList.ShowModal;
end;

procedure TForm_Workout.Button_Workout_SaveClick(Sender: TObject);
var
  WorkoutNoStr: string;
  SQL: string;
  FloatFormatSettings: TFormatSettings;
begin
  FloatFormatSettings.DecimalSeparator := '.';
  // Check if we create a new workout or update an existing one :
  if Form_Main.CurrentWorkout = nil then
    WorkoutNoStr := 'NULL' // insert record
  else
    WorkoutNoStr := IntToStr(Form_Main.CurrentWorkout.FWorkoutNo);
  // update existing record

  // SQL query
  SQL := 'INSERT OR REPLACE INTO workout(workout_no,account_no,workout_type_no,';
  SQL := SQL + 'creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type_no,note) ';
  SQL := SQL + ' VALUES(';
  SQL := SQL + WorkoutNoStr + ',';
  SQL := SQL + IntToStr(Form_Main.CurrentAccount.FAccountNo) + ',';
  SQL := SQL + IntToStr(ComboBox_WorkoutType.ItemIndex) + ',';
  SQL := SQL + 'date(''' + FormatDateTime('yyyy-mm-dd', DateTimePicker_Date.DateTime) + '''),';
  SQL := SQL + 'time(''' + FormatDateTime('hh:mm:ss', DateTimePicker_StartTime.DateTime) + '''),';
  SQL := SQL + 'time(''' + FormatDateTime('hh:mm:ss', DateTimePicker_Duration.DateTime) + '''),';
  SQL := SQL + SpinEdit_MinCF.Text + ',';
  SQL := SQL + SpinEdit_MeanCF.Text + ',';
  SQL := SQL + SpinEdit_MaxCF.Text + ',';
  SQL := SQL + FloatToStr(StrToFloat(FloatSpinEdit_Calories.Text), FloatFormatSettings) + ',';
  SQL := SQL + IntToStr(RadioGroup_WorkoutFeelings.ItemIndex) + ',';
  SQL := SQL + QuotedStr(Memo_WorkoutNote.Text) + ')';

  // Execute SQL query
  SQLQuery1.Close;
  SQLQuery1.SQL.Clear;
  SQLQuery1.SQL.Text := SQL;
  SQLQuery1.ExecSQL;
  Form_Main.SQLTransaction1.Commit;
  ModalResult := mrOK;
end;

procedure TForm_Workout.FormShow(Sender: TObject);
var
  WorkoutLabels: TStringList;
  FeelingsLabels: TStringList;
begin
  PageControl_Workout.ActivePageIndex := 0;
  // Initialize form labels
  WorkoutLabels := TStringList.Create;
  WorkoutLabels.Add(SDBWorkoutType_None); // Id. 0
  WorkoutLabels.Add(SDBWorkoutType_Bodybuilding); // Id. 1
  WorkoutLabels.Add(SDBWorkoutType_Fitness); // Id. 2
  WorkoutLabels.Add(SDBWorkoutType_HomeCycling); // Id. 3
  WorkoutLabels.Add(SDBWorkoutType_MartialArt); // Id. 4
  WorkoutLabels.Add(SDBWorkoutType_Running); // Id. 5
  WorkoutLabels.Add(SDBWorkoutType_Swimming); // Id. 6
  WorkoutLabels.Add(SDBWorkoutType_Walk); // Id. 7
  WorkoutLabels.Add(SDBWorkoutType_Yoga); // Id. 8
  ComboBox_WorkoutType.Items := WorkoutLabels;
  WorkoutLabels.Free;

  FeelingsLabels := TStringList.Create;
  FeelingsLabels.add(SDBFeelingType_None);
  FeelingsLabels.add(SDBFeelingType_VeryBad);
  FeelingsLabels.add(SDBFeelingType_Bad);
  FeelingsLabels.add(SDBFeelingType_Neutral);
  FeelingsLabels.add(SDBFeelingType_Good);
  FeelingsLabels.add(SDBFeelingType_VeryGood);
  RadioGroup_WorkoutFeelings.Items := FeelingsLabels;
  FeelingsLabels.Free;

  if Form_Main.CurrentWorkout = nil then
  begin
    // New workout: initialize to default values
    Caption := SDlgNewWorkoutTitle;
    DateTimePicker_Date.DateTime := Form_Main.Calendar1.DateTime;
    DateTimePicker_StartTime.DateTime := Now;
    DateTimePicker_Duration.DateTime := StrToTime('00:00:00');
    ComboBox_WorkoutType.ItemIndex := 0;
    RadioGroup_WorkoutFeelings.ItemIndex := 0;
    SpinEdit_MinCF.Caption := '0';
    SpinEdit_MeanCF.Caption := '0';
    SpinEdit_MaxCF.Caption := '0';
    FloatSpinEdit_Calories.Caption := FormatFloat('0.00', 0);
    Memo_WorkoutNote.Text := '';
  end
  else
  begin
    // Update workout: get current values
    Caption := SDlgModifyWorkoutTitle;
    DateTimePicker_Date.DateTime :=
      StrToDate(Form_Main.CurrentWorkout.FCreationDate, 'yyyy-mm-dd', '-');
    DateTimePicker_StartTime.DateTime := StrToTime(Form_Main.CurrentWorkout.FStartTime);
    DateTimePicker_Duration.DateTime := StrToTime(Form_Main.CurrentWorkout.FDuration);
    ComboBox_WorkoutType.ItemIndex := Form_Main.CurrentWorkout.FWorkoutType;
    RadioGroup_WorkoutFeelings.ItemIndex := Form_Main.CurrentWorkout.FFeeleingType;
    SpinEdit_MinCF.Caption := IntToStr(Form_Main.CurrentWorkout.FMinCF);
    SpinEdit_MeanCF.Caption := IntToStr(Form_Main.CurrentWorkout.FMeanCF);
    SpinEdit_MaxCF.Caption := IntToStr(Form_Main.CurrentWorkout.FMaxCF);
    FloatSpinEdit_Calories.Caption := FloatToStr(Form_Main.CurrentWorkout.FCalories);
    Memo_WorkoutNote.Text := Form_Main.CurrentWorkout.FNote;
  end;

  ComboBox_WorkoutTypeChange(Sender);
end;

end.


