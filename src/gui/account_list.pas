{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit account_list;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, DB, FileUtil, Forms, Controls, Graphics,
  Dialogs, StdCtrls, DBGrids, EditBtn, SSAccount;

type

  { TForm_AccountList }

  TForm_AccountList = class(TForm)
    Button_Add: TButton;
    Button_Edit: TButton;
    Button_Delete: TButton;
    Button_Open: TButton;
    Button_Close: TButton;
    CheckBox_ShowAtStartup: TCheckBox;
    DBGrid_AccountList: TDBGrid;
    Label_Accounts: TLabel;
    procedure Button_AddClick(Sender: TObject);
    procedure Button_CloseClick(Sender: TObject);
    procedure Button_DeleteClick(Sender: TObject);
    procedure Button_EditClick(Sender: TObject);
    procedure Button_OpenClick(Sender: TObject);
    procedure CheckBox_ShowAtStartupChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    { My custom stuff }
    procedure HideDBGridColumns();
    procedure UpdateControls();
  private

  public
    CurrentAccount: TSSAccount;

  end;

var
  Form_AccountList: TForm_AccountList;

implementation

{$R *.lfm}

uses main, account;

{ TForm_AccountList }

procedure TForm_AccountList.HideDBGridColumns();
begin
  DBGrid_AccountList.DataSource.DataSet.FieldByName('account_no').Visible := False;
  DBGrid_AccountList.DataSource.DataSet.FieldByName('name').Visible := True;
  DBGrid_AccountList.DataSource.DataSet.FieldByName('creation_date').Visible := False;
  DBGrid_AccountList.DataSource.DataSet.FieldByName('first_name').Visible := True;
  DBGrid_AccountList.DataSource.DataSet.FieldByName('last_name').Visible := True;
  DBGrid_AccountList.DataSource.DataSet.FieldByName('birth_date').Visible := False;
  DBGrid_AccountList.DataSource.DataSet.FieldByName('gender').Visible := False;
  DBGrid_AccountList.DataSource.DataSet.FieldByName('height').Visible := False;
  DBGrid_AccountList.DataSource.DataSet.FieldByName('weight').Visible := False;
end;

procedure TForm_AccountList.UpdateControls();
var NumAccounts: Integer;
begin
  // Manage edit and delete buttons
  NumAccounts := Form_Main.SQLQuery_Account.RecordCount;
  if NumAccounts = 0 then
  begin
    Button_Edit.Enabled := False;
    Button_Delete.Enabled := False;
    Button_Open.Enabled := False;
  end
  else
  begin
    Button_Edit.Enabled := True;
    Button_Delete.Enabled := True;
    Button_Open.Enabled := True;
  end;

  // Show at startup
  if Form_Main.ApplicationSettings.ShowAccountsAtStartup = True then
    CheckBox_ShowAtStartup.Checked := True;
end;

procedure TForm_AccountList.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_AccountList.Button_DeleteClick(Sender: TObject);
var
  SQL: string;
  AccountNumber: longint;
  QueryWorkout, QueryMeals, QueryMealItems, QueryAccount: TSQLQuery;
begin
  // Get account id
  AccountNumber := Form_Main.SQLQuery_Account.FieldByName('account_no').AsLongint;

  // Check if the account is already opened
  if Form_Main.CurrentAccount <> nil then
  begin
    // There is an opened account
    if AccountNumber = Form_Main.CurrentAccount.FAccountNo then
    begin
      // We try to delete an opened account
      // We prevent the user to delete an opened account
      QuestionDlg(SDlgDeleteAccountTitle,
        SDlgDeleteAccountMsg1,
        mtCustom, [mrYes, SDlgBtnYes], '');
      Exit;
    end;
  end;

  case QuestionDlg(SDlgDeleteAccountTitle,
      SDlgDeleteAccountMsg2,
      mtCustom, [mrNo, SDlgBtnNo, 'IsDefault', mrYes, SDlgBtnYes], '') of
    mrYes:
    begin
      { --------------------------------------------- }
      { Delete workouts related to the account number }
      { --------------------------------------------- }
      QueryWorkout := TSQLQuery.Create(nil);
      QueryWorkout.DataBase := Form_Main.DBConnection;
      SQL := 'DELETE FROM workout WHERE account_no = ' + IntToStr(AccountNumber);
      QueryWorkout.Close;
      QueryWorkout.SQL.Clear;
      QueryWorkout.SQL.Text := SQL;
      QueryWorkout.ExecSQL;
      Form_Main.SQLTransaction1.Commit;
      FreeAndNil(QueryWorkout);

      { ----------------------------------------------------------}
      { Delete meals and meal items related to the account number }
      { ----------------------------------------------------------}
      QueryMeals := TSQLQuery.Create(nil);
      QueryMeals.DataBase := Form_Main.DBConnection;
      SQL := 'SELECT * FROM meal WHERE account_no = ' + IntToStr(AccountNumber);
      QueryMeals.Close;
      QueryMeals.SQL.Clear;
      QueryMeals.SQL.Text := SQL;
      QueryMeals.Open;

      QueryMealItems:= TSQLQuery.Create(nil);
      QueryMealItems.DataBase := Form_Main.DBConnection;
      QueryMeals.First;
      while not QueryMeals.EOF do
      begin
        { Delete meal items }
        SQL := 'DELETE FROM meal_item WHERE meal_no = ' + QueryMeals.FieldByName('meal_no').AsString;
        QueryMealItems.Close;
        QueryMealItems.SQL.Clear;
        QueryMealItems.SQL.Text := SQL;
        QueryMealItems.ExecSQL;
        QueryMeals.Next;
      end;
      Form_Main.SQLTransaction1.Commit;
      FreeAndNil(QueryMealItems);

      { Delete meals }
      SQL := 'DELETE FROM meal WHERE account_no = ' + IntToStr(AccountNumber);
      QueryMeals.Close;
      QueryMeals.SQL.Clear;
      QueryMeals.SQL.Text := SQL;
      QueryMeals.ExecSQL;
      Form_Main.SQLTransaction1.Commit;
      FreeAndNil(QueryMeals);

      { -------------------------------------------- }
      { Delete account related to the account number }
      { -------------------------------------------- }
      QueryAccount := TSQLQuery.Create(nil);
      QueryAccount.DataBase := Form_Main.DBConnection;
      SQL := 'DELETE FROM account WHERE account_no = ' + IntToStr(AccountNumber);
      QueryAccount.Close;
      QueryAccount.SQL.Clear;
      QueryAccount.SQL.Text := SQL;
      QueryAccount.ExecSQL;
      Form_Main.SQLTransaction1.Commit;
      FreeAndNil(QueryAccount);
    end;
    mrNo: ;
    mrCancel: ;
  end;
  Form_Main.UpdateAccountList();
  HideDBGridColumns();
  UpdateControls();
end;

procedure TForm_AccountList.Button_EditClick(Sender: TObject);
begin
  CurrentAccount := TSSAccount.Create;

  CurrentAccount.FAccountNo :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('account_no').AsLongint;
  CurrentAccount.FName := DBGrid_AccountList.DataSource.DataSet.FieldByName('name').AsString;
  CurrentAccount.FCreationDate :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('creation_date').AsString;
  CurrentAccount.FFirstName :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('first_name').AsString;
  CurrentAccount.FLastName := DBGrid_AccountList.DataSource.DataSet.FieldByName(
    'last_name').AsString;
  CurrentAccount.FBirthDate :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('birth_date').AsString;
  CurrentAccount.FGender := DBGrid_AccountList.DataSource.DataSet.FieldByName('gender').AsInteger;
  CurrentAccount.FHeight := DBGrid_AccountList.DataSource.DataSet.FieldByName('height').AsInteger;
  CurrentAccount.FWeight := DBGrid_AccountList.DataSource.DataSet.FieldByName('weight').AsFloat;

  Form_Account.ShowModal;

  FreeAndNil(CurrentAccount);
  Form_Main.UpdateAccountList();
  HideDBGridColumns();
  UpdateControls();
end;

procedure TForm_AccountList.Button_OpenClick(Sender: TObject);
begin
  Form_Main.CurrentAccount.FAccountNo :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('account_no').AsLongint;
  Form_Main.CurrentAccount.FName :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('name').AsString;
  Form_Main.CurrentAccount.FCreationDate :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('creation_date').AsString;
  Form_Main.CurrentAccount.FFirstName :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('first_name').AsString;
  Form_Main.CurrentAccount.FLastName :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('last_name').AsString;
  Form_Main.CurrentAccount.FBirthDate :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('birth_date').AsString;
  Form_Main.CurrentAccount.FGender :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('gender').AsInteger;
  Form_Main.CurrentAccount.FHeight :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('height').AsInteger;
  Form_Main.CurrentAccount.FWeight :=
    DBGrid_AccountList.DataSource.DataSet.FieldByName('weight').AsFloat;
  Close;
end;

procedure TForm_AccountList.CheckBox_ShowAtStartupChange(Sender: TObject);
begin
  if CheckBox_ShowAtStartup.Checked = True then
    Form_Main.ApplicationSettings.ShowAccountsAtStartup := True
  else
    Form_Main.ApplicationSettings.ShowAccountsAtStartup := False;
end;

procedure TForm_AccountList.Button_AddClick(Sender: TObject);
begin
  Form_Account.ShowModal;
  Form_Main.UpdateAccountList();
  HideDBGridColumns();
  UpdateControls();
end;

procedure TForm_AccountList.FormShow(Sender: TObject);
begin
  HideDBGridColumns();
  UpdateControls();
end;

end.
