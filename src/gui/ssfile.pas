{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit SSFile;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, sqlite3conn, FileUtil;

resourcestring
  { ----------------------------------- }
  { Database fields translation strings }
  { ----------------------------------- }

  { Gender type translation strings }
  SDBGender_None = 'None'; // Id. 0
  SDBGender_Female = 'Female'; // Id. 1
  SDBGender_Male = 'Male'; // Id. 2

  { Feelings types translation strings }
  SDBFeelingType_None = 'None';
  SDBFeelingType_VeryBad = 'Very bad';
  SDBFeelingType_Bad = 'Bad';
  SDBFeelingType_Neutral = 'Neutral';
  SDBFeelingType_Good = 'Good';
  SDBFeelingType_VeryGood = 'Very good';

  { Workout types translation strings }
  SDBWorkoutType_None = 'None';
  SDBWorkoutType_Bodybuilding = 'Bodybuilding';
  SDBWorkoutType_Fitness = 'Fitness';
  SDBWorkoutType_HomeCycling = 'Home cycling';
  SDBWorkoutType_MartialArt = 'Martial art';
  SDBWorkoutType_Running = 'Running';
  SDBWorkoutType_Swimming = 'Swimming';
  SDBWorkoutType_Walk = 'Walk';
  SDBWorkoutType_Yoga = 'Yoga';

  { Food units translation strings }
  SDBFoodUnit_Grams = 'Grams (g)';
  SDBFoodUnit_Milligrams = 'Milligrams (mg)';
  SDBFoodUnit_Teaspoon = 'Teaspoon (ts)';
  SDBFoodUnit_Tablespoon = 'Tablespoon (tas)';
  SDBFoodUnit_Tablet = 'Tablet (tab)';

  { Food categories translation strings}
  SDBFoodCategory_None = 'None';
  SDBFoodCategory_Starters = 'Starters and dishes';
  SDBFoodCategory_Fruits = 'Fruits, vegetables, legumes and nuts';
  SDBFoodCategory_Cereals = 'Cereal products';
  SDBFoodCategory_Meat = 'Meat, egg and fish';
  SDBFoodCategory_Milk = 'Milk and milk products';
  SDBFoodCategory_Beverages = 'Beverages';
  SDBFoodCategory_Sugar = 'Sugar and confectionery';
  DBFoodCategory_IceCream = 'Ice cream and sorbet';
  SDBFoodCategory_Fats = 'Fats and oils';
  SDBFoodCategory_Miscellaneous = 'Miscellaneous';
  SDBFoodCategory_Baby = 'Baby food';

  { Meal type translation strings }
  SDBMealType_None = 'None';
  SDBMealType_Breakfast = 'Breakfast';
  SDBMealType_Lunch = 'Lunch';
  SDBMealType_Dinner = 'Dinner';
  SDBMealType_Snack = 'Snack';

  { Global health translation strings }
  SDBGlobalHealth_Weight = 'Weight';
  SDBGlobalHealth_BMI = 'Body Mass Index';
  SDBGlobalHealth_Sleep = 'Sleep';
  SDBGlobalHealth_BloodPressure = 'Blood pressure';

  { Body measurements translation strings }
  SDBBodyMeasurements_Neck = 'Neck';
  SDBBodyMeasurements_Shoulders = 'Shoulders';
  SDBBodyMeasurements_Chest = 'Chest';
  SDBBodyMeasurements_MidSection = 'Mid section';
  SDBBodyMeasurements_Hips = 'Hips';
  SDBBodyMeasurements_Arms = 'Arms';
  SDBBodyMeasurements_Forearms = 'Forearms';
  SDBBodyMeasurements_Thighs = 'Thighs';
  SDBBodyMeasurements_Calves = 'Calves';

type

  { TSSFile }

  TSSFile = class
  public
    FMasterFullName: string; // StaySharp master database full name
    FMasterDBConnection: TSQLConnection; // StaySharp master database connection
    FMasterDBTransaction: TSQLTransaction;
    FFullName: string; // User database full name
    FDBConnection: TSQLConnection; // User database connection
    FDBTransaction: TSQLTransaction;
    constructor Create(FileName: string);
    procedure CreateFile(const LangLongCode: string; const Overwrite: boolean = False);
    function GetSchemaVersion(): string;
    procedure UpdateStaySharpVersion1(SourceFileName: string);
    procedure Close();

  private
    procedure CreateStaySharpSysTable();
    procedure CreateAccountTable();
    procedure CreateFeelingTypeTable();
    procedure CreateWorkoutTypeTable();
    procedure CreateWorkoutTable();
    procedure CreateFoodCategoryTable();
    procedure CreateFoodUnitTable();
    procedure CreateFoodTable(const LangLongCode: string);
    procedure CreateMealTypeTable();
    procedure CreateMealTable();
    procedure CreateMealItemTable();
    procedure CreateMeasurementTable();
    procedure CreatePictureTable();
  end;

implementation

uses
  main;

constructor TSSFile.Create(FileName: string);
begin
  inherited Create;

  FMasterFullName := 'staysharp.sqlite';
  FFullName := FileName;

  { Create master database connection }
  FMasterDBConnection := TSQLite3Connection.Create(nil);
  FMasterDBTransaction := TSQLTransaction.Create(FMasterDBConnection);
  FMasterDBConnection.Transaction := FMasterDBTransaction;
  FMasterDBConnection.DatabaseName := FMasterFullName;

  { Create user database connection }
  FDBConnection := TSQLite3Connection.Create(nil);
  FDBTransaction := TSQLTransaction.Create(FDBConnection);
  FDBConnection.Transaction := FDBTransaction;
  FDBConnection.DatabaseName := FFullName;
end;

procedure TSSFile.Close();
begin
  FreeAndNil(FDBTransaction);
  FreeAndNil(FDBConnection);

  FreeAndNil(FMasterDBTransaction);
  FreeAndNil(FMasterDBConnection);
end;

function TSSFile.GetSchemaVersion(): string;
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'SELECT schema_version FROM staysharpsys';
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.Open;
    Query.First;
    Result := Query.FieldByName('schema_version').AsString;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.UpdateStaySharpVersion1(SourceFileName: string);
var
  DBConnectionSrc: TSQLConnection;
  DBTransactionSrc: TSQLTransaction;
  QuerySrc: TSQLQuery;
  SQLSrc: string;
  Query: TSQLQuery;
  SQL: string;
  FloatFormatSettings: TFormatSettings;
begin
  FloatFormatSettings.DecimalSeparator := '.';

  { Initialize source database connection }
  DBConnectionSrc := TSQLite3Connection.Create(nil);
  DBTransactionSrc := TSQLTransaction.Create(DBConnectionSrc);
  DBConnectionSrc.Transaction := DBTransactionSrc;
  DBConnectionSrc.DatabaseName := SourceFileName;

  QuerySrc := TSQLQuery.Create(nil);
  Query := TSQLQuery.Create(nil);
  try
    QuerySrc.DataBase := DBConnectionSrc;
    Query.DataBase := FDBConnection;

    { -------------- }
    { Update Account }
    { -------------- }
    SQLSrc := 'SELECT * FROM account';
    QuerySrc.SQL.Clear;
    QuerySrc.SQL.Text := SQLSrc;
    QuerySrc.Open;

    QuerySrc.First;
    while not QuerySrc.EOF do
    begin
      SQL := 'INSERT INTO account (account_no,name,creation_date,first_name,last_name,birth_date,gender,height,weight) ' +
        'VALUES (:AccountNo,:Name,:CreationDate,:FirstName,:LastName,:BirthDate,:Gender,:Height,:Weight)';
      Query.SQL.Clear;
      Query.SQL.Text := SQL;
      Query.Params.ParamByName('AccountNo').AsInteger := QuerySrc.FieldByName('account_no').AsInteger;
      Query.Params.ParamByName('Name').AsString := QuerySrc.FieldByName('name').AsString;
      Query.Params.ParamByName('CreationDate').AsString := QuerySrc.FieldByName('creation_date').AsString;
      Query.Params.ParamByName('FirstName').AsString := QuerySrc.FieldByName('first_name').AsString;
      Query.Params.ParamByName('LastName').AsString := QuerySrc.FieldByName('last_name').AsString;
      Query.Params.ParamByName('BirthDate').AsString := QuerySrc.FieldByName('birth_date').AsString;
      Query.Params.ParamByName('Gender').AsInteger := QuerySrc.FieldByName('gender').AsInteger;
      Query.Params.ParamByName('Height').AsInteger := QuerySrc.FieldByName('height').AsInteger;
      Query.Params.ParamByName('Weight').AsFloat := QuerySrc.FieldByName('weight').AsFloat;
      Query.ExecSQL;
      QuerySrc.Next;
    end;
    FDBTransaction.Commit;
    QuerySrc.Close;

    { --------------- }
    { Update Workouts }
    { --------------- }
    SQLSrc := 'SELECT * FROM workout';
    QuerySrc.SQL.Clear;
    QuerySrc.SQL.Text := SQLSrc;
    QuerySrc.Open;

    QuerySrc.First;
    while not QuerySrc.EOF do
    begin
      SQL := 'INSERT INTO workout (workout_no,account_no,workout_type_no,creation_date,start_time,duration,min_cf,mean_cf,max_cf,calories,feeling_type_no,note) ' +
        'VALUES (:WorkoutNo,:AccountNo,:WorkoutTypeNo,:CreationDate,:StartTime,:Duration,:MinCF,:MeanCF,:MaxCF,:Calories,:FeelingTypeNo,:Note)';
      Query.SQL.Clear;
      Query.SQL.Text := SQL;
      Query.Params.ParamByName('WorkoutNo').AsInteger := QuerySrc.FieldByName('workout_no').AsInteger;
      Query.Params.ParamByName('AccountNo').AsInteger := QuerySrc.FieldByName('account_no').AsInteger;
      Query.Params.ParamByName('WorkoutTypeNo').AsInteger := QuerySrc.FieldByName('workout_type_no').AsInteger;
      Query.Params.ParamByName('CreationDate').AsString := QuerySrc.FieldByName('creation_date').AsString;
      Query.Params.ParamByName('StartTime').AsString := QuerySrc.FieldByName('start_time').AsString;
      Query.Params.ParamByName('Duration').AsString := QuerySrc.FieldByName('duration').AsString;
      Query.Params.ParamByName('MinCF').AsInteger := QuerySrc.FieldByName('min_cf').AsInteger;
      Query.Params.ParamByName('MeanCF').AsInteger := QuerySrc.FieldByName('mean_cf').AsInteger;
      Query.Params.ParamByName('MaxCF').AsInteger := QuerySrc.FieldByName('max_cf').AsInteger;
      Query.Params.ParamByName('Calories').AsInteger := QuerySrc.FieldByName('calories').AsInteger;
      Query.Params.ParamByName('FeelingTypeNo').AsInteger := QuerySrc.FieldByName('feeling_type_no').AsInteger;
      Query.Params.ParamByName('Note').AsString := QuerySrc.FieldByName('note').AsString;
      Query.ExecSQL;
      QuerySrc.Next;
    end;
    FDBTransaction.Commit;
    QuerySrc.Close;

    { ------------ }
    { Update Meals }
    { ------------ }
    SQLSrc := 'SELECT * FROM meal';
    QuerySrc.SQL.Clear;
    QuerySrc.SQL.Text := SQLSrc;
    QuerySrc.Open;

    QuerySrc.First;
    while not QuerySrc.EOF do
    begin
      SQL := 'INSERT INTO meal (meal_no,account_no,meal_type_no,creation_date,creation_time) ' + 'VALUES (:MealNo,:AccountNo,:MealTypeNo,:CreationDate,:CreationTime)';

      Query.SQL.Clear;
      Query.SQL.Text := SQL;
      Query.Params.ParamByName('MealNo').AsInteger := QuerySrc.FieldByName('meal_no').AsInteger;
      Query.Params.ParamByName('AccountNo').AsInteger := QuerySrc.FieldByName('account_no').AsInteger;
      Query.Params.ParamByName('MealTypeNo').AsInteger := QuerySrc.FieldByName('meal_type_no').AsInteger;
      Query.Params.ParamByName('CreationDate').AsString := QuerySrc.FieldByName('creation_date').AsString;
      Query.Params.ParamByName('CreationTime').AsString := QuerySrc.FieldByName('creation_time').AsString;
      Query.ExecSQL;
      QuerySrc.Next;
    end;
    FDBTransaction.Commit;
    QuerySrc.Close;

    { ---------------- }
    { Update MealItems }
    { ---------------- }
    SQLSrc := 'SELECT * FROM meal_item';
    QuerySrc.SQL.Clear;
    QuerySrc.SQL.Text := SQLSrc;
    QuerySrc.Open;

    QuerySrc.First;
    while not QuerySrc.EOF do
    begin
      SQL := 'INSERT OR REPLACE INTO meal_item(';
      SQL := SQL + 'meal_item_no,meal_no,quantity,food_no,name,energy,protein,carbohydrate,sugar,alcohol,fat,cholesterol,water,salt,calcium,iron,magnesium,phosphorus,potassium,sodium,zinc,beta_carotene,vitamin_d,vitamin_e,vitamin_k1,vitamin_k2,vitamin_c,vitamin_b1,vitamin_b2,vitamin_b3,vitamin_b5,vitamin_b6,vitamin_b9,vitamin_b12,fibres,polyols,organic_acids,unit_name,unit_quantity';
      SQL := SQL + ') ';
      SQL := SQL + 'VALUES';
      SQL := SQL + '(';
      SQL := SQL + QuerySrc.FieldByName('meal_item_no').AsString + ',';
      SQL := SQL + QuerySrc.FieldByName('meal_no').AsString + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('quantity').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + QuerySrc.FieldByName('food_no').AsString + ',';
      SQL := SQL + QuotedStr(QuerySrc.FieldByName('name').AsString) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('energy').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('protein').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('carbohydrate').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('sugar').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('alcohol').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('fat').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('cholesterol').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('water').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('salt').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('calcium').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('iron').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('magnesium').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('phosphorus').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('potassium').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('sodium').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('zinc').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('beta_carotene').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_d').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_e').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_k1').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_k2').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_c').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b1').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b2').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b3').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b5').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b6').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b9').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b12').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('fibres').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('polyols').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('organic_acids').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + QuotedStr(QuerySrc.FieldByName('unit_name').AsString) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('unit_quantity').AsFloat, FloatFormatSettings);
      SQL := SQL + ')';
      Query.SQL.Clear;
      Query.SQL.Text := SQL;
      Query.ExecSQL;
      QuerySrc.Next;
    end;
    FDBTransaction.Commit;
    QuerySrc.Close;

    { ----------------- }
    { Update user Foods }
    { ----------------- }
    SQLSrc := 'SELECT * FROM food WHERE user_defined = 1';
    QuerySrc.SQL.Clear;
    QuerySrc.SQL.Text := SQLSrc;
    QuerySrc.Open;

    QuerySrc.First;
    while not QuerySrc.EOF do
    begin
      SQL := 'INSERT INTO food (';
      SQL := SQL + 'food_no,name,food_category_no,energy,';
      SQL := SQL + 'protein,carbohydrate,sugar,alcohol,fat,fat_saturated,fat_monosaturated,fat_polysaturated,cholesterol,water,salt,';
      SQL := SQL + 'calcium,iron,magnesium,phosphorus,potassium,sodium,zinc,';
      SQL := SQL + 'beta_carotene,vitamin_d,vitamin_e,vitamin_k1,vitamin_k2,vitamin_c,vitamin_b1,vitamin_b2,vitamin_b3,vitamin_b5,vitamin_b6,vitamin_b9,vitamin_b12,';
      SQL := SQL + 'fibres,polyols,organic_acids,user_defined,food_unit_no,unit_quantity';
      SQL := SQL + ') VALUES (';
      SQL := SQL + QuerySrc.FieldByName('food_no').AsString + ',';
      SQL := SQL + QuotedStr(QuerySrc.FieldByName('name').AsString) + ',';
      SQL := SQL + QuerySrc.FieldByName('food_category_no').AsString + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('energy').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('protein').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('carbohydrate').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('sugar').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('alcohol').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('fat').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('fat_saturated').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('fat_monosaturated').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('fat_polysaturated').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('cholesterol').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('water').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('salt').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('calcium').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('iron').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('magnesium').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('phosphorus').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('potassium').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('sodium').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('zinc').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('beta_carotene').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_d').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_e').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_k1').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_k2').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_c').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b1').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b2').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b3').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b5').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b6').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b9').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('vitamin_b12').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('fibres').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('polyols').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('organic_acids').AsFloat, FloatFormatSettings) + ',';
      SQL := SQL + QuerySrc.FieldByName('user_defined').AsString + ',';
      SQL := SQL + QuerySrc.FieldByName('food_unit_no').AsString + ',';
      SQL := SQL + FloatToStr(QuerySrc.FieldByName('unit_quantity').AsFloat, FloatFormatSettings);
      SQL := SQL + ')';
      QuerySrc.Next;
    end;
    FDBTransaction.Commit;
    QuerySrc.Close;
  finally
    FreeAndNil(Query);
    FreeAndNil(QuerySrc);
  end;
end;

procedure TSSFile.CreateStaySharpSysTable();
var
  Query: TSQLQuery;
  SQL: string;
begin
  { Create user 'staysharpsys' table }
  SQL := 'CREATE TABLE IF NOT EXISTS staysharpsys (';
  SQL := SQL + 'params_id	INTEGER,';
  SQL := SQL + 'schema_version	VARCHAR ( 10 ) NOT NULL,';
  SQL := SQL + 'schema_creation_date	VARCHAR ( 10 ) NOT NULL,';
  SQL := SQL + 'PRIMARY KEY(params_id)';
  SQL := SQL + ');';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;

  { Populate user 'staysharpsys' table }
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;

    Query.SQL.Clear;
    Query.SQL.Text :=
      'INSERT INTO staysharpsys (params_id,schema_version,schema_creation_date) ' + 'VALUES (:ParamId,:SchemaVersion,:SchemaCreationDate)';
    Query.Params.ParamByName('ParamId').AsInteger := 0;
    Query.Params.ParamByName('SchemaVersion').AsString := StaySharpSchemaVersion;
    Query.Params.ParamByName('SchemaCreationDate').AsString := StaySharpSchemaVersionCreationDate;
    Query.ExecSQL;
    FDBTransaction.Commit;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateAccountTable();
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'CREATE TABLE IF NOT EXISTS account (';
  SQL := SQL + 'account_no	INTEGER,';
  SQL := SQL + 'name	VARCHAR ( 100 ) NOT NULL,';
  SQL := SQL + 'creation_date	VARCHAR ( 10 ) NOT NULL,';
  SQL := SQL + 'first_name	VARCHAR ( 100 ),';
  SQL := SQL + 'last_name	VARCHAR ( 100 ),';
  SQL := SQL + 'birth_date	VARCHAR ( 10 ),';
  SQL := SQL + 'gender	INTEGER,';
  SQL := SQL + 'height	INTEGER,';
  SQL := SQL + 'weight	REAL,';
  SQL := SQL + 'CONSTRAINT ct_name_unique UNIQUE(name),';
  SQL := SQL + 'PRIMARY KEY(account_no)';
  SQL := SQL + ')';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateFeelingTypeTable();
var
  Query: TSQLQuery;
  SQL: string;
  FeelingTypes: array of string;
  i: integer;
begin
  { Initialize feeling types }
  SetLength(FeelingTypes, 6);
  FeelingTypes[0] := SDBFeelingType_None;
  FeelingTypes[1] := SDBFeelingType_VeryBad;
  FeelingTypes[2] := SDBFeelingType_Bad;
  FeelingTypes[3] := SDBFeelingType_Neutral;
  FeelingTypes[4] := SDBFeelingType_Good;
  FeelingTypes[5] := SDBFeelingType_VeryGood;

  { Create 'feeling_type' table }
  SQL := 'CREATE TABLE IF NOT EXISTS feeling_type (';
  SQL := SQL + 'feeling_type_no	INTEGER,';
  SQL := SQL + 'name	VARCHAR ( 20 ) NOT NULL,';
  SQL := SQL + 'user_defined	INTEGER NOT NULL,';
  SQL := SQL + 'PRIMARY KEY(feeling_type_no)';
  SQL := SQL + ');';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;

  { Populate 'feeling_type' table }
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    for i := 0 to Length(FeelingTypes) - 1 do
    begin
      Query.SQL.Clear;
      Query.SQL.Text :=
        'INSERT INTO feeling_type (feeling_type_no,name,user_defined) VALUES (:FeelingTypeNo,:Name,:UserDefined)';
      Query.Params.ParamByName('FeelingTypeNo').AsInteger := i;
      Query.Params.ParamByName('Name').AsString := FeelingTypes[i];
      Query.Params.ParamByName('UserDefined').AsInteger := 0;
      Query.ExecSQL;
      FDBTransaction.Commit;
    end;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateWorkoutTypeTable();
var
  Query: TSQLQuery;
  SQL: string;
  WorkoutTypes: array of string;
  i: integer;
begin
  { Initialize }
  SetLength(WorkoutTypes, 9);
  WorkoutTypes[0] := SDBWorkoutType_None;
  WorkoutTypes[1] := SDBWorkoutType_Bodybuilding;
  WorkoutTypes[2] := SDBWorkoutType_Fitness;
  WorkoutTypes[3] := SDBWorkoutType_HomeCycling;
  WorkoutTypes[4] := SDBWorkoutType_MartialArt;
  WorkoutTypes[5] := SDBWorkoutType_Running;
  WorkoutTypes[6] := SDBWorkoutType_Swimming;
  WorkoutTypes[7] := SDBWorkoutType_Walk;
  WorkoutTypes[8] := SDBWorkoutType_Yoga;

  { Create 'workout_type' table }
  SQL := 'CREATE TABLE IF NOT EXISTS workout_type (';
  SQL := SQL + 'workout_type_no	INTEGER,';
  SQL := SQL + 'name	VARCHAR ( 20 ) NOT NULL,';
  SQL := SQL + 'user_defined	INTEGER NOT NULL,';
  SQL := SQL + 'PRIMARY KEY(workout_type_no)';
  SQL := SQL + ');';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;

  { Populate 'workout_type' table from 'workout_type_trans' table }
  // Populate 'workout_type'
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;

    for i := 0 to Length(WorkoutTypes) - 1 do
    begin
      Query.SQL.Clear;
      Query.SQL.Text :=
        'INSERT INTO workout_type (workout_type_no,name,user_defined) VALUES (:WorkoutTypeNo,:Name,:UserDefined)';
      Query.Params.ParamByName('WorkoutTypeNo').AsInteger := i;
      Query.Params.ParamByName('Name').AsString := WorkoutTypes[i];
      Query.Params.ParamByName('UserDefined').AsInteger := 0;
      Query.ExecSQL;
      FDBTransaction.Commit;
    end;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateWorkoutTable();
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'CREATE TABLE IF NOT EXISTS workout (';
  SQL := SQL + 'workout_no	INTEGER,';
  SQL := SQL + 'account_no	INTEGER NOT NULL,';
  SQL := SQL + 'workout_type_no	INTEGER NOT NULL,';
  SQL := SQL + 'creation_date	VARCHAR ( 10 ) NOT NULL,';
  SQL := SQL + 'start_time	VARCHAR ( 8 ) NOT NULL,';
  SQL := SQL + 'duration	VARCHAR ( 8 ) NOT NULL,';
  SQL := SQL + 'min_cf	INTEGER,';
  SQL := SQL + 'mean_cf	INTEGER,';
  SQL := SQL + 'max_cf	INTEGER,';
  SQL := SQL + 'calories	INTEGER,';
  SQL := SQL + 'feeling_type_no	INTEGER NOT NULL,';
  SQL := SQL + 'note	VARCHAR ( 10000 ),';
  SQL := SQL + 'PRIMARY KEY(workout_no),';
  SQL := SQL + 'FOREIGN KEY(workout_type_no) REFERENCES workout_type(workout_type_no),';
  SQL := SQL + 'FOREIGN KEY(account_no) REFERENCES account(account_no)';
  SQL := SQL + 'FOREIGN KEY(feeling_type_no) REFERENCES feeling_type(feeling_type_no)';
  SQL := SQL + ')';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateFoodCategoryTable();
var
  FoodCategories: array of string;
begin
  SetLength(FoodCategories, 12);
  FoodCategories[0] := SDBFoodCategory_None;
  FoodCategories[1] := SDBFoodCategory_Starters;
  FoodCategories[2] := SDBFoodCategory_Fruits;
  FoodCategories[3] := SDBFoodCategory_Cereals;
  FoodCategories[4] := SDBFoodCategory_Meat;
  FoodCategories[5] := SDBFoodCategory_Milk;
  FoodCategories[6] := SDBFoodCategory_Beverages;
  FoodCategories[7] := SDBFoodCategory_Sugar;
  FoodCategories[8] := DBFoodCategory_IceCream;
  FoodCategories[9] := SDBFoodCategory_Fats;
  FoodCategories[10] := SDBFoodCategory_Miscellaneous;
  FoodCategories[11] := SDBFoodCategory_Baby;

  { TODO : Food category table }
end;

procedure TSSFile.CreateFoodUnitTable();
var
  Query: TSQLQuery;
  SQL: string;
  FoodUnits: array of string;
  i: integer;
begin
  { Initialize food units }
  SetLength(FoodUnits, 5);
  FoodUnits[0] := SDBFoodUnit_Grams;
  FoodUnits[1] := SDBFoodUnit_Milligrams;
  FoodUnits[2] := SDBFoodUnit_Teaspoon;
  FoodUnits[3] := SDBFoodUnit_Tablespoon;
  FoodUnits[4] := SDBFoodUnit_Tablet;

  { Create 'food_unit' table }
  SQL := 'CREATE TABLE IF NOT EXISTS food_unit (';
  SQL := SQL + 'food_unit_no INTEGER,';
  SQL := SQL + 'name	VARCHAR ( 50 ) NOT NULL,';
  SQL := SQL + 'user_defined INTEGER NOT NULL,';
  SQL := SQL + 'PRIMARY KEY(food_unit_no)';
  SQL := SQL + ');';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;

  { Populate 'food_unit' table from 'food_unit_trans' table }
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;

    for i := 0 to Length(FoodUnits) - 1 do
    begin
      Query.SQL.Clear;
      Query.SQL.Text :=
        'INSERT INTO food_unit (food_unit_no,name,user_defined) VALUES (:FoodUnitNo,:Name,:UserDefined)';
      Query.Params.ParamByName('FoodUnitNo').AsInteger := i;
      Query.Params.ParamByName('Name').AsString := FoodUnits[i];
      Query.Params.ParamByName('UserDefined').AsInteger := 0;
      Query.ExecSQL;
      FDBTransaction.Commit;
    end;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateFoodTable(const LangLongCode: string);
var
  SelectQuery: TSQLQuery;
  Query: TSQLQuery;
  SQLCreateTable: string;
  SQLSelect: string;
  SQLInsert: string;
  FloatFormatSettings: TFormatSettings;
begin
  FloatFormatSettings.DecimalSeparator := '.';

  SQLCreateTable := 'CREATE TABLE IF NOT EXISTS `food` (';
  SQLCreateTable := SQLCreateTable + '`food_no`	INTEGER,';
  SQLCreateTable := SQLCreateTable + '`name`	VARCHAR ( 200 ) NOT NULL,';
  SQLCreateTable := SQLCreateTable + '`food_category_no`	INTEGER,';
  SQLCreateTable := SQLCreateTable + '`energy`	REAL,';
  SQLCreateTable := SQLCreateTable + '`protein`	REAL,';
  SQLCreateTable := SQLCreateTable + '`carbohydrate`	REAL,';
  SQLCreateTable := SQLCreateTable + '`sugar`	REAL,';
  SQLCreateTable := SQLCreateTable + '`alcohol`	REAL,';
  SQLCreateTable := SQLCreateTable + '`fat`	REAL,';
  SQLCreateTable := SQLCreateTable + '`fat_saturated`	REAL,';
  SQLCreateTable := SQLCreateTable + '`fat_monosaturated`	REAL,';
  SQLCreateTable := SQLCreateTable + '`fat_polysaturated`	REAL,';
  SQLCreateTable := SQLCreateTable + '`cholesterol`	REAL,';
  SQLCreateTable := SQLCreateTable + '`water`	REAL,';
  SQLCreateTable := SQLCreateTable + '`salt`	REAL,';
  SQLCreateTable := SQLCreateTable + '`calcium`	REAL,';
  SQLCreateTable := SQLCreateTable + '`iron`	REAL,';
  SQLCreateTable := SQLCreateTable + '`magnesium`	REAL,';
  SQLCreateTable := SQLCreateTable + '`phosphorus`	REAL,';
  SQLCreateTable := SQLCreateTable + '`potassium`	REAL,';
  SQLCreateTable := SQLCreateTable + '`sodium`	REAL,';
  SQLCreateTable := SQLCreateTable + '`zinc`	REAL,';
  SQLCreateTable := SQLCreateTable + '`beta_carotene`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_d`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_e`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_k1`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_k2`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_c`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_b1`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_b2`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_b3`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_b5`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_b6`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_b9`	REAL,';
  SQLCreateTable := SQLCreateTable + '`vitamin_b12`	REAL,';
  SQLCreateTable := SQLCreateTable + '`fibres`	REAL,';
  SQLCreateTable := SQLCreateTable + '`polyols`	REAL,';
  SQLCreateTable := SQLCreateTable + '`organic_acids`	REAL,';
  SQLCreateTable := SQLCreateTable + '`user_defined`	INTEGER,';
  SQLCreateTable := SQLCreateTable + '`food_unit_no`	INTEGER,';
  SQLCreateTable := SQLCreateTable + '`unit_quantity`	REAL,';
  SQLCreateTable := SQLCreateTable + 'PRIMARY KEY(`food_no`),';
  SQLCreateTable := SQLCreateTable + 'FOREIGN KEY(`food_unit_no`) REFERENCES `food_unit`(`food_unit_no`)';
  SQLCreateTable := SQLCreateTable + ')';

  if LangLongCode = 'fr_fr' then
    SQLSelect := 'SELECT * FROM food_fr ORDER BY food_no'
  else
    SQLSelect := 'SELECT * FROM food ORDER BY food_no';

  { 1. Create food table in the user file }
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQLCreateTable;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;

  { 2. Select all foods in StaySharp master database }
  SelectQuery := TSQLQuery.Create(nil);
  try
    SelectQuery.DataBase := FMasterDBConnection;
    SelectQuery.SQL.Clear;
    SelectQuery.SQL.Text := SQLSelect;
    SelectQuery.Open;

    { 3. Insert all foods in the user file }
    Query := TSQLQuery.Create(nil);
    try
      Query.DataBase := FDBConnection;
      SelectQuery.First;
      while not SelectQuery.EOF do
      begin
        SQLInsert := 'INSERT INTO food (';
        SQLInsert := SQLInsert + 'food_no,name,food_category_no,energy,';
        SQLInsert := SQLInsert + 'protein,carbohydrate,sugar,alcohol,fat,fat_saturated,fat_monosaturated,fat_polysaturated,cholesterol,water,salt,';
        SQLInsert := SQLInsert + 'calcium,iron,magnesium,phosphorus,potassium,sodium,zinc,';
        SQLInsert := SQLInsert + 'beta_carotene,vitamin_d,vitamin_e,vitamin_k1,vitamin_k2,vitamin_c,vitamin_b1,vitamin_b2,vitamin_b3,vitamin_b5,vitamin_b6,vitamin_b9,vitamin_b12,';
        SQLInsert := SQLInsert + 'fibres,polyols,organic_acids,user_defined,food_unit_no,unit_quantity';
        SQLInsert := SQLInsert + ') VALUES (';
        SQLInsert := SQLInsert + SelectQuery.FieldByName('food_no').AsString + ',';
        SQLInsert := SQLInsert + QuotedStr(SelectQuery.FieldByName('name').AsString) + ',';
        SQLInsert := SQLInsert + SelectQuery.FieldByName('food_category_no').AsString + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('energy').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('protein').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('carbohydrate').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('sugar').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('alcohol').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('fat').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('fat_saturated').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('fat_monosaturated').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('fat_polysaturated').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('cholesterol').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('water').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('salt').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('calcium').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('iron').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('magnesium').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('phosphorus').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('potassium').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('sodium').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('zinc').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('beta_carotene').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_d').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_e').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_k1').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_k2').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_c').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_b1').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_b2').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_b3').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_b5').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_b6').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_b9').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('vitamin_b12').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('fibres').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('polyols').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('organic_acids').AsFloat, FloatFormatSettings) + ',';
        SQLInsert := SQLInsert + SelectQuery.FieldByName('user_defined').AsString + ',';
        SQLInsert := SQLInsert + SelectQuery.FieldByName('food_unit_no').AsString + ',';
        SQLInsert := SQLInsert + FloatToStr(SelectQuery.FieldByName('unit_quantity').AsFloat, FloatFormatSettings);
        SQLInsert := SQLInsert + ')';

        Query.SQL.Clear;
        Query.SQL.Text := SQLInsert;
        Query.ExecSQL;
        SelectQuery.Next;
      end;
      FDBTransaction.Commit;
    finally
      FreeAndNil(Query);
    end;
  finally
    FreeAndNil(SelectQuery);
  end;
end;

procedure TSSFile.CreateMealTypeTable();
var
  Query: TSQLQuery;
  SQL: string;
  MealTypes: array of string;
  i: integer;
begin
  { Initialize array }
  SetLength(MealTypes, 5);
  MealTypes[0] := SDBMealType_None;
  MealTypes[1] := SDBMealType_Breakfast;
  MealTypes[2] := SDBMealType_Lunch;
  MealTypes[3] := SDBMealType_Dinner;
  MealTypes[4] := SDBMealType_Snack;

  { Create 'meal_type' table }
  SQL := 'CREATE TABLE IF NOT EXISTS meal_type (';
  SQL := SQL + 'meal_type_no INTEGER,';
  SQL := SQL + 'name VARCHAR ( 100 ) NOT NULL,';
  SQL := SQL + 'user_defined INTEGER NOT NULL,';
  SQL := SQL + 'PRIMARY KEY(meal_type_no)';
  SQL := SQL + ');';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;

  { Populate 'meal_type' table }
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;

    for i := 0 to Length(MealTypes) - 1 do
    begin
      Query.SQL.Clear;
      Query.SQL.Text :=
        'INSERT INTO meal_type (meal_type_no,name,user_defined) VALUES (:MealTypeNo,:Name,:UserDefined)';
      Query.Params.ParamByName('MealTypeNo').AsInteger := i;
      Query.Params.ParamByName('Name').AsString := MealTypes[i];
      Query.Params.ParamByName('UserDefined').AsInteger := 0;
      Query.ExecSQL;
      FDBTransaction.Commit;
    end;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateMealTable();
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'CREATE TABLE IF NOT EXISTS meal (';
  SQL := SQL + 'meal_no	INTEGER,';
  SQL := SQL + 'account_no	INTEGER,';
  SQL := SQL + 'meal_type_no	INTEGER,';
  SQL := SQL + 'creation_date	VARCHAR ( 10 ),';
  SQL := SQL + 'creation_time	VARCHAR ( 8 ),';
  SQL := SQL + 'FOREIGN KEY(meal_type_no) REFERENCES meal_type(meal_type_no),';
  SQL := SQL + 'PRIMARY KEY(meal_no)';
  SQL := SQL + ')';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateMealItemTable();
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'CREATE TABLE IF NOT EXISTS meal_item (';
  SQL := SQL + 'meal_item_no INTEGER,';
  SQL := SQL + 'meal_no INTEGER,';
  SQL := SQL + 'quantity REAL,';
  SQL := SQL + 'food_no INTEGER,';
  SQL := SQL + 'name VARCHAR ( 200 ),';
  SQL := SQL + 'energy REAL,';
  SQL := SQL + 'protein REAL,';
  SQL := SQL + 'carbohydrate REAL,';
  SQL := SQL + 'sugar REAL,';
  SQL := SQL + 'alcohol	REAL,';
  SQL := SQL + 'fat	REAL,';
  SQL := SQL + 'cholesterol	REAL,';
  SQL := SQL + 'water REAL,';
  SQL := SQL + 'salt REAL,';
  SQL := SQL + 'calcium	REAL,';
  SQL := SQL + 'iron REAL,';
  SQL := SQL + 'magnesium	REAL,';
  SQL := SQL + 'phosphorus REAL,';
  SQL := SQL + 'potassium REAL,';
  SQL := SQL + 'sodium REAL,';
  SQL := SQL + 'zinc REAL,';
  SQL := SQL + 'beta_carotene REAL,';
  SQL := SQL + 'vitamin_d REAL,';
  SQL := SQL + 'vitamin_e REAL,';
  SQL := SQL + 'vitamin_k1 REAL,';
  SQL := SQL + 'vitamin_k2 REAL,';
  SQL := SQL + 'vitamin_c REAL,';
  SQL := SQL + 'vitamin_b1 REAL,';
  SQL := SQL + 'vitamin_b2 REAL,';
  SQL := SQL + 'vitamin_b3 REAL,';
  SQL := SQL + 'vitamin_b5 REAL,';
  SQL := SQL + 'vitamin_b6 REAL,';
  SQL := SQL + 'vitamin_b9 REAL,';
  SQL := SQL + 'vitamin_b12 REAL,';
  SQL := SQL + 'fibres REAL,';
  SQL := SQL + 'polyols REAL,';
  SQL := SQL + 'organic_acids REAL,';
  SQL := SQL + 'unit_name	VARCHAR ( 50 ),';
  SQL := SQL + 'unit_quantity REAL,';
  SQL := SQL + 'PRIMARY KEY(meal_item_no),';
  SQL := SQL + 'FOREIGN KEY(meal_no) REFERENCES meal(meal_no),';
  SQL := SQL + 'FOREIGN KEY(food_no) REFERENCES food(food_no)';
  SQL := SQL + ')';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateMeasurementTable();
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'CREATE TABLE IF NOT EXISTS measurement (';
  SQL := SQL + 'measurement_no	INTEGER,';
  SQL := SQL + 'account_no INTEGER NOT NULL,';
  SQL := SQL + 'creation_date	VARCHAR ( 10 ) NOT NULL,';
  SQL := SQL + 'weight REAL,';
  SQL := SQL + 'body_mass_index REAL,';
  SQL := SQL + 'sleep REAL,';
  SQL := SQL + 'rest_heart_rate INTEGER,';
  SQL := SQL + 'blood_pressure_systolic INTEGER,';
  SQL := SQL + 'blood_pressure_diastolic INTEGER,';
  SQL := SQL + 'neck REAL,';
  SQL := SQL + 'shoulders REAL,';
  SQL := SQL + 'chest REAL,';
  SQL := SQL + 'arm_left REAL,';
  SQL := SQL + 'arm_right REAL,';
  SQL := SQL + 'forearm_left REAL,';
  SQL := SQL + 'forearm_right REAL,';
  SQL := SQL + 'mid_section REAL,';
  SQL := SQL + 'hips REAL,';
  SQL := SQL + 'thigh_left REAL,';
  SQL := SQL + 'thigh_right REAL,';
  SQL := SQL + 'calf_left REAL,';
  SQL := SQL + 'calf_right REAL,';
  SQL := SQL + 'FOREIGN KEY(account_no) REFERENCES account(account_no),';
  SQL := SQL + 'PRIMARY KEY(measurement_no)';
  SQL := SQL + ')';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreatePictureTable();
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'CREATE TABLE IF NOT EXISTS picture (';
  SQL := SQL + 'picture_no	INTEGER,';
  SQL := SQL + 'account_no	INTEGER NOT NULL,';
  SQL := SQL + 'creation_date	VARCHAR ( 10 ),';
  SQL := SQL + 'layout_id	INTEGER NOT NULL,';
  SQL := SQL + 'dataBLOB BLOB NOT NULL,';
  SQL := SQL + 'FOREIGN KEY(account_no) REFERENCES account(account_no),';
  SQL := SQL + 'PRIMARY KEY(picture_no)';
  SQL := SQL + ')';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := FDBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    FDBTransaction.Commit;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFile.CreateFile(const LangLongCode: string; const Overwrite: boolean = False);
begin
  try
    if Overwrite = True then
      DeleteFile(FFullName);

    { Create user tables }
    CreateStaySharpSysTable();
    CreateAccountTable();
    CreateFeelingTypeTable();
    CreateWorkoutTypeTable();
    CreateWorkoutTable();
    CreateFoodCategoryTable();
    CreateFoodUnitTable();
    CreateFoodTable(LangLongCode);
    CreateMealTypeTable();
    CreateMealTable();
    CreateMealItemTable();
    CreateMeasurementTable();
    CreatePictureTable();

    FDBTransaction.Commit;
  finally
  end;
end;

end.
