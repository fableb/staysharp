{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DateUtils, FileUtil, TAGraph, TASources, TASeries, Forms,
  Controls, Graphics, Dialogs, Menus, ExtCtrls, StdCtrls, Calendar, ComCtrls,
  DBCtrls, DBGrids, ActnList, EditBtn, sqlite3conn, sqldb, DB,
  DefaultTranslator, LCLTranslator, LazHelpHTML, DOM, XMLWrite,
  gettext, LCLIntf, SSAccount, SSWorkout, SSMeal, SSFood, SSFile,
  SSApplicationSettings, SSMeasurement, SSPicture, DateTimePicker, mrumanager
{$IFDEF Win32}
  , shlobj;
{$ELSE}
  , TACustomSeries;
{$ENDIF}

resourcestring

  { ------------------------ }
  { Hint translation strings }
  { ------------------------ }
  SHintExpand = 'Expand';
  SHintRestore = 'Restore';

  { -------------------------- }
  { Dialog translation strings }
  { -------------------------- }
  SDlgNotImplemented = 'Not implemented!';
  SDlgOverwriteFileMsg = 'already exists. Do you want to overwrite it?';

  SDlgNewStaySharpFileTitle = 'New StaySharp file';

  SDlgNewWorkoutTitle = 'New workout';
  SDlgModifyWorkoutTitle = 'Modify workout';
  SDlgDeleteWorkoutTitle = 'Delete workout';
  SDlgDeleteWorkoutMsg = 'Do you want to delete this workout?';
  SDlgBtnYes = 'Yes';
  SDlgBtnNo = 'No';

  SDlgNewAccountTitle = 'New account';
  SDlgModifyAccountTitle = 'Modify account';
  SDlgDeleteAccountTitle = 'Delete account';
  SDlgDeleteAccountMsg1 =
    'This account is opened. It cannot be deleted. Please close the account before deletion.';
  SDlgDeleteAccountMsg2 =
    'Do you want to delete this account? ALL your DATA will be LOST!';

  SDlgDeleteMealTitle = 'Delete meal';
  SDlgDeleteMealMsg = 'Do you want to delete this meal?';

  SDlgDeletePictureTitle = 'Delete picture';
  SDlgDeletePictureMsg = 'Do you want to delete this picture?';

  SDlgNewFoodTitle = 'Add food';
  SDlgModifyFoodTitle = 'Modify food';
  SDlgDeleteFoodTitle = 'Delete food';
  SDlgDeleteFoodMsg = 'Do you want to delete this food?';
  SDlgFoodEnergyTitle = 'Computed energy';
  SDlgFoodEnergyMsg1 = 'The computed energy (calories) from your nutrients is: ';
  SDlgFoodEnergyMsg2 = 'Do you want to use this value?';

  SDlgDeleteMeasurementsTitle = 'Delete measurements';
  SDlgDeleteMeasurementsMsg = 'Do you want to delete all measurements?';

  SDlgExportTitle = 'Export file';
  SDlgImportTitle = 'Import file';

  SDlgImageTypeError = 'This image type is not supported. Supported image type are JPEG or JPG and PNG.';
  SDlgImageSizeError1 = 'Your picture size is too big. Picture size is limited to ';
  SDlgImageSizeError2 = ' ko.';

  { Pie chart translation strings }
  SCalorieBreakdown = 'Calorie Breakdown';
  SProtein = 'Protein';
  SCarbohydrate = 'Carbohydrate';
  SFat = 'Fat';

type

  { TForm_Main }

  TForm_Main = class(TForm)
    Button_AddMeal: TButton;
    Button_AddPicture1: TButton;
    Button_AddPicture2: TButton;
    Button_AddPicture3: TButton;
    Button_AddPicture4: TButton;
    Button_AddWorkout: TButton;
    Button_DeleteMeal: TButton;
    Button_DeletePicture1: TButton;
    Button_DeletePicture2: TButton;
    Button_DeletePicture3: TButton;
    Button_DeletePicture4: TButton;
    Button_DeleteWorkout: TButton;
    Button_EditMeal: TButton;
    Button_EditWorkout: TButton;
    Button_Measurements_Delete: TButton;
    Button_Measurements_Edit: TButton;
    Button_Today: TButton;
    Calendar1: TCalendar;
    Chart_BodyMeasurementsLineSeries_CalfRight: TLineSeries;
    Chart_BodyMeasurementsLineSeries_CalfLeft: TLineSeries;
    Chart_BodyMeasurementsLineSeries_ThighRight: TLineSeries;
    Chart_BodyMeasurementsLineSeries_ThighLeft: TLineSeries;
    Chart_BodyMeasurementsLineSeries_Hips: TLineSeries;
    Chart_BodyMeasurementsLineSeries_MidSection: TLineSeries;
    Chart_BodyMeasurementsLineSeries_ForearmRight: TLineSeries;
    Chart_BodyMeasurementsLineSeries_ForearmLeft: TLineSeries;
    Chart_BodyMeasurementsLineSeries_ArmRight: TLineSeries;
    Chart_BodyMeasurementsLineSeries_ArmLeft: TLineSeries;
    Chart_BodyMeasurementsLineSeries_Chest: TLineSeries;
    Chart_BodyMeasurementsLineSeries_Shoulders: TLineSeries;
    Chart_BodyMeasurementsLineSeries_Neck: TLineSeries;
    Chart_GlobalHealth: TChart;
    Chart_GlobalHealthLineSeries_BMILimit4: TLineSeries;
    Chart_GlobalHealthLineSeries_BMILimit3: TLineSeries;
    Chart_GlobalHealthLineSeries_BMILimit2: TLineSeries;
    Chart_GlobalHealthLineSeries_BMILimit1: TLineSeries;
    Chart_GlobalHealthLineSeries_Diastolic: TLineSeries;
    Chart_GlobalHealthLineSeries_Systolic: TLineSeries;
    Chart_GlobalHealthLineSeries_Sleep: TLineSeries;
    Chart_GlobalHealthLineSeries_BodyMassIndex: TLineSeries;
    Chart_GlobalHealthLineSeries_Weight: TLineSeries;
    Chart_BodyMeasurements: TChart;
    Chart_DailyCalories: TChart;
    Chart_DailyCaloriesSlice: TPieSeries;
    Chart_MonthlyCalories: TChart;
    Chart_MonthlyCaloriesSlice: TPieSeries;
    Chart_WeeklyCalories: TChart;
    Chart_WeeklyCaloriesSlice: TPieSeries;
    ComboBox_BodyMeasurementsCategory: TComboBox;
    ComboBox_GlobalHealthCategory: TComboBox;
    DataSource_MealItems: TDataSource;
    DataSource_Meals: TDataSource;
    DataSource_Workout: TDataSource;
    DataSource_Account: TDataSource;
    DateTimePicker_BodyMeasurementsEndDate: TDateTimePicker;
    DateTimePicker_BodyMeasurementsStartDate: TDateTimePicker;
    DateTimePicker_GlobalHealthEndDate: TDateTimePicker;
    DateTimePicker_GlobalHealthStartDate: TDateTimePicker;
    DBGrid_MealItemList: TDBGrid;
    DBGrid_MealList: TDBGrid;
    DBGrid_Workouts: TDBGrid;
    DBMemo_Note: TDBMemo;
    GroupBox_AccountInfo: TGroupBox;
    GroupBox_BloodPressure: TGroupBox;
    GroupBox_BodyMeasurements: TGroupBox;
    GroupBox_BodyMeasurementsDateRange: TGroupBox;
    GroupBox_BodyMeasurementsStatistics: TGroupBox;
    GroupBox_CaloricBalance: TGroupBox;
    GroupBox_DailyWorkouts: TGroupBox;
    GroupBox_GlobalHealth: TGroupBox;
    GroupBox_GlobalHealthDateRange: TGroupBox;
    GroupBox_GlobalHealthStatistics: TGroupBox;
    GroupBox_MacroNutrients: TGroupBox;
    GroupBox_Minerals: TGroupBox;
    GroupBox_MonthlyCaloricBalance: TGroupBox;
    GroupBox_MonthlyWorkouts: TGroupBox;
    GroupBox_Picture1: TGroupBox;
    GroupBox_Picture2: TGroupBox;
    GroupBox_Picture3: TGroupBox;
    GroupBox_Picture4: TGroupBox;
    GroupBox_TotalNutrientsFromMeals: TGroupBox;
    GroupBox_Vitamins: TGroupBox;
    GroupBox_WeeklyCaloricBalance: TGroupBox;
    GroupBox_WeeklyWorkouts: TGroupBox;
    HTMLBrowserHelpViewer1: THTMLBrowserHelpViewer;
    HTMLHelpDatabase1: THTMLHelpDatabase;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    ImageList1: TImageList;
    Image_Measurements: TImage;
    Label_Alcohol: TLabel;
    Label_BetaCarotene: TLabel;
    Label_BMIInterpretation: TLabel;
    Label_BodyMeasurementsCategory: TLabel;
    Label_BodyMeasurementsEndDate: TLabel;
    Label_BodyMeasurementsStartDate: TLabel;
    Label_Calcium: TLabel;
    Label_Carbohydrates: TLabel;
    Label_Cholesterol: TLabel;
    Label_Energy: TLabel;
    Label_Fats: TLabel;
    Label_Fibres: TLabel;
    Label_GlobalHealthCategory: TLabel;
    Label_GlobalHealthEndDate: TLabel;
    Label_GlobalHealthStartDate: TLabel;
    Label_AmountCaloricBalance: TLabel;
    Label_AmountDailyBMR: TLabel;
    Label_AmountDailyCarbohydrates: TLabel;
    Label_AmountDailyExpenses: TLabel;
    Label_AmountDailyFat: TLabel;
    Label_AmountDailyMealCalories: TLabel;
    Label_AmountDailyNumberWorkouts: TLabel;
    Label_AmountDailyProteins: TLabel;
    Label_AmountDailyWorkoutCalories: TLabel;
    Label_AmountDailyWorkoutTime: TLabel;
    Label_AmountMonthlyBMR: TLabel;
    Label_AmountMonthlyCaloricBalance: TLabel;
    Label_AmountMonthlyCarbohydrates: TLabel;
    Label_AmountMonthlyExpenses: TLabel;
    Label_AmountMonthlyFat: TLabel;
    Label_AmountMonthlyMealCalories: TLabel;
    Label_AmountMonthlyNumberWorkouts: TLabel;
    Label_AmountMonthlyProteins: TLabel;
    Label_AmountMonthlyWorkoutCalories: TLabel;
    Label_AmountMonthlyWorkoutTime: TLabel;
    Label_AmountWeeklyBMR: TLabel;
    Label_AmountWeeklyCaloricBalance: TLabel;
    Label_AmountWeeklyCarbohydrates: TLabel;
    Label_AmountWeeklyExpenses: TLabel;
    Label_AmountWeeklyFat: TLabel;
    Label_AmountWeeklyMealCalories: TLabel;
    Label_AmountWeeklyNumberWorkouts: TLabel;
    Label_AmountWeeklyProteins: TLabel;
    Label_AmountWeeklyWorkoutCalories: TLabel;
    Label_AmountWeeklyWorkoutTime: TLabel;
    Label_BMRValue: TLabel;
    Label_DailyBalance: TLabel;
    Label_DailyBalance1: TLabel;
    Label_DailyBMR: TLabel;
    Label_DailyCaloricBalance: TLabel;
    Label_DailyExpenses: TLabel;
    Label_DailyExpenses1: TLabel;
    Label_DailyExpenses2: TLabel;
    Label_DailyIntakes: TLabel;
    Label_DailyIntakes1: TLabel;
    Label_DailyIntakes2: TLabel;
    Label_DailyMealCalories: TLabel;
    Label_DailyMealCalories1: TLabel;
    Label_DailyMealCarbohydrates: TLabel;
    Label_DailyMealCarbohydrates1: TLabel;
    Label_DailyMealFats: TLabel;
    Label_DailyMealFats1: TLabel;
    Label_DailyMealProteins: TLabel;
    Label_DailyMealProteins1: TLabel;
    Label_DailyNumberWorkouts: TLabel;
    Label_DailyNumberWorkouts2: TLabel;
    Label_Iron: TLabel;
    Label_Left: TLabel;
    Label_Magnesium: TLabel;
    Label_MealItems: TLabel;
    Label_MealList: TLabel;
    Label_Measure_ArmLeft: TLabel;
    Label_Measure_ArmRight: TLabel;
    Label_Measure_BMI: TLabel;
    Label_Measure_BMIValue: TLabel;
    Label_Measure_CalfLeft: TLabel;
    Label_Measure_CalfRight: TLabel;
    Label_Measure_Chest: TLabel;
    Label_Measure_Diastolic: TLabel;
    Label_Measure_DiastolicValue: TLabel;
    Label_Measure_ForearmLeft: TLabel;
    Label_Measure_ForearmRight: TLabel;
    Label_Measure_Hips: TLabel;
    Label_Measure_MidSection: TLabel;
    Label_Measure_Neck: TLabel;
    Label_Measure_RestHeartRate: TLabel;
    Label_Measure_RestHeartRateValue: TLabel;
    Label_Measure_Shoulders: TLabel;
    Label_Measure_Sleep: TLabel;
    Label_Measure_SleepValue: TLabel;
    Label_Measure_Systolic: TLabel;
    Label_Measure_SystolicValue: TLabel;
    Label_Measure_ThighLeft: TLabel;
    Label_Measure_ThighRight: TLabel;
    Label_Measure_Weight: TLabel;
    Label_Measure_WeightValue: TLabel;
    Label_MonthlyBalance: TLabel;
    Label_MonthlyBMR: TLabel;
    Label_MonthlyCaloricBalance: TLabel;
    Label_MonthlyMealCalories: TLabel;
    Label_MonthlyMealCarbohydrates: TLabel;
    Label_MonthlyMealFats: TLabel;
    Label_MonthlyMealProteins: TLabel;
    Label_Note: TLabel;
    Label_OrganicAcids: TLabel;
    Label_Phosphorus: TLabel;
    Label_Polyols: TLabel;
    Label_Potassium: TLabel;
    Label_Proteins: TLabel;
    Label_Right: TLabel;
    Label_Salt: TLabel;
    Label_Sodium: TLabel;
    Label_Sugar: TLabel;
    Label_TotalAlcohol: TLabel;
    Label_TotalBetaCarotene: TLabel;
    Label_TotalCalcium: TLabel;
    Label_TotalCarbohydrate: TLabel;
    Label_TotalCholesterol: TLabel;
    Label_TotalDailyExpenses: TLabel;
    Label_TotalDailyWorkoutCalories: TLabel;
    Label_TotalDailyWorkoutTime: TLabel;
    Label_TotalDailyWorkoutTime1: TLabel;
    Label_TotalEnergy: TLabel;
    Label_TotalFat: TLabel;
    Label_TotalFibres: TLabel;
    Label_TotalIron: TLabel;
    Label_TotalMagnesium: TLabel;
    Label_TotalMonthlyExpenses: TLabel;
    Label_TotalMonthlyWorkoutCalories: TLabel;
    Label_TotalMonthlyWorkoutTime: TLabel;
    Label_TotalOrganicAcids: TLabel;
    Label_TotalPhosphorus: TLabel;
    Label_TotalPolyols: TLabel;
    Label_TotalPotassium: TLabel;
    Label_TotalProtein: TLabel;
    Label_TotalSalt: TLabel;
    Label_TotalSodium: TLabel;
    Label_TotalSugar: TLabel;
    Label_TotalVitB1: TLabel;
    Label_TotalVitB12: TLabel;
    Label_TotalVitB2: TLabel;
    Label_TotalVitB3: TLabel;
    Label_TotalVitB5: TLabel;
    Label_TotalVitB6: TLabel;
    Label_TotalVitB9: TLabel;
    Label_TotalVitC: TLabel;
    Label_TotalVitD: TLabel;
    Label_TotalVitE: TLabel;
    Label_TotalVitK1: TLabel;
    Label_TotalVitK2: TLabel;
    Label_TotalWeeklyExpenses: TLabel;
    Label_TotalWeeklyWorkoutCalories: TLabel;
    Label_TotalZinc: TLabel;
    Label_VitB1: TLabel;
    Label_VitB12: TLabel;
    Label_VitB2: TLabel;
    Label_VitB3: TLabel;
    Label_VitB5: TLabel;
    Label_VitB6: TLabel;
    Label_VitB9: TLabel;
    Label_VitC: TLabel;
    Label_VitD: TLabel;
    Label_VitE: TLabel;
    Label_VitK1: TLabel;
    Label_VitK2: TLabel;
    Label_WeeklyBMR: TLabel;
    Label_WeeklyCaloricBalance: TLabel;
    Label_WeeklyNumberWorkouts: TLabel;
    Label_WeightValue: TLabel;
    Label_HeightValue: TLabel;
    Label_GenderValue: TLabel;
    Label_AgeValue: TLabel;
    Label_BirthDate_Value: TLabel;
    Label_LastNameValue: TLabel;
    Label_FirstNameValue: TLabel;
    Label_AccountCreationDateValue: TLabel;
    Label_AccountNameValue: TLabel;
    Label_Age: TLabel;
    Label_BMR: TLabel;
    Label_Weight: TLabel;
    Label_Height: TLabel;
    Label_Gender: TLabel;
    Label_AccountCreationDate: TLabel;
    Label_BirthDate: TLabel;
    Label_LastName: TLabel;
    Label_FirstName: TLabel;
    Label_AccountName: TLabel;
    Label_WorkoutList: TLabel;
    Label_Zinc: TLabel;
    ListChartSource_BodyMeasurementsDateLabels: TListChartSource;
    ListChartSource_GlobalHealthDateLabels: TListChartSource;
    MainMenu: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem_Edit_Exercises: TMenuItem;
    MenuItem_Edit_Pictures: TMenuItem;
    MenuItem_File_Backup: TMenuItem;
    MenuItem_File_OpenRecentFile: TMenuItem;
    MenuItem_Help_Help: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem_File_Import: TMenuItem;
    MenuItem_File_Export: TMenuItem;
    MenuItem_File_Open: TMenuItem;
    MenuItem_File_New: TMenuItem;
    MenuItem_Edit_Foods: TMenuItem;
    MenuItem_Edit_Workouts: TMenuItem;
    MenuItem_File_CloseAccount: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem_Edit: TMenuItem;
    MenuItem_Help_About: TMenuItem;
    MenuItem_Help: TMenuItem;
    MenuItem_File_OpenAccount: TMenuItem;
    MenuItem_File_Quit: TMenuItem;
    MenuItem_File: TMenuItem;
    MRUMenuManager_MostRecentFiles: TMRUMenuManager;
    OpenDialog_AddPicture: TOpenDialog;
    OpenDialog_Import: TOpenDialog;
    OpenDialog_StaySharpFile: TOpenDialog;
    PageControl_DailyActivities: TPageControl;
    PageControl_Statistics: TPageControl;
    Panel_DailyActivities: TPanel;
    Panel_DailyStatistics: TPanel;
    Panel_Day_Summary: TPanel;
    Panel_Menu: TPanel;
    DBConnection: TSQLite3Connection;
    SaveDialog_Export: TSaveDialog;
    SaveDialog_StaySharpFile: TSaveDialog;
    ScrollBox1: TScrollBox;
    ScrollBox2: TScrollBox;
    ScrollBox_DailyStatistics: TScrollBox;
    ScrollBox_MonthlyStatistics: TScrollBox;
    ScrollBox_WeeklyStatistics: TScrollBox;
    Splitter2: TSplitter;
    SQLQuery_MealItemsalcohol: TFloatField;
    SQLQuery_MealItemsbeta_carotene: TFloatField;
    SQLQuery_MealItemscalcium: TFloatField;
    SQLQuery_MealItemscarbohydrate: TFloatField;
    SQLQuery_MealItemscholesterol: TFloatField;
    SQLQuery_MealItemsenergy: TFloatField;
    SQLQuery_MealItemsfat: TFloatField;
    SQLQuery_MealItemsfibres: TFloatField;
    SQLQuery_MealItemsfood_no: TLongintField;
    SQLQuery_MealItemsiron: TFloatField;
    SQLQuery_MealItemsmagnesium: TFloatField;
    SQLQuery_MealItemsmeal_item_no: TAutoIncField;
    SQLQuery_MealItemsmeal_no: TLongintField;
    SQLQuery_MealItemsname: TStringField;
    SQLQuery_MealItemsorganic_acids: TFloatField;
    SQLQuery_MealItemsphosphorus: TFloatField;
    SQLQuery_MealItemspolyols: TFloatField;
    SQLQuery_MealItemspotassium: TFloatField;
    SQLQuery_MealItemsprotein: TFloatField;
    SQLQuery_MealItemsquantity: TFloatField;
    SQLQuery_MealItemssalt: TFloatField;
    SQLQuery_MealItemssodium: TFloatField;
    SQLQuery_MealItemssugar: TFloatField;
    SQLQuery_MealItemsunit_name: TStringField;
    SQLQuery_MealItemsunit_quantity: TFloatField;
    SQLQuery_MealItemsvitamin_b1: TFloatField;
    SQLQuery_MealItemsvitamin_b12: TFloatField;
    SQLQuery_MealItemsvitamin_b2: TFloatField;
    SQLQuery_MealItemsvitamin_b3: TFloatField;
    SQLQuery_MealItemsvitamin_b5: TFloatField;
    SQLQuery_MealItemsvitamin_b6: TFloatField;
    SQLQuery_MealItemsvitamin_b9: TFloatField;
    SQLQuery_MealItemsvitamin_c: TFloatField;
    SQLQuery_MealItemsvitamin_d: TFloatField;
    SQLQuery_MealItemsvitamin_e: TFloatField;
    SQLQuery_MealItemsvitamin_k1: TFloatField;
    SQLQuery_MealItemsvitamin_k2: TFloatField;
    SQLQuery_MealItemswater: TFloatField;
    SQLQuery_MealItemszinc: TFloatField;
    SQLQuery_Mealsaccount_no: TLongintField;
    SQLQuery_Mealscreation_date: TStringField;
    SQLQuery_Mealscreation_time: TStringField;
    SQLQuery_Mealsmeal_name: TStringField;
    SQLQuery_Mealsmeal_no: TAutoIncField;
    SQLQuery_Mealsmeal_type_no: TLongintField;
    SQLQuery_MealStatistics: TSQLQuery;
    SQLQuery_MealItems: TSQLQuery;
    SQLQuery_Meals: TSQLQuery;
    SQLQuery_CurrentWorkout: TSQLQuery;
    SQLQuery_DailyWorkoutStatistics: TSQLQuery;
    SQLQuery_Workout: TSQLQuery;
    SQLQuery_Account: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    StatusBar1: TStatusBar;
    TabSheet_DailyMeals: TTabSheet;
    TabSheet_DailyWorkouts: TTabSheet;
    TabSheet_Measurements: TTabSheet;
    TabSheet_OtherStatistics: TTabSheet;
    TabSheet_DailyStatistics: TTabSheet;
    TabSheet_MonthlyStatistics: TTabSheet;
    TabSheet_Pictures: TTabSheet;
    TabSheet_WeeklyStatistics: TTabSheet;
    ToolBar_DailyActivities: TToolBar;
    ToolBar_DailyStatistics: TToolBar;
    ToolBar_MainToolBar: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton_Exercises: TToolButton;
    ToolButton_Pictures: TToolButton;
    ToolButton_DailyStatisticsExpand: TToolButton;
    ToolButton_Foods: TToolButton;
    ToolButton_StatisticsPanel_Expand: TToolButton;
    ToolButton_DailyActivitiesPanel_Expand: TToolButton;
    ToolButton_Workouts: TToolButton;
    ToolButton_OpenAccount: TToolButton;
    ToolButton_Open: TToolButton;
    ToolButton_New: TToolButton;
    procedure Button_AddMealClick(Sender: TObject);
    procedure Button_AddPicture1Click(Sender: TObject);
    procedure Button_AddPicture2Click(Sender: TObject);
    procedure Button_AddPicture3Click(Sender: TObject);
    procedure Button_AddPicture4Click(Sender: TObject);
    procedure Button_AddWorkoutClick(Sender: TObject);
    procedure Button_DeleteMealClick(Sender: TObject);
    procedure Button_DeletePicture1Click(Sender: TObject);
    procedure Button_DeletePicture2Click(Sender: TObject);
    procedure Button_DeletePicture3Click(Sender: TObject);
    procedure Button_DeletePicture4Click(Sender: TObject);
    procedure Button_DeleteWorkoutClick(Sender: TObject);
    procedure Button_EditMealClick(Sender: TObject);
    procedure Button_EditWorkoutClick(Sender: TObject);
    procedure Button_Measurements_DeleteClick(Sender: TObject);
    procedure Button_Measurements_EditClick(Sender: TObject);
    procedure Button_TodayClick(Sender: TObject);
    procedure Calendar1Change(Sender: TObject);
    procedure Calendar1Click(Sender: TObject);
    procedure ComboBox_BodyMeasurementsCategoryChange(Sender: TObject);
    procedure ComboBox_GlobalHealthCategoryChange(Sender: TObject);
    procedure DateTimePicker_BodyMeasurementsEndDateChange(Sender: TObject);
    procedure DateTimePicker_BodyMeasurementsStartDateChange(Sender: TObject);
    procedure DateTimePicker_GlobalHealthEndDateChange(Sender: TObject);
    procedure DateTimePicker_GlobalHealthStartDateChange(Sender: TObject);
    procedure DBGrid_MealListCellClick(Column: TColumn);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MenuItem_EditClick(Sender: TObject);
    procedure MenuItem_Edit_ExercisesClick(Sender: TObject);
    procedure MenuItem_Edit_PicturesClick(Sender: TObject);
    procedure MenuItem_Edit_WorkoutsClick(Sender: TObject);
    procedure MenuItem_File_BackupClick(Sender: TObject);
    procedure MenuItem_File_CloseAccountClick(Sender: TObject);
    procedure MenuItem_File_ExportClick(Sender: TObject);
    procedure MenuItem_File_ImportClick(Sender: TObject);
    procedure MenuItem_File_OpenAccountClick(Sender: TObject);
    procedure MenuItem_File_QuitClick(Sender: TObject);
    procedure MenuItem_Edit_FoodsClick(Sender: TObject);
    procedure MenuItem_Help_AboutClick(Sender: TObject);
    procedure MenuItem_File_NewClick(Sender: TObject);
    procedure MenuItem_File_OpenClick(Sender: TObject);
    procedure MenuItem_Help_HelpClick(Sender: TObject);
    procedure MRUMenuManager_MostRecentFilesRecentFile(Sender: TObject;
      const AFileName: String);
    procedure Splitter2Moved(Sender: TObject);
    procedure ToolButton_AddWorkoutClick(Sender: TObject);
    procedure DBSelectAccountInfo(Account: TSSAccount; AccountNo: longint);
    procedure ToolButton_ExercisesClick(Sender: TObject);
    procedure ToolButton_FoodsClick(Sender: TObject);
    procedure ToolButton_NewClick(Sender: TObject);
    procedure ToolButton_OpenAccountClick(Sender: TObject);
    procedure ToolButton_OpenClick(Sender: TObject);
    procedure ToolButton_PicturesClick(Sender: TObject);
    procedure ToolButton_StatisticsPanel_ExpandClick(Sender: TObject);
    procedure ToolButton_DailyActivitiesPanel_ExpandClick(Sender: TObject);
    procedure ToolButton_WorkoutsClick(Sender: TObject);
    procedure UpdateAccountInfo();
    procedure UpdateAccountList();
    procedure UpdateDailyWorkouts();
    procedure UpdateMealList();
    procedure UpdateMealItemList();
    procedure UpdateMealStatistics();
    procedure UpdateStatistics();
    procedure UpdateMeasurements();
    procedure UpdatePictures();
    procedure UpdateAllLayouts();
    function ComputeTotalWorkoutCalories(const StartDate: TDate; const EndDate: TDate): integer;
    function ComputeTotalWorkoutTime(const StartDate: TDate; const EndDate: TDate): string;
    function ComputeNumberWorkouts(const StartDate: TDate; const EndDate: TDate): integer;
    procedure DBSelectWorkout(Workout: TSSWorkout; WorkoutNo: longint);
    function ComputeMealNutrients(DataSet: TDataSet): TNutrients;
    function ComputeMealStatistics(const StartDate: TDate; const EndDate: TDate): TNutrients;
  private
    procedure ReadConfigurationFile(const ConfigFile: TSSApplicationSettings);
    procedure SaveConfigurationFile(ConfigFile: TSSApplicationSettings);
    procedure RefreshGlobalHealthPlot();
    procedure GlobalHealthPlot(StartDate: string; EndDate: string);
    procedure RefreshBodyMeasurementsPlot();
    procedure BodyMeasurementsPlot(StartDate: string; EndDate: string);
    procedure AddPicture(Image: TImage; LayoutId: Integer);
    procedure UpdatePicture(Image: TImage; LayoutId: integer);
    procedure DeletePicture(Image: TImage; LayoutId: Integer);
    procedure BusinessObjectsCleanUp();

  public
    ApplicationSettings: TSSApplicationSettings;
    CurrentAccount: TSSAccount;
    CurrentWorkout: TSSWorkout;
    CurrentMeal: TSSMeal;
    CurrentMeasurement: TSSMeasurement;
    CurrentPictures: array [1..4] of TSSPicture;
  end;

var
  Form_Main: TForm_Main;
  StaySharpVersion: string;
  StaySharpSchemaVersion: string; // Supported database schema
  StaySharpSchemaVersionCreationDate: string;
  AppDataFolder: string;
  AppDataPath: array[0..MaxPathLen] of char; // Allocate memory
  LanguageLongCode: string;
  LanguageShortCode: string;
  MaxPictureSize: integer;

implementation

{$R *.lfm}

uses workout, about, account_list, workout_list, food_list, meal, measurements,
  backup, picture_list, exercise_list;

procedure TForm_Main.BusinessObjectsCleanUp();
var
  i: integer;
begin
  FreeAndNil(CurrentWorkout);
  FreeAndNil(CurrentMeal);
  FreeAndNil(CurrentMeasurement);
  for i := Low(CurrentPictures) to High(CurrentPictures) do
  begin
    FreeAndNil(CurrentPictures);
  end;
  FreeAndNil(CurrentAccount);
end;

procedure TForm_Main.UpdateAllLayouts();
begin
  UpdateAccountInfo();
  UpdateDailyWorkouts();
  UpdateStatistics();
  UpdateMealList();
  UpdateMealItemList();
  UpdateMeasurements();
  RefreshGlobalHealthPlot();
  RefreshBodyMeasurementsPlot();
  UpdatePictures();
end;

procedure TForm_Main.DeletePicture(Image: TImage; LayoutId: Integer);
begin
  { Ask the user if he wants to delete the picture }
  case QuestionDlg(SDlgDeletePictureTitle, SDlgDeletePictureMsg, mtCustom, [mrNo, SDlgBtnNo,
      'IsDefault', mrYes, SDlgBtnYes], '') of
    mrYes:
    begin
      { Delete the picture from TImage component }
      Image.Picture.Clear;

      { Delete the picture from database }
      CurrentPictures[LayoutId].DBDelete(DBConnection, SQLTransaction1);
    end;
    mrNo:
    begin
      Exit;
    end;
    mrCancel:
    begin
    end;
  end;
end;

procedure TForm_Main.AddPicture(Image: TImage; LayoutId: Integer);
var
  FileExt: string;
begin
  { Open a picture }
  if OpenDialog_AddPicture.Execute then
  begin
    { Check for supported images }
    FileExt := LowerCase(ExtractFileExt(OpenDialog_AddPicture.FileName));

    if (FileExt <> '.jpeg') and (FileExt <> '.jpg') and (FileExt <> '.png') then
    begin
      ShowMessage(SDlgImageTypeError);
      Exit;
    end;

    { Check max. file size }
    if(FileSize(OpenDialog_AddPicture.FileName) > MaxPictureSize) then
    begin
      ShowMessage(SDlgImageSizeError1 + (MaxPictureSize / 1024).ToString() + SDlgImageSizeError2);
      Exit;
    end;

    { Load picture into TImage component }
    Image.Picture.LoadFromFile(OpenDialog_AddPicture.FileName);

    { Save picture to database }
    CurrentPictures[LayoutId].AccountNo := CurrentAccount.FAccountNo;
    CurrentPictures[LayoutId].CreationDate := FormatDateTime('yyyy-mm-dd', Calendar1.DateTime);
    CurrentPictures[LayoutId].LayoutId := LayoutId;
    Image.Picture.SaveToStream(CurrentPictures[LayoutId].DataBLOB);
    CurrentPictures[LayoutId].DBInsertReplace(DBConnection, SQLTransaction1);
  end;
end;

procedure TForm_Main.UpdatePicture(Image: TImage; LayoutId: integer);
begin
  if CurrentPictures[LayoutId].PictureNo <> -1 then
  begin
    Image.Picture.LoadFromStream(CurrentPictures[LayoutId].DataBLOB);
    case LayoutId of
      1: Button_DeletePicture1.Enabled := True;
      2: Button_DeletePicture2.Enabled := True;
      3: Button_DeletePicture3.Enabled := True;
      4: Button_DeletePicture4.Enabled := True;
    end;
  end
  else
  begin
    Image.Picture.Clear;
    case LayoutId of
      1: Button_DeletePicture1.Enabled := False;
      2: Button_DeletePicture2.Enabled := False;
      3: Button_DeletePicture3.Enabled := False;
      4: Button_DeletePicture4.Enabled := False;
    end;
  end;
end;

procedure TForm_Main.UpdatePictures();
var
  i: integer;
begin
  if CurrentAccount <> nil then
  begin
    for i := Low(CurrentPictures) to High(CurrentPictures) do
    begin
      CurrentPictures[i].Clear();
      CurrentPictures[i].DBFind(DBConnection, CurrentAccount.FAccountNo, FormatDateTime('yyyy-mm-dd', Form_Main.Calendar1.DateTime), i);
    end;
    UpdatePicture(Image1, 1);
    UpdatePicture(Image2, 2);
    UpdatePicture(Image3, 3);
    UpdatePicture(Image4, 4);
  end
  else
  begin
    Image1.Picture.Clear;
    Image2.Picture.Clear;
    Image3.Picture.Clear;
    Image4.Picture.Clear;
  end;
end;

procedure TForm_Main.RefreshBodyMeasurementsPlot();
var
  StartDate: string;
  EndDate: string;
begin
  StartDate := FormatDateTime('yyyy-mm-dd', DateTimePicker_BodyMeasurementsStartDate.DateTime);
  EndDate := FormatDateTime('yyyy-mm-dd', DateTimePicker_BodyMeasurementsEndDate.DateTime);

  { Select which plot to display }
  Chart_BodyMeasurementsLineSeries_Neck.Active := False;
  Chart_BodyMeasurementsLineSeries_Shoulders.Active := False;
  Chart_BodyMeasurementsLineSeries_Chest.Active := False;
  Chart_BodyMeasurementsLineSeries_ArmLeft.Active := False;
  Chart_BodyMeasurementsLineSeries_ArmRight.Active := False;
  Chart_BodyMeasurementsLineSeries_ForearmLeft.Active := False;
  Chart_BodyMeasurementsLineSeries_ForearmRight.Active := False;
  Chart_BodyMeasurementsLineSeries_MidSection.Active := False;
  Chart_BodyMeasurementsLineSeries_Hips.Active := False;
  Chart_BodyMeasurementsLineSeries_ThighLeft.Active := False;
  Chart_BodyMeasurementsLineSeries_ThighRight.Active := False;
  Chart_BodyMeasurementsLineSeries_CalfLeft.Active := False;
  Chart_BodyMeasurementsLineSeries_CalfRight.Active := False;

  // Neck
  if ComboBox_BodyMeasurementsCategory.ItemIndex = 0 then
    Chart_BodyMeasurementsLineSeries_Neck.Active := True;
  // Shoulders
  if ComboBox_BodyMeasurementsCategory.ItemIndex = 1 then
    Chart_BodyMeasurementsLineSeries_Shoulders.Active := True;
  // Chest
  if ComboBox_BodyMeasurementsCategory.ItemIndex = 2 then
    Chart_BodyMeasurementsLineSeries_Chest.Active := True;
  // Arms
  if ComboBox_BodyMeasurementsCategory.ItemIndex = 3 then
    begin
      Chart_BodyMeasurementsLineSeries_ArmLeft.Active := True;
      Chart_BodyMeasurementsLineSeries_ArmRight.Active := True;
    end;
  // Forearms
  if ComboBox_BodyMeasurementsCategory.ItemIndex = 4 then
    begin
      Chart_BodyMeasurementsLineSeries_ForearmLeft.Active := True;
      Chart_BodyMeasurementsLineSeries_ForearmRight.Active := True;
    end;
  // Mid section
  if ComboBox_BodyMeasurementsCategory.ItemIndex = 5 then
    Chart_BodyMeasurementsLineSeries_MidSection.Active := True;
  // Hips
  if ComboBox_BodyMeasurementsCategory.ItemIndex = 6 then
    Chart_BodyMeasurementsLineSeries_Hips.Active := True;
  // Thighs
  if ComboBox_BodyMeasurementsCategory.ItemIndex = 7 then
    begin
      Chart_BodyMeasurementsLineSeries_ThighLeft.Active := True;
      Chart_BodyMeasurementsLineSeries_ThighRight.Active := True;
    end;
  // Calves
  if ComboBox_BodyMeasurementsCategory.ItemIndex = 8 then
    begin
      Chart_BodyMeasurementsLineSeries_CalfLeft.Active := True;
      Chart_BodyMeasurementsLineSeries_CalfRight.Active := True;
    end;

  BodyMeasurementsPlot(StartDate, EndDate);
end;

procedure TForm_Main.BodyMeasurementsPlot(StartDate: string; EndDate: string);
var
  Query: TSQLQuery;
  SQL: string;
  Date: TDateTime;
  DateTimeSettings: TFormatSettings;
  Index: Integer;
begin
  if CurrentAccount <> nil then
  begin
    SQL := 'SELECT * FROM measurement WHERE ';
    SQL := SQL + 'account_no = ' + IntToStr(CurrentAccount.FAccountNo) + ' AND ';
    SQL := SQL + 'creation_date >= ''' + StartDate + ''' AND ';
    SQL := SQL + 'creation_date <= ''' + EndDate + ''' ';
    SQL := SQL + ' ORDER BY creation_date';
    Query := TSQLQuery.Create(nil);
    try
      Query.DataBase := DBConnection;
      Query.Clear;
      Query.SQL.Text := SQL;
      Query.Open;

      { Clear plots }
      ListChartSource_BodyMeasurementsDateLabels.Clear;
      Chart_BodyMeasurementsLineSeries_Neck.Clear;
      Chart_BodyMeasurementsLineSeries_Shoulders.Clear;
      Chart_BodyMeasurementsLineSeries_Chest.Clear;
      Chart_BodyMeasurementsLineSeries_ArmLeft.Clear;
      Chart_BodyMeasurementsLineSeries_ArmRight.Clear;
      Chart_BodyMeasurementsLineSeries_ForearmLeft.Clear;
      Chart_BodyMeasurementsLineSeries_ForearmRight.Clear;
      Chart_BodyMeasurementsLineSeries_MidSection.Clear;
      Chart_BodyMeasurementsLineSeries_Hips.Clear;
      Chart_BodyMeasurementsLineSeries_ThighLeft.Clear;
      Chart_BodyMeasurementsLineSeries_ThighRight.Clear;
      Chart_BodyMeasurementsLineSeries_CalfLeft.Clear;
      Chart_BodyMeasurementsLineSeries_CalfRight.Clear;

      if Query.RecordCount > 0 then
      begin
        { Record found }
        DateTimeSettings.ShortDateFormat := 'yyyy-mm-dd';
        DateTimeSettings.DateSeparator := '-';

        Index := 0;
        Query.First;
        while not Query.EOF do
        begin
          Date := StrToDate(Query.FieldByName('creation_date').AsString, DateTimeSettings);

          {if Index = 0 then
            ListChartSource_BodyMeasurementsDateLabels.Add(Index, 0, FormatDateTime('dd/mm', Date));}

          { Neck }
          if Query.FieldByName('neck').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_Neck.AddXY(Index, Query.FieldByName('neck').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Shoulders }
          if Query.FieldByName('shoulders').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_Shoulders.AddXY(Index, Query.FieldByName('shoulders').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Chest }
          if Query.FieldByName('chest').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_Chest.AddXY(Index, Query.FieldByName('chest').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Arms }
          if Query.FieldByName('arm_left').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_ArmLeft.AddXY(Index, Query.FieldByName('arm_left').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          if Query.FieldByName('arm_right').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_ArmRight.AddXY(Index, Query.FieldByName('arm_right').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Forearms }
          if Query.FieldByName('forearm_left').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_ForearmLeft.AddXY(Index, Query.FieldByName('forearm_left').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          if Query.FieldByName('forearm_right').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_ForearmRight.AddXY(Index, Query.FieldByName('forearm_right').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Mid section }
          if Query.FieldByName('mid_section').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_MidSection.AddXY(Index, Query.FieldByName('mid_section').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Hips }
          if Query.FieldByName('hips').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_Hips.AddXY(Index, Query.FieldByName('hips').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Thighs }
          if Query.FieldByName('thigh_left').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_ThighLeft.AddXY(Index, Query.FieldByName('thigh_left').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          if Query.FieldByName('thigh_right').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_ThighRight.AddXY(Index, Query.FieldByName('thigh_right').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Calves }
          if Query.FieldByName('calf_left').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_CalfLeft.AddXY(Index, Query.FieldByName('calf_left').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          if Query.FieldByName('calf_right').AsFloat > 0 then
          begin
            Chart_BodyMeasurementsLineSeries_CalfRight.AddXY(Index, Query.FieldByName('calf_right').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          Index := Index + 1;
          Query.Next;
        end;
      end
      else
      begin
      end;
    finally
      FreeAndNil(Query);
    end;
  end;

end;

procedure TForm_Main.RefreshGlobalHealthPlot();
var
  StartDate: string;
  EndDate: string;
begin
  StartDate := FormatDateTime('yyyy-mm-dd', DateTimePicker_GlobalHealthStartDate.DateTime);
  EndDate := FormatDateTime('yyyy-mm-dd', DateTimePicker_GlobalHealthEndDate.DateTime);

  { Select which plot to display }
  Chart_GlobalHealthLineSeries_Weight.Active := False;
  Chart_GlobalHealthLineSeries_BMILimit1.Active := False;
  Chart_GlobalHealthLineSeries_BMILimit2.Active := False;
  Chart_GlobalHealthLineSeries_BMILimit3.Active := False;
  Chart_GlobalHealthLineSeries_BMILimit4.Active := False;
  Chart_GlobalHealthLineSeries_BodyMassIndex.Active := False;
  Chart_GlobalHealthLineSeries_Sleep.Active := False;
  Chart_GlobalHealthLineSeries_Systolic.Active := False;
  Chart_GlobalHealthLineSeries_Diastolic.Active := False;

  // Weight
  if ComboBox_GlobalHealthCategory.ItemIndex = 0 then
    Chart_GlobalHealthLineSeries_Weight.Active := True;
  // Body Mass Index
  if ComboBox_GlobalHealthCategory.ItemIndex = 1 then
  begin
    Chart_GlobalHealthLineSeries_BodyMassIndex.Active := True;
    Chart_GlobalHealthLineSeries_BMILimit1.Active := True;
    Chart_GlobalHealthLineSeries_BMILimit2.Active := True;
    Chart_GlobalHealthLineSeries_BMILimit3.Active := True;
    Chart_GlobalHealthLineSeries_BMILimit4.Active := True;
  end;
  // Sleep
  if ComboBox_GlobalHealthCategory.ItemIndex = 2 then
    Chart_GlobalHealthLineSeries_Sleep.Active := True;
  // Blood pressue
  if ComboBox_GlobalHealthCategory.ItemIndex = 3 then
    begin
      Chart_GlobalHealthLineSeries_Systolic.Active := True;
      Chart_GlobalHealthLineSeries_Diastolic.Active := True;
    end;

  GlobalHealthPlot(StartDate, EndDate);
end;

procedure TForm_Main.GlobalHealthPlot(StartDate: string; EndDate: string);
var
  Query: TSQLQuery;
  SQL: string;
  Date: TDate;
  DateTimeSettings: TFormatSettings;
  Index: Integer;
begin
  if CurrentAccount <> nil then
  begin
    SQL := 'SELECT * FROM measurement WHERE ';
    SQL := SQL + 'account_no = ' + IntToStr(CurrentAccount.FAccountNo) + ' AND ';
    SQL := SQL + 'creation_date >= ''' + StartDate + ''' AND ';
    SQL := SQL + 'creation_date <= ''' + EndDate + ''' ';
    SQL := SQL + ' ORDER BY creation_date';
    Query := TSQLQuery.Create(nil);
    try
      Query.DataBase := DBConnection;
      Query.Clear;
      Query.SQL.Text := SQL;
      Query.Open;

      { Clear plots }
      ListChartSource_GlobalHealthDateLabels.Clear;
      Chart_GlobalHealthLineSeries_Weight.Clear;
      Chart_GlobalHealthLineSeries_BodyMassIndex.Clear;
      Chart_GlobalHealthLineSeries_BMILimit1.Clear;
      Chart_GlobalHealthLineSeries_BMILimit2.Clear;
      Chart_GlobalHealthLineSeries_BMILimit3.Clear;
      Chart_GlobalHealthLineSeries_BMILimit4.Clear;
      Chart_GlobalHealthLineSeries_Sleep.Clear;
      Chart_GlobalHealthLineSeries_Systolic.Clear;
      Chart_GlobalHealthLineSeries_Diastolic.Clear;

      if Query.RecordCount > 0 then
      begin
        { Record found }
        DateTimeSettings.ShortDateFormat := 'yyyy-mm-dd';
        DateTimeSettings.DateSeparator := '-';

        Index := 0;
        Query.First;
        while not Query.EOF do
        begin
          Date := StrToDate(Query.FieldByName('creation_date').AsString, DateTimeSettings);

          {if Index = 0 then
            ListChartSource_GlobalHealthDateLabels.Add(Index, 0, FormatDateTime('dd/mm', Date));}

          { Weight }
          if Query.FieldByName('weight').AsFloat > 0 then
          begin
            Chart_GlobalHealthLineSeries_Weight.AddXY(Index, Query.FieldByName('weight').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Body Mass Index }
          if Query.FieldByName('body_mass_index').AsFloat > 0 then
          begin
            Chart_GlobalHealthLineSeries_BodyMassIndex.AddXY(Index, Query.FieldByName('body_mass_index').AsFloat, FormatDateTime('dd/mm', Date));
            Chart_GlobalHealthLineSeries_BMILimit1.AddXY(Index, 18.5, FormatDateTime('dd/mm', Date));
            Chart_GlobalHealthLineSeries_BMILimit2.AddXY(Index, 25, FormatDateTime('dd/mm', Date));
            Chart_GlobalHealthLineSeries_BMILimit3.AddXY(Index, 30, FormatDateTime('dd/mm', Date));
            Chart_GlobalHealthLineSeries_BMILimit4.AddXY(Index, 35, FormatDateTime('dd/mm', Date));
          end;
          { Sleep }
          if Query.FieldByName('sleep').AsFloat > 0 then
          begin
            Chart_GlobalHealthLineSeries_Sleep.AddXY(Index, Query.FieldByName('sleep').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          { Blood pressure }
          if Query.FieldByName('blood_pressure_systolic').AsFloat > 0 then
          begin
            Chart_GlobalHealthLineSeries_Systolic.AddXY(Index, Query.FieldByName('blood_pressure_systolic').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          if Query.FieldByName('blood_pressure_diastolic').AsFloat > 0 then
          begin
            Chart_GlobalHealthLineSeries_Diastolic.AddXY(Index, Query.FieldByName('blood_pressure_diastolic').AsFloat, FormatDateTime('dd/mm', Date));
          end;
          Index := Index + 1;
          Query.Next;
        end;
      end
      else
      begin
      end;
    finally
      FreeAndNil(Query);
    end;
  end;

end;

procedure TForm_Main.UpdateMeasurements();
begin
  if CurrentAccount <> nil then
  begin
    CurrentMeasurement.DBFind(DBConnection, CurrentAccount.FAccountNo, FormatDateTime('yyyy-mm-dd', Form_Main.Calendar1.DateTime));
    if CurrentMeasurement.MeasurementNo = -1 then
    begin
      { No record found }
      Button_Measurements_Delete.Enabled := False;

      Label_Measure_WeightValue.Caption := '?';
      Label_Measure_BMIValue.Caption := '?';
      Label_BMIInterpretation.Caption := '--';
      Label_Measure_SleepValue.Caption := '?';
      Label_Measure_RestHeartRateValue.Caption := '?';
      Label_Measure_SystolicValue.Caption := '?';
      Label_Measure_DiastolicValue.Caption := '?';
      Label_Measure_Neck.Caption := '?';
      Label_Measure_Shoulders.Caption := '?';
      Label_Measure_Chest.Caption := '?';
      Label_Measure_ArmLeft.Caption := '?';
      Label_Measure_ArmRight.Caption := '?';
      Label_Measure_ForearmLeft.Caption := '?';
      Label_Measure_ForearmRight.Caption := '?';
      Label_Measure_MidSection.Caption := '?';
      Label_Measure_Hips.Caption := '?';
      Label_Measure_ThighLeft.Caption := '?';
      Label_Measure_ThighRight.Caption := '?';
      Label_Measure_CalfLeft.Caption := '?';
      Label_Measure_CalfRight.Caption := '?';
    end
    else
    begin
      { Record found }
      Button_Measurements_Delete.Enabled := True;

      Label_Measure_WeightValue.Caption := FormatFloat('0.00', CurrentMeasurement.Weight);
      Label_Measure_BMIValue.Caption := FormatFloat('0.00', CurrentMeasurement.BodyMassIndex);
      Label_BMIInterpretation.Caption := Form_Measurements.BodyMassIndexInterpretation(CurrentMeasurement.BodyMassIndex);
      Label_Measure_SleepValue.Caption := FormatFloat('0.00', CurrentMeasurement.Sleep);
      Label_Measure_RestHeartRateValue.Caption := IntToStr(CurrentMeasurement.RestHeartRate);
      Label_Measure_SystolicValue.Caption := IntToStr(CurrentMeasurement.BloodPressureSystolic);
      Label_Measure_DiastolicValue.Caption := IntToStr(CurrentMeasurement.BloodPressureDiastolic);
      Label_Measure_Neck.Caption := FormatFloat('0.00', CurrentMeasurement.Neck);
      Label_Measure_Shoulders.Caption := FormatFloat('0.00', CurrentMeasurement.Shoulders);
      Label_Measure_Chest.Caption := FormatFloat('0.00', CurrentMeasurement.Chest);
      Label_Measure_ArmLeft.Caption := FormatFloat('0.00', CurrentMeasurement.ArmLeft);
      Label_Measure_ArmRight.Caption := FormatFloat('0.00', CurrentMeasurement.ArmRignt);
      Label_Measure_ForearmLeft.Caption := FormatFloat('0.00', CurrentMeasurement.ForearmLeft);
      Label_Measure_ForearmRight.Caption := FormatFloat('0.00', CurrentMeasurement.ForearmRight);
      Label_Measure_MidSection.Caption := FormatFloat('0.00', CurrentMeasurement.MidSection);
      Label_Measure_Hips.Caption := FormatFloat('0.00', CurrentMeasurement.Hips);
      Label_Measure_ThighLeft.Caption := FormatFloat('0.00', CurrentMeasurement.ThighLeft);
      Label_Measure_ThighRight.Caption := FormatFloat('0.00', CurrentMeasurement.ThighRight);
      Label_Measure_CalfLeft.Caption := FormatFloat('0.00', CurrentMeasurement.CalfLeft);
      Label_Measure_CalfRight.Caption := FormatFloat('0.00', CurrentMeasurement.CalfRight);
    end;
    RefreshGlobalHealthPlot();
    RefreshBodyMeasurementsPlot();
  end;
end;

procedure TForm_Main.ReadConfigurationFile(const ConfigFile: TSSApplicationSettings);
begin
  if ConfigFile.IsMaximized = True then
    Form_Main.WindowState := wsMaximized
  else
    Form_Main.WindowState := wsNormal;
  Form_Main.Top := ConfigFile.PositionTop;
  Form_Main.Left := ConfigFile.PositionLeft;
  Form_Main.Height := ConfigFile.PositionHeight;
  Form_Main.Width := ConfigFile.PositionWidth;
  Form_Main.Panel_DailyActivities.Height := ConfigFile.ActivitiesPanelHeight;
end;

procedure TForm_Main.SaveConfigurationFile(ConfigFile: TSSApplicationSettings);
begin
  if Form_Main.WindowState = wsMaximized then
    ConfigFile.IsMaximized := True
  else
    ConfigFile.IsMaximized := False;
  ConfigFile.PositionTop := Form_Main.Top;
  ConfigFile.PositionLeft := Form_Main.Left;
  ConfigFile.PositionHeight := Form_Main.Height;
  ConfigFile.PositionWidth := Form_Main.Width;
  ConfigFile.ActivitiesPanelHeight := Form_Main.Panel_DailyActivities.Height;
  ConfigFile.SaveConfigurationFile();
end;

procedure TForm_Main.DBSelectAccountInfo(Account: TSSAccount; AccountNo: longint);
var
  SQL: string;
begin
  SQL := 'select * from account where account_no = ' + IntToStr(AccountNo);
  SQLQuery_Account.Close;
  SQLQuery_Account.Clear;
  SQLQuery_Account.SQL.Text := SQL;
  SQLQuery_Account.Open;

  Account.FAccountNo := SQLQuery_Account.FieldByName('account_no').AsInteger;
  Account.FName := SQLQuery_Account.FieldByName('name').AsString;
  Account.FCreationDate := SQLQuery_Account.FieldByName('creation_date').AsString;
  Account.FFirstName := SQLQuery_Account.FieldByName('first_name').AsString;
  Account.FLastName := SQLQuery_Account.FieldByName('last_name').AsString;
  Account.FBirthDate := SQLQuery_Account.FieldByName('birth_date').AsString;
  Account.FGender := SQLQuery_Account.FieldByName('gender').AsInteger;
  Account.FHeight := SQLQuery_Account.FieldByName('height').AsInteger;
  Account.FWeight := SQLQuery_Account.FieldByName('weight').AsFloat;
end;

procedure TForm_Main.ToolButton_ExercisesClick(Sender: TObject);
begin
  MenuItem_Edit_ExercisesClick(Sender);
end;

procedure TForm_Main.ToolButton_FoodsClick(Sender: TObject);
begin
  MenuItem_Edit_FoodsClick(Sender);
end;

procedure TForm_Main.ToolButton_NewClick(Sender: TObject);
begin
  MenuItem_File_NewClick(Sender);
end;

procedure TForm_Main.ToolButton_OpenAccountClick(Sender: TObject);
begin
  MenuItem_File_OpenAccountClick(Sender);
end;

procedure TForm_Main.ToolButton_OpenClick(Sender: TObject);
begin
  MenuItem_File_OpenClick(Sender);
end;

procedure TForm_Main.ToolButton_PicturesClick(Sender: TObject);
begin
  MenuItem_Edit_PicturesClick(Sender);
end;

procedure TForm_Main.ToolButton_StatisticsPanel_ExpandClick(Sender: TObject);
begin
  if Panel_DailyActivities.Height = 1 then
  begin
    { Set to default size }
    ToolButton_StatisticsPanel_Expand.ImageIndex := 15;
    ToolButton_StatisticsPanel_Expand.Hint := SHintExpand;
    Panel_DailyActivities.Height := ApplicationSettings.ActivitiesPanelHeight;
  end
  else
  begin
    { Expands statistics panel }
    ToolButton_StatisticsPanel_Expand.ImageIndex := 14;
    ToolButton_StatisticsPanel_Expand.Hint := SHintRestore;
    Panel_DailyActivities.Height := 1;
  end;
end;

procedure TForm_Main.ToolButton_DailyActivitiesPanel_ExpandClick(Sender: TObject);
var
  PanelHeight: integer;
begin
  PanelHeight := Panel_Day_Summary.Height;
  if Panel_DailyActivities.Height = PanelHeight then
  begin
    { Set to default size }
    ToolButton_DailyActivitiesPanel_Expand.ImageIndex := 15;
    ToolButton_DailyActivitiesPanel_Expand.Hint := SHintExpand;
    Panel_DailyActivities.Height := ApplicationSettings.ActivitiesPanelHeight;
  end
  else
  begin
    { Expand daily activities panel }
    ToolButton_DailyActivitiesPanel_Expand.ImageIndex := 14;
    ToolButton_DailyActivitiesPanel_Expand.Hint := SHintRestore;
    Panel_DailyActivities.Height := PanelHeight;
  end;
end;

procedure TForm_Main.ToolButton_WorkoutsClick(Sender: TObject);
begin
  MenuItem_Edit_WorkoutsClick(Sender);
end;

procedure TForm_Main.DBSelectWorkout(Workout: TSSWorkout; WorkoutNo: longint);
var
  SQL: string;
begin
  SQL := 'select * from workout where workout_no = ' + IntToStr(WorkoutNo);
  SQLQuery_CurrentWorkout.Close;
  SQLQuery_CurrentWorkout.Clear;
  SQLQuery_CurrentWorkout.SQL.Text := SQL;
  SQLQuery_CurrentWorkout.Open;

  Workout.FWorkoutNo := SQLQuery_CurrentWorkout.FieldByName('workout_no').AsInteger;
  Workout.FAccountNo := SQLQuery_CurrentWorkout.FieldByName('account_no').AsInteger;
  Workout.FWorkoutType := SQLQuery_CurrentWorkout.FieldByName('workout_type_no').AsInteger;
  Workout.FCreationDate := SQLQuery_CurrentWorkout.FieldByName('creation_date').AsString;
  Workout.FStartTime := SQLQuery_CurrentWorkout.FieldByName('start_time').AsString;
  Workout.FDuration := SQLQuery_CurrentWorkout.FieldByName('duration').AsString;
  Workout.FMinCF := SQLQuery_CurrentWorkout.FieldByName('min_cf').AsInteger;
  Workout.FMeanCF := SQLQuery_CurrentWorkout.FieldByName('mean_cf').AsInteger;
  Workout.FMaxCF := SQLQuery_CurrentWorkout.FieldByName('max_cf').AsInteger;
  Workout.FCalories := SQLQuery_CurrentWorkout.FieldByName('calories').AsInteger;
  Workout.FFeeleingType := SQLQuery_CurrentWorkout.FieldByName('feeling_type_no').AsInteger;
  Workout.FNote := SQLQuery_CurrentWorkout.FieldByName('note').AsString;
end;

procedure TForm_Main.UpdateAccountInfo();
begin
  if CurrentAccount <> nil then
  begin
    Label_AccountNameValue.Caption := CurrentAccount.FName;
    Label_AccountCreationDateValue.Caption := CurrentAccount.FCreationDate;
    Label_FirstNameValue.Caption := CurrentAccount.FFirstName;
    Label_LastNameValue.Caption := CurrentAccount.FLastName;
    Label_BirthDate_Value.Caption := CurrentAccount.FBirthDate;
    Label_AgeValue.Caption := IntToStr(CurrentAccount.ComputeAge());
    if CurrentAccount.FGender = 1 then
    begin
       Label_GenderValue.Caption := SDBGender_Female;
    end
    else
    begin
      if CurrentAccount.FGender = 2 then
           Label_GenderValue.Caption := SDBGender_Male
      else
          Label_GenderValue.Caption := SDBGender_None;
    end;

    Label_HeightValue.Caption := IntToStr(CurrentAccount.FHeight);
    Label_WeightValue.Caption := FormatFloat('0.00', CurrentAccount.FWeight);
    Label_BMRValue.Caption := FloatToStr(CurrentAccount.ComputeBMR());
  end
  else
  begin
    Label_AccountNameValue.Caption := '';
    Label_AccountCreationDateValue.Caption := '';
    Label_FirstNameValue.Caption := '';
    Label_LastNameValue.Caption := '';
    Label_BirthDate_Value.Caption := '';
    Label_AgeValue.Caption := '';
    Label_GenderValue.Caption := '';
    Label_HeightValue.Caption := '';
    Label_WeightValue.Caption := '';
    Label_BMRValue.Caption := '';
  end;
end;

procedure TForm_Main.UpdateAccountList();
var
  SQL: string;
begin
  SQL := 'SELECT * FROM account';

  SQLQuery_Account.Close;
  SQLQuery_Account.SQL.Text := SQL;
  SQLQuery_Account.Open;
end;

procedure TForm_Main.UpdateDailyWorkouts();
var
  SQL: string;
begin
  if CurrentAccount <> nil then
  begin
    SQL := 'SELECT ';
    SQL := SQL + 'w.workout_no as workout_no,';
    SQL := SQL + 'wt.name as workout_type_name,';
    SQL := SQL + 'w.creation_date as creation_date,';
    SQL := SQL + 'w.start_time as start_time,';
    SQL := SQL + 'w.duration as duration,';
    SQL := SQL + 'w.calories as calories,';
    SQL := SQL + 'w.min_cf as min_cf,';
    SQL := SQL + 'w.mean_cf as mean_cf,';
    SQL := SQL + 'w.max_cf as max_cf,';
    SQL := SQL + 'ft.name as feeling_type_name,';
    SQL := SQL + 'w.note as note ';
    SQL := SQL + 'FROM workout_type as wt, workout as w, feeling_type as ft ';
    SQL := SQL  + 'WHERE w.account_no = ' + IntToStr(CurrentAccount.FAccountNo) + ' ';
    SQL := SQL + 'AND w.workout_type_no = wt.workout_type_no ';
    SQL := SQL + 'AND w.feeling_type_no = ft.feeling_type_no ' ;
    SQL := SQL + 'AND w.creation_date = ''' + FormatDateTime('yyyy-mm-dd', Form_Main.Calendar1.DateTime) + ''' ' ;
    SQL := SQL + 'ORDER BY w.start_time';

    SQLQuery_Workout.Close;
    SQLQuery_Workout.SQL.Text := SQL;
    SQLQuery_Workout.Open;

    // Updade Note field
    DBMemo_Note.DataField := 'note';

    // Manage edit and delete buttons
    if SQLQuery_Workout.RecordCount = 0 then
    begin
      Button_EditWorkout.Enabled := False;
      Button_DeleteWorkout.Enabled := False;
    end
    else
    begin
      Button_EditWorkout.Enabled := True;
      Button_DeleteWorkout.Enabled := True;
    end;
  end
  else
  begin
    DBGrid_Workouts.DataSource.DataSet.Active := False; // Clear the DBGrid
  end;
end;

procedure TForm_Main.UpdateMealList();
var
  SQL: string;
begin
  if CurrentAccount <> nil then
  begin
    SQL := 'SELECT meal.*, meal_type.name as meal_name FROM meal, meal_type WHERE ';
    SQL := SQL + 'account_no = '  + IntToStr(CurrentAccount.FAccountNo) + ' ';
    SQL := SQL + 'AND creation_date = ''' + FormatDateTime('yyyy-mm-dd', Form_Main.Calendar1.DateTime) + ''' ';
    SQL := SQL + 'AND meal.meal_type_no = meal_type.meal_type_no ';
    SQL := SQL + 'ORDER BY creation_time';

    SQLQuery_Meals.Close;
    SQLQuery_Meals.SQL.Text := SQL;
    SQLQuery_Meals.Open;

    // Manage edit and delete buttons
    if SQLQuery_Meals.RecordCount = 0 then
    begin
      Button_EditMeal.Enabled := False;
      Button_DeleteMeal.Enabled := False;
    end
    else
    begin
      Button_EditMeal.Enabled := True;
      Button_DeleteMeal.Enabled := True;
    end;
  end
  else
  begin
    DBGrid_MealList.DataSource.DataSet.Active:=False;
  end;
end;

procedure TForm_Main.UpdateMealItemList();
var SQL: string;
  MealNo: String;
begin
  if (CurrentAccount <> nil) and (SQLQuery_Meals.RecordCount > 0) then
  begin
    MealNo := DBGrid_MealList.DataSource.DataSet.FieldByName('meal_no').AsString;
    SQL := 'SELECT * FROM meal_item WHERE ';
    SQL := SQL + 'meal_no = '  + MealNo;

    SQLQuery_MealItems.Close;
    SQLQuery_MealItems.SQL.Text := SQL;
    SQLQuery_MealItems.Open;
  end
  else
  begin
    DBGrid_MealItemList.DataSource.DataSet.Active:=False;
  end;
  UpdateMealStatistics();
end;

function TForm_Main.ComputeMealNutrients(DataSet: TDataSet): TNutrients;
begin
  Result.Quantity := 0;
  Result.Protein := 0; Result.Carbohydrate := 0; Result.Sugar := 0;
  Result.Alcohol := 0; Result.Fibres := 0; Result.Polyols := 0;
  Result.OrganicAcids := 0; Result.Fat := 0; Result.Cholesterol := 0;
  Result.Salt := 0; Result.Energy := 0;

  Result.Calcium := 0; Result.Iron := 0; Result.Magnesium := 0;
  Result.Phosphorus := 0; Result.Potassium := 0; Result.Sodium := 0;
  Result.Zinc := 0;

  Result.BetaCarotene := 0; Result.VitD := 0; Result.VitE := 0;
  Result.VitK1 := 0; Result.VitK2 := 0; Result.VitC := 0; Result.VitB1 := 0;
  Result.VitB2 := 0; Result.VitB3 := 0; Result.VitB5 := 0; Result.VitB6 := 0;
  Result.VitB9 := 0; Result.VitB12 := 0;

  if DataSet.RecordCount > 0 then
  begin
    DataSet.First;
    while not DataSet.EOF do
    begin
      Result.Quantity := Result.Quantity + DataSet.FieldByName('quantity').AsFloat;
      // Macro-nutrients
      Result.Protein := Result.Protein + DataSet.FieldByName('protein').AsFloat;
      Result.Carbohydrate := Result.Carbohydrate + DataSet.FieldByName('carbohydrate').AsFloat;
      Result.Sugar := Result.Sugar + DataSet.FieldByName('sugar').AsFloat;
      Result.Alcohol := Result.Alcohol + DataSet.FieldByName('alcohol').AsFloat;
      Result.Fibres := Result.Fibres + DataSet.FieldByName('fibres').AsFloat;
      Result.Polyols := Result.Polyols + DataSet.FieldByName('polyols').AsFloat;
      Result.OrganicAcids := Result.OrganicAcids + DataSet.FieldByName('organic_acids').AsFloat;
      Result.Cholesterol := Result.Cholesterol + DataSet.FieldByName('cholesterol').AsFloat;
      Result.Fat := Result.Fat + DataSet.FieldByName('fat').AsFloat;
      Result.Salt := Result.Salt + DataSet.FieldByName('salt').AsFloat;
      Result.Energy := Result.Energy + DataSet.FieldByName('energy').AsFloat;
      // Vitamins
      Result.BetaCarotene := Result.BetaCarotene + DataSet.FieldByName('beta_carotene').AsFloat;
      Result.VitD := Result.VitD + DataSet.FieldByName('vitamin_d').AsFloat;
      Result.VitE := Result.VitE + DataSet.FieldByName('vitamin_e').AsFloat;
      Result.VitK1 := Result.VitK1 + DataSet.FieldByName('vitamin_k1').AsFloat;
      Result.VitK2 := Result.VitK2 + DataSet.FieldByName('vitamin_k2').AsFloat;
      Result.VitC := Result.VitC + DataSet.FieldByName('vitamin_c').AsFloat;
      Result.VitB1 := Result.VitB1 + DataSet.FieldByName('vitamin_b1').AsFloat;
      Result.VitB2 := Result.VitB2 + DataSet.FieldByName('vitamin_b2').AsFloat;
      Result.VitB3 := Result.VitB3 + DataSet.FieldByName('vitamin_b3').AsFloat;
      Result.VitB5 := Result.VitB5 + DataSet.FieldByName('vitamin_b5').AsFloat;
      Result.VitB6 := Result.VitB6 + DataSet.FieldByName('vitamin_b6').AsFloat;
      Result.VitB9 := Result.VitB9 + DataSet.FieldByName('vitamin_b9').AsFloat;
      Result.VitB12 := Result.VitB12 + DataSet.FieldByName('vitamin_b12').AsFloat;
      // Minerals
      Result.Calcium := Result.Calcium + DataSet.FieldByName('calcium').AsFloat;
      Result.Iron := Result.Iron + DataSet.FieldByName('iron').AsFloat;
      Result.Magnesium := Result.Magnesium + DataSet.FieldByName('magnesium').AsFloat;
      Result.Phosphorus := Result.Phosphorus + DataSet.FieldByName('phosphorus').AsFloat;
      Result.Potassium := Result.Potassium + DataSet.FieldByName('potassium').AsFloat;
      Result.Sodium := Result.Sodium + DataSet.FieldByName('sodium').AsFloat;
      Result.Zinc := Result.Zinc + DataSet.FieldByName('zinc').AsFloat;
      DataSet.Next;
    end;
  end;
end;

procedure TForm_Main.UpdateMealStatistics();
var
  SQL: String;
  MealNutrients: TNutrients;
begin
  if (CurrentAccount <> nil) then
  begin
    SQL := 'SELECT * FROM meal AS m, meal_item AS mi ';
    SQL := SQL + 'WHERE m.account_no = ' + IntToStr(CurrentAccount.FAccountNo) + ' ';
    SQL := SQL + 'AND m.creation_date = date(''' + FormatDateTime('yyyy-mm-dd', Calendar1.DateTime) + ''') ';
    SQL := SQL + 'AND mi.meal_no = m.meal_no';
    SQLQuery_MealStatistics.Close;
    SQLQuery_MealStatistics.SQL.Text := SQL;
    SQLQuery_MealStatistics.Open;

    MealNutrients := ComputeMealNutrients(SQLQuery_MealStatistics);
     // Macro-nutrients
    Label_TotalProtein.Caption := FormatFloat('0.00', MealNutrients.Protein);
    Label_TotalCarbohydrate.Caption := FormatFloat('0.00', MealNutrients.Carbohydrate);
    Label_TotalSugar.Caption := FormatFloat('0.00', MealNutrients.Sugar);
    Label_TotalAlcohol.Caption := FormatFloat('0.00', MealNutrients.Alcohol);
    Label_TotalFibres.Caption := FormatFloat('0.00', MealNutrients.Fibres);
    Label_TotalPolyols.Caption := FormatFloat('0.00', MealNutrients.Polyols);
    Label_TotalOrganicAcids.Caption := FormatFloat('0.00', MealNutrients.OrganicAcids);
    Label_TotalFat.Caption := FormatFloat('0.00', MealNutrients.Fat);
    Label_TotalCholesterol.Caption := FormatFloat('0.00', MealNutrients.Cholesterol);
    Label_TotalSalt.Caption := FormatFloat('0.00', MealNutrients.Salt);
    Label_TotalEnergy.Caption := FormatFloat('0.00', MealNutrients.Energy);
    // Vitamins
    Label_TotalBetaCarotene.Caption := FormatFloat('0.00', MealNutrients.BetaCarotene);
    Label_TotalVitD.Caption := FormatFloat('0.00', MealNutrients.VitD);
    Label_TotalVitE.Caption := FormatFloat('0.00', MealNutrients.VitE);
    Label_TotalVitK1.Caption := FormatFloat('0.00', MealNutrients.VitK1);
    Label_TotalVitK2.Caption := FormatFloat('0.00', MealNutrients.VitK2);
    Label_TotalVitC.Caption := FormatFloat('0.00', MealNutrients.VitC);
    Label_TotalVitB1.Caption := FormatFloat('0.00', MealNutrients.VitB1);
    Label_TotalVitB2.Caption := FormatFloat('0.00', MealNutrients.VitB2);
    Label_TotalVitB3.Caption := FormatFloat('0.00', MealNutrients.VitB3);
    Label_TotalVitB5.Caption := FormatFloat('0.00', MealNutrients.VitB5);
    Label_TotalVitB6.Caption := FormatFloat('0.00', MealNutrients.VitB6);
    Label_TotalVitB9.Caption := FormatFloat('0.00', MealNutrients.VitB9);
    Label_TotalVitB12.Caption := FormatFloat('0.00', MealNutrients.VitB12);
    // Minerals
    Label_TotalCalcium.Caption := FormatFloat('0.00', MealNutrients.Calcium);
    Label_TotalIron.Caption := FormatFloat('0.00', MealNutrients.Iron);
    Label_TotalMagnesium.Caption := FormatFloat('0.00', MealNutrients.Magnesium);
    Label_TotalPhosphorus.Caption := FormatFloat('0.00', MealNutrients.Phosphorus);
    Label_TotalPotassium.Caption := FormatFloat('0.00', MealNutrients.Potassium);
    Label_TotalSodium.Caption := FormatFloat('0.00', MealNutrients.Sodium);
    Label_TotalZinc.Caption := FormatFloat('0.00', MealNutrients.Zinc);
  end
  else
  begin
    // Macro-nutrients
    Label_TotalProtein.Caption := FormatFloat('0.00', 0);
    Label_TotalCarbohydrate.Caption := FormatFloat('0.00', 0);
    Label_TotalSugar.Caption := FormatFloat('0.00', 0);
    Label_TotalAlcohol.Caption := FormatFloat('0.00', 0);
    Label_TotalFibres.Caption := FormatFloat('0.00', 0);
    Label_TotalPolyols.Caption := FormatFloat('0.00', 0);
    Label_TotalOrganicAcids.Caption := FormatFloat('0.00', 0);
    Label_TotalFat.Caption := FormatFloat('0.00', 0);
    Label_TotalCholesterol.Caption := FormatFloat('0.00', 0);
    Label_TotalSalt.Caption := FormatFloat('0.00', 0);
    Label_TotalEnergy.Caption := FormatFloat('0.00', 0);
    // Vitamins
    Label_TotalBetaCarotene.Caption := FormatFloat('0.00', 0);
    Label_TotalVitD.Caption := FormatFloat('0.00', 0);
    Label_TotalVitE.Caption := FormatFloat('0.00', 0);
    Label_TotalVitK1.Caption := FormatFloat('0.00', 0);
    Label_TotalVitK2.Caption := FormatFloat('0.00', 0);
    Label_TotalVitC.Caption := FormatFloat('0.00', 0);
    Label_TotalVitB1.Caption := FormatFloat('0.00', 0);
    Label_TotalVitB2.Caption := FormatFloat('0.00', 0);
    Label_TotalVitB3.Caption := FormatFloat('0.00', 0);
    Label_TotalVitB5.Caption := FormatFloat('0.00', 0);
    Label_TotalVitB6.Caption := FormatFloat('0.00', 0);
    Label_TotalVitB9.Caption := FormatFloat('0.00', 0);
    Label_TotalVitB12.Caption := FormatFloat('0.00', 0);
    // Minerals
    Label_TotalCalcium.Caption := FormatFloat('0.00', 0);
    Label_TotalIron.Caption := FormatFloat('0.00', 0);
    Label_TotalMagnesium.Caption := FormatFloat('0.00', 0);
    Label_TotalPhosphorus.Caption := FormatFloat('0.00', 0);
    Label_TotalPotassium.Caption := FormatFloat('0.00', 0);
    Label_TotalSodium.Caption := FormatFloat('0.00', 0);
    Label_TotalZinc.Caption := FormatFloat('0.00', 0);
  end;
end;

function TForm_Main.ComputeMealStatistics(const StartDate: TDate; const EndDate: TDate): TNutrients;
var
  SQL: String;
begin
  if (CurrentAccount <> nil) then
  begin
    SQL := 'SELECT * FROM meal AS m, meal_item AS mi ';
    SQL := SQL + 'WHERE m.account_no = ' + IntToStr(CurrentAccount.FAccountNo) + ' ';
    SQL := SQL + 'AND m.creation_date >= date(''' + FormatDateTime('yyyy-mm-dd', StartDate) + ''') ';
    SQL := SQL + 'AND m.creation_date <= date(''' + FormatDateTime('yyyy-mm-dd', EndDate) + ''') ';
    SQL := SQL + 'AND mi.meal_no = m.meal_no';
    SQLQuery_MealStatistics.Close;
    SQLQuery_MealStatistics.SQL.Text := SQL;
    SQLQuery_MealStatistics.Open;
    Result := ComputeMealNutrients(SQLQuery_MealStatistics);
  end;
end;

function TForm_Main.ComputeTotalWorkoutCalories(const StartDate: TDate; const EndDate: TDate): integer;
var
  SQL: string;
begin
  Result := 0;
  if CurrentAccount <> nil then
  begin
    SQL := 'SELECT SUM(calories) FROM workout WHERE' + ' account_no = ' +
      IntToStr(CurrentAccount.FAccountNo) + ' AND creation_date >=''' +
      FormatDateTime('yyyy-mm-dd', StartDate) + '''' + ' AND creation_date <=''' +
      FormatDateTime('yyyy-mm-dd', EndDate) + '''';
    SQLQuery_DailyWorkoutStatistics.Close;
    SQLQuery_DailyWorkoutStatistics.SQL.Clear;
    SQLQuery_DailyWorkoutStatistics.SQL.Text := SQL;
    SQLQuery_DailyWorkoutStatistics.Open;
    if SQLQuery_DailyWorkoutStatistics.RecordCount <> 0 then
      if SQLQuery_DailyWorkoutStatistics.Fields[0].IsNull = False then
        Result := SQLQuery_DailyWorkoutStatistics.Fields[0].AsInteger
      else
        Result := 0;
  end;
end;

function TForm_Main.ComputeTotalWorkoutTime(const StartDate: TDate;
  const EndDate: TDate): string;
var
  SQL: string;
begin
  Result := '00:00:00';
  if CurrentAccount <> nil then
  begin
    SQL :=
      'SELECT time(SUM(strftime(''%s'', duration) - strftime(''%s'', ''00:00:00'')), ''unixepoch'') FROM workout WHERE'
      + ' account_no = ' + IntToStr(CurrentAccount.FAccountNo) +
      ' AND creation_date >=''' + FormatDateTime('yyyy-mm-dd', StartDate) +
      '''' + ' AND creation_date <=''' + FormatDateTime('yyyy-mm-dd', EndDate) + '''';
    SQLQuery_DailyWorkoutStatistics.Close;
    SQLQuery_DailyWorkoutStatistics.SQL.Clear;
    SQLQuery_DailyWorkoutStatistics.SQL.Text := SQL;
    SQLQuery_DailyWorkoutStatistics.Open;
    if SQLQuery_DailyWorkoutStatistics.RecordCount <> 0 then
      if SQLQuery_DailyWorkoutStatistics.Fields[0].IsNull = False then
        Result := SQLQuery_DailyWorkoutStatistics.Fields[0].AsString
      else
        Result := '00:00:00';
  end;
end;

function TForm_Main.ComputeNumberWorkouts(const StartDate: TDate;
  const EndDate: TDate): integer;
var
  SQL: string;
begin
  Result := 0;
  if CurrentAccount <> nil then
  begin
    SQL := 'SELECT COUNT(duration) FROM workout WHERE' + ' account_no = ' +
      IntToStr(CurrentAccount.FAccountNo) + ' AND creation_date >=''' +
      FormatDateTime('yyyy-mm-dd', StartDate) + '''' + ' AND creation_date <=''' +
      FormatDateTime('yyyy-mm-dd', EndDate) + '''';
    SQLQuery_DailyWorkoutStatistics.Close;
    SQLQuery_DailyWorkoutStatistics.SQL.Clear;
    SQLQuery_DailyWorkoutStatistics.SQL.Text := SQL;
    SQLQuery_DailyWorkoutStatistics.Open;
    if SQLQuery_DailyWorkoutStatistics.RecordCount <> 0 then
      if SQLQuery_DailyWorkoutStatistics.Fields[0].IsNull = False then
        Result := SQLQuery_DailyWorkoutStatistics.Fields[0].AsInteger
      else
        Result := 0;
  end;
end;

procedure TForm_Main.UpdateStatistics();
var
  BMR: real;
  TotalWorkoutCalories: integer;
  TotalExpenses: real;
  Nutrients: TNutrients;
  CaloricBalance: Real;
  NumDaysBetweenDates: integer; // including start date and end date
  TotalWorkoutTime: string;
  NumberWorkouts: integer;
begin
  if CurrentAccount <> nil then
  begin
    // Daily statistics
    BMR := CurrentAccount.ComputeBMR();
    TotalWorkoutCalories := ComputeTotalWorkoutCalories(Calendar1.DateTime,
      Calendar1.DateTime);
    TotalExpenses := BMR + TotalWorkoutCalories;
    Nutrients := ComputeMealStatistics(Calendar1.DateTime, Calendar1.DateTime);
    CaloricBalance := Nutrients.Energy - TotalExpenses;
    TotalWorkoutTime := ComputeTotalWorkoutTime(Calendar1.DateTime, Calendar1.DateTime);
    NumberWorkouts := ComputeNumberWorkouts(Calendar1.DateTime, Calendar1.DateTime);

    Label_AmountDailyBMR.Caption := FormatFloat('0.00', BMR);
    Label_AmountDailyWorkoutCalories.Caption := FormatFloat('0.00', TotalWorkoutCalories);
    Label_AmountDailyExpenses.Caption := FormatFloat('0.00', TotalExpenses);
    Label_AmountDailyMealCalories.Caption := FormatFloat('0.00', Nutrients.Energy);
    Label_AmountDailyProteins.Caption := FormatFloat('0.00', Nutrients.Protein);
    Label_AmountDailyFat.Caption := FormatFloat('0.00', Nutrients.Fat);
    Label_AmountDailyCarbohydrates.Caption := FormatFloat('0.00', Nutrients.Carbohydrate);
    Label_AmountCaloricBalance.Caption := FormatFloat('0.00', CaloricBalance);

    if CaloricBalance <=0 then
    begin
      Label_DailyCaloricBalance.Font.Color := clBlue;
      Label_AmountCaloricBalance.Font.Color := clBlue
    end
    else
    begin
      Label_DailyCaloricBalance.Font.Color := clRed;
      Label_AmountCaloricBalance.Font.Color := clRed;
    end;

    Label_AmountDailyWorkoutTime.Caption := TotalWorkoutTime;
    Label_AmountDailyNumberWorkouts.Caption := IntToStr(NumberWorkouts);

    Chart_DailyCalories.Title.Text[0] := SCalorieBreakdown;
    Chart_DailyCaloriesSlice.Clear;
    if Nutrients.Protein > 0 then
      Chart_DailyCaloriesSlice.AddPie(Nutrients.Protein * 4, SProtein, clBlue);
    if Nutrients.Fat > 0 then
      Chart_DailyCaloriesSlice.AddPie(Nutrients.Fat * 7, SFat, clRed);
    if Nutrients.Carbohydrate > 0 then
      Chart_DailyCaloriesSlice.AddPie(Nutrients.Carbohydrate * 4, SCarbohydrate, clGreen);

    // Weekly statistics
    NumDaysBetweenDates := DaysBetween(EndOfTheWeek(Calendar1.DateTime),
      StartOfTheWeek(Calendar1.DateTime)) + 1;
    BMR := CurrentAccount.ComputeBMR() * NumDaysBetweenDates;
    TotalWorkoutCalories := ComputeTotalWorkoutCalories(
      StartOfTheWeek(Calendar1.DateTime), EndOfTheWeek(Calendar1.DateTime));
    TotalExpenses := BMR + TotalWorkoutCalories;
    Nutrients := ComputeMealStatistics(StartOfTheWeek(Calendar1.DateTime), EndOfTheWeek(Calendar1.DateTime));
    CaloricBalance := Nutrients.Energy - TotalExpenses;
    TotalWorkoutTime := ComputeTotalWorkoutTime(StartOfTheWeek(Calendar1.DateTime),
      EndOfTheWeek(Calendar1.DateTime));
    NumberWorkouts := ComputeNumberWorkouts(StartOfTheWeek(Calendar1.DateTime),
      EndOfTheWeek(Calendar1.DateTime));

    Label_AmountWeeklyBMR.Caption := FormatFloat('0.00', BMR);
    Label_AmountWeeklyWorkoutCalories.Caption := FormatFloat('0.00', TotalWorkoutCalories);
    Label_AmountWeeklyExpenses.Caption := FormatFloat('0.00', TotalExpenses);
    Label_AmountWeeklyMealCalories.Caption := FormatFloat('0.00', Nutrients.Energy);
    Label_AmountWeeklyProteins.Caption := FormatFloat('0.00', Nutrients.Protein);
    Label_AmountWeeklyFat.Caption := FormatFloat('0.00', Nutrients.Fat);
    Label_AmountWeeklyCarbohydrates.Caption := FormatFloat('0.00', Nutrients.Carbohydrate);
    Label_AmountWeeklyCaloricBalance.Caption := FormatFloat('0.00', CaloricBalance);

    if CaloricBalance <=0 then
    begin
      Label_WeeklyCaloricBalance.Font.Color := clBlue;
      Label_AmountWeeklyCaloricBalance.Font.Color := clBlue
    end
    else
    begin
      Label_WeeklyCaloricBalance.Font.Color := clRed;
      Label_AmountWeeklyCaloricBalance.Font.Color := clRed;
    end;

    Label_AmountWeeklyWorkoutTime.Caption := TotalWorkoutTime;
    Label_AmountWeeklyNumberWorkouts.Caption := IntToStr(NumberWorkouts);

    Chart_WeeklyCalories.Title.Text[0] := SCalorieBreakdown;
    Chart_WeeklyCaloriesSlice.Clear;
    Chart_WeeklyCaloriesSlice.Title := SCalorieBreakdown;
    if Nutrients.Protein > 0 then
      Chart_WeeklyCaloriesSlice.AddPie(Nutrients.Protein * 4, SProtein, clBlue);
    if Nutrients.Fat > 0 then
      Chart_WeeklyCaloriesSlice.AddPie(Nutrients.Fat * 7, SFat, clRed);
    if Nutrients.Carbohydrate > 0 then
      Chart_WeeklyCaloriesSlice.AddPie(Nutrients.Carbohydrate * 4, SCarbohydrate, clGreen);

    // Monthly statistics
    NumDaysBetweenDates := DaysBetween(EndOfTheMonth(Calendar1.DateTime),
      StartOfTheMonth(Calendar1.DateTime)) + 1;
    BMR := CurrentAccount.ComputeBMR() * NumDaysBetweenDates;
    TotalWorkoutCalories := ComputeTotalWorkoutCalories(
      StartOfTheMonth(Calendar1.DateTime), EndOfTheMonth(Calendar1.DateTime));
    TotalExpenses := BMR + TotalWorkoutCalories;
    Nutrients := ComputeMealStatistics(StartOfTheMonth(Calendar1.DateTime), EndOfTheMonth(Calendar1.DateTime));
    CaloricBalance := Nutrients.Energy - TotalExpenses;
    TotalWorkoutTime := ComputeTotalWorkoutTime(StartOfTheMonth(Calendar1.DateTime),
      EndOfTheMonth(Calendar1.DateTime));
    NumberWorkouts := ComputeNumberWorkouts(StartOfTheMonth(Calendar1.DateTime),
      EndOfTheMonth(Calendar1.DateTime));

    Label_AmountMonthlyBMR.Caption := FormatFloat('0.00', BMR);
    Label_AmountMonthlyWorkoutCalories.Caption := FormatFloat('0.00', TotalWorkoutCalories);
    Label_AmountMonthlyExpenses.Caption := FormatFloat('0.00', TotalExpenses);
    Label_AmountMonthlyMealCalories.Caption := FormatFloat('0.00', Nutrients.Energy);
    Label_AmountMonthlyProteins.Caption := FormatFloat('0.00', Nutrients.Protein);
    Label_AmountMonthlyFat.Caption := FormatFloat('0.00', Nutrients.Fat);
    Label_AmountMonthlyCarbohydrates.Caption := FormatFloat('0.00', Nutrients.Carbohydrate);
    Label_AmountMonthlyCaloricBalance.Caption := FormatFloat('0.00', CaloricBalance);

    if CaloricBalance <=0 then
    begin
      Label_MonthlyCaloricBalance.Font.Color := clBlue;
      Label_AmountMonthlyCaloricBalance.Font.Color := clBlue
    end
    else
    begin
      Label_MonthlyCaloricBalance.Font.Color := clRed;
      Label_AmountMonthlyCaloricBalance.Font.Color := clRed;
    end;

    Label_AmountMonthlyWorkoutTime.Caption := TotalWorkoutTime;
    Label_AmountMonthlyNumberWorkouts.Caption := IntToStr(NumberWorkouts);

    Chart_MonthlyCalories.Title.Text[0] := SCalorieBreakdown;
    Chart_MonthlyCaloriesSlice.Clear;
    if Nutrients.Protein > 0 then
      Chart_MonthlyCaloriesSlice.AddPie(Nutrients.Protein * 4, SProtein, clBlue);
    if Nutrients.Fat > 0 then
      Chart_MonthlyCaloriesSlice.AddPie(Nutrients.Fat * 7, SFat, clRed);
    if  Nutrients.Carbohydrate > 0 then
      Chart_MonthlyCaloriesSlice.AddPie(Nutrients.Carbohydrate * 4, SCarbohydrate, clGreen);
  end
  else
  begin
    Label_AmountDailyBMR.Caption := FormatFloat('0.00', 0);
    Label_AmountDailyWorkoutCalories.Caption := FormatFloat('0.00', 0);
    Label_AmountDailyExpenses.Caption := FormatFloat('0.00', 0);
    Label_AmountDailyMealCalories.Caption := FormatFloat('0.00', 0);
    Label_AmountDailyProteins.Caption := FormatFloat('0.00', 0);
    Label_AmountDailyFat.Caption := FormatFloat('0.00', 0);
    Label_AmountDailyCarbohydrates.Caption := FormatFloat('0.00', 0);
    Label_AmountCaloricBalance.Caption := FormatFloat('0.00', 0);
    Chart_DailyCaloriesSlice.Clear;
    Label_AmountDailyWorkoutTime.Caption := '00:00:00';
    Label_AmountDailyNumberWorkouts.Caption := '0';

    Label_AmountWeeklyBMR.Caption := FormatFloat('0.00', 0);
    Label_AmountWeeklyWorkoutCalories.Caption := FormatFloat('0.00', 0);
    Label_AmountWeeklyExpenses.Caption := FormatFloat('0.00', 0);
    Label_AmountWeeklyMealCalories.Caption := FormatFloat('0.00', 0);
    Label_AmountWeeklyProteins.Caption := FormatFloat('0.00', 0);
    Label_AmountWeeklyFat.Caption := FormatFloat('0.00', 0);
    Label_AmountWeeklyCarbohydrates.Caption := FormatFloat('0.00', 0);
    Label_AmountWeeklyCaloricBalance.Caption := FormatFloat('0.00', 0);
    Chart_WeeklyCaloriesSlice.Clear;
    Label_AmountWeeklyWorkoutTime.Caption := '00:00:00';
    Label_AmountWeeklyNumberWorkouts.Caption := IntToStr(0);

    Label_AmountMonthlyBMR.Caption := FormatFloat('0.00', 0);
    Label_AmountMonthlyWorkoutCalories.Caption := FormatFloat('0.00', 0);
    Label_AmountMonthlyExpenses.Caption := FormatFloat('0.00', 0);
    Label_AmountMonthlyMealCalories.Caption := FormatFloat('0.00', 0);
    Label_AmountMonthlyProteins.Caption := FormatFloat('0.00', 0);
    Label_AmountMonthlyFat.Caption := FormatFloat('0.00', 0);
    Label_AmountMonthlyCarbohydrates.Caption := FormatFloat('0.00', 0);
    Label_AmountMonthlyCaloricBalance.Caption := FormatFloat('0.00', 0);
    Chart_MonthlyCaloriesSlice.Clear;
    Label_AmountMonthlyWorkoutTime.Caption := '00:00:00';
    Label_AmountMonthlyNumberWorkouts.Caption := IntToStr(0);
  end;
end;

procedure TForm_Main.FormShow(Sender: TObject);
var
  GlobalHealthLabels: TStringList;
  BodyMeasurementsLabels: TStringList;
  i: Integer;
begin
  CurrentAccount := nil;
  CurrentWorkout := nil;
  CurrentMeal := nil;
  CurrentMeasurement := nil;
  for i := Low(CurrentPictures) to High(CurrentPictures) do
  begin
    CurrentPictures[i] := nil;
  end;
  Calendar1.DateTime := Now; // Initialize calendar to current date/time

  { Initialize global health categories }
  GlobalHealthLabels := TStringList.Create;
  GlobalHealthLabels.Add(SDBGlobalHealth_Weight); // Id. 0
  GlobalHealthLabels.Add(SDBGlobalHealth_BMI); // Id. 1
  GlobalHealthLabels.Add(SDBGlobalHealth_Sleep); // Id. 2
  GlobalHealthLabels.Add(SDBGlobalHealth_BloodPressure); // Id. 3
  ComboBox_GlobalHealthCategory.Items := GlobalHealthLabels;
  ComboBox_GlobalHealthCategory.ItemIndex := 0;
  GlobalHealthLabels.Free;

  DateTimePicker_GlobalHealthStartDate.Date := IncMonth(Now, -1);
  DateTimePicker_GlobalHealthEndDate.Date := Now;

  { Initialize body measurements categories }
  BodyMeasurementsLabels := TStringList.Create;
  BodyMeasurementsLabels.Add(SDBBodyMeasurements_Neck); // Id. 0
  BodyMeasurementsLabels.Add(SDBBodyMeasurements_Shoulders); // Id. 1
  BodyMeasurementsLabels.Add(SDBBodyMeasurements_Chest); // Id. 2
  BodyMeasurementsLabels.Add(SDBBodyMeasurements_Arms); // Id. 3
  BodyMeasurementsLabels.Add(SDBBodyMeasurements_Forearms); // Id. 4
  BodyMeasurementsLabels.Add(SDBBodyMeasurements_MidSection); // Id. 5
  BodyMeasurementsLabels.Add(SDBBodyMeasurements_Hips); // Id. 6
  BodyMeasurementsLabels.Add(SDBBodyMeasurements_Thighs); // Id. 7
  BodyMeasurementsLabels.Add(SDBBodyMeasurements_Calves); // Id. 8
  ComboBox_BodyMeasurementsCategory.Items := BodyMeasurementsLabels;
  ComboBox_BodyMeasurementsCategory.ItemIndex := 0;
  BodyMeasurementsLabels.Free;

  DateTimePicker_BodyMeasurementsStartDate.Date := IncMonth(Now, -1);
  DateTimePicker_BodyMeasurementsEndDate.Date := Now;

  UpdateAllLayouts();

  // Controls management
  Chart_DailyCalories.Title.Text[0] := SCalorieBreakdown;
  Chart_WeeklyCalories.Title.Text[0] := SCalorieBreakdown;
  Chart_MonthlyCalories.Title.Text[0] := SCalorieBreakdown;
  TabSheet_DailyMeals.Show; // Make sure first panel is selected by default
  TabSheet_DailyStatistics.Show; // Make sure first panel is selected by default

  if DBConnection.DatabaseName = '' then
  begin
    MenuItem_File_OpenAccount.Enabled := False;
    MenuItem_File_Backup.Enabled := False;
    MenuItem_File_Export.Enabled := False;
    MenuItem_File_Import.Enabled := False;
    ToolButton_OpenAccount.Enabled := False;
    MenuItem_Edit_Foods.Enabled := False;
    ToolButton_Foods.Enabled := False;
    MenuItem_Edit_Exercises.Enabled := False;
    ToolButton_Exercises.Enabled := False;
  end;

  if CurrentAccount = nil then
  begin
    MenuItem_File_CloseAccount.Enabled := False;
    MenuItem_Edit_Workouts.Enabled := False;
    MenuItem_Edit_Pictures.Enabled := False;
    ToolButton_Workouts.Enabled := False;
    ToolButton_Pictures.Enabled := False;
    Panel_Day_Summary.Enabled := False;
  end;
end;

procedure TForm_Main.MenuItem_EditClick(Sender: TObject);
begin
  if CurrentAccount = nil then
  begin
    MenuItem_Edit_Workouts.Enabled := False;
    MenuItem_Edit_Pictures.Enabled := False;
  end
  else
  begin
    MenuItem_Edit_Workouts.Enabled := True;
    MenuItem_Edit_Pictures.Enabled := True;
  end;
end;

procedure TForm_Main.MenuItem_Edit_ExercisesClick(Sender: TObject);
begin
  Form_ExerciseList.FromListOfExercises := True;
  Form_ExerciseList.ShowModal;
end;

procedure TForm_Main.MenuItem_Edit_PicturesClick(Sender: TObject);
begin
  Form_PictureList.Show;
end;

procedure TForm_Main.MenuItem_Edit_WorkoutsClick(Sender: TObject);
begin
  Form_WorkoutList.ShowModal;
end;

procedure TForm_Main.MenuItem_File_BackupClick(Sender: TObject);
begin
  Form_Backup.ShowModal;
end;

procedure TForm_Main.Calendar1Change(Sender: TObject);
begin
  UpdateAllLayouts();
end;

procedure TForm_Main.Calendar1Click(Sender: TObject);
begin
  Calendar1Change(Sender);
end;

procedure TForm_Main.ComboBox_BodyMeasurementsCategoryChange(Sender: TObject);
begin
  RefreshBodyMeasurementsPlot();
end;

procedure TForm_Main.ComboBox_GlobalHealthCategoryChange(Sender: TObject);
begin
  RefreshGlobalHealthPlot();
end;

procedure TForm_Main.DateTimePicker_BodyMeasurementsEndDateChange(
  Sender: TObject);
begin
  RefreshBodyMeasurementsPlot();
end;

procedure TForm_Main.DateTimePicker_BodyMeasurementsStartDateChange(
  Sender: TObject);
begin
  RefreshBodyMeasurementsPlot();
end;

procedure TForm_Main.DateTimePicker_GlobalHealthEndDateChange(Sender: TObject);
begin
  RefreshGlobalHealthPlot();
end;

procedure TForm_Main.DateTimePicker_GlobalHealthStartDateChange(Sender: TObject
  );
begin
  RefreshGlobalHealthPlot();
end;

procedure TForm_Main.DBGrid_MealListCellClick(Column: TColumn);
begin
  UpdateMealItemList();
end;

procedure TForm_Main.Button_TodayClick(Sender: TObject);
begin
  // Reset date pickers
  Calendar1.DateTime := Now;
  Calendar1Change(Sender);
end;

procedure TForm_Main.Button_AddWorkoutClick(Sender: TObject);
begin
  // Initialize workout types
  Form_Workout.ComboBox_WorkoutType.ItemIndex := 0;
  // Show workout form
  Form_Workout.ShowModal;
  UpdateAllLayouts();
end;

procedure TForm_Main.Button_DeleteMealClick(Sender: TObject);
var
  SQL: string;
  MealNo: String;
begin
  case QuestionDlg(SDlgDeleteMealTitle, SDlgDeleteMealMsg,
      mtCustom, [mrNo, SDlgBtnNo, 'IsDefault', mrYes, SDlgBtnYes], '') of
    mrYes:
    begin
      MealNo:=DBGrid_MealList.DataSource.DataSet.FieldByName('meal_no').AsString;
      { Delete meal items }
      SQL := 'DELETE FROM meal_item WHERE meal_no = ' + MealNo;
      // Execute SQL query
      SQLQuery_MealItems.Close;
      SQLQuery_MealItems.SQL.Clear;
      SQLQuery_MealItems.SQL.Text := SQL;
      SQLQuery_MealItems.ExecSQL;
      SQLTransaction1.Commit;

      { Delete meal }
      SQL := 'DELETE FROM meal WHERE meal_no = ' + MealNo;
      // Execute SQL query
      SQLQuery_Meals.Close;
      SQLQuery_Meals.SQL.Clear;
      SQLQuery_Meals.SQL.Text := SQL;
      SQLQuery_Meals.ExecSQL;
      SQLTransaction1.Commit;

      UpdateMealList();
      UpdateMealItemList();
      UpdateStatistics();
    end;
    mrNo:
    begin

    end;
    mrCancel:
    begin

    end;
  end;
end;

procedure TForm_Main.Button_DeletePicture1Click(Sender: TObject);
begin
  DeletePicture(Image1, 1);
  Button_DeletePicture1.Enabled := False;
  UpdateAllLayouts();
end;

procedure TForm_Main.Button_DeletePicture2Click(Sender: TObject);
begin
  DeletePicture(Image2, 2);
  Button_DeletePicture2.Enabled := False;
  UpdateAllLayouts();
end;

procedure TForm_Main.Button_DeletePicture3Click(Sender: TObject);
begin
  DeletePicture(Image3, 3);
  Button_DeletePicture3.Enabled := False;
  UpdateAllLayouts();
end;

procedure TForm_Main.Button_DeletePicture4Click(Sender: TObject);
begin
  DeletePicture(Image4, 4);
  Button_DeletePicture4.Enabled := False;
  UpdateAllLayouts();
end;

procedure TForm_Main.Button_AddMealClick(Sender: TObject);
begin
  CurrentMeal := TSSMeal.Create;
  CurrentMeal.FMealNo := -1; // Meal creation flag
  CurrentMeal.FAccountNo := CurrentAccount.FAccountNo; // Pass account_no
  Form_Meal.ShowModal;
  FreeAndNil(CurrentMeal);
  UpdateMealList();
  UpdateMealItemList();
  UpdateStatistics();
end;

procedure TForm_Main.Button_AddPicture1Click(Sender: TObject);
begin
  AddPicture(Image1, 1);
  Button_DeletePicture1.Enabled := True;
  UpdateAllLayouts();
end;

procedure TForm_Main.Button_AddPicture2Click(Sender: TObject);
begin
  AddPicture(Image2, 2);
  Button_DeletePicture2.Enabled := True;
  UpdateAllLayouts();
end;

procedure TForm_Main.Button_AddPicture3Click(Sender: TObject);
begin
  AddPicture(Image3, 3);
  Button_DeletePicture3.Enabled := True;
  UpdateAllLayouts();
end;

procedure TForm_Main.Button_AddPicture4Click(Sender: TObject);
begin
  AddPicture(Image4, 4);
  Button_DeletePicture4.Enabled := True;
  UpdateAllLayouts();
end;

procedure TForm_Main.Button_DeleteWorkoutClick(Sender: TObject);
var
  SQL: string;
begin
  SQL := 'DELETE FROM workout WHERE workout_no = ' +
    DBGrid_Workouts.DataSource.DataSet.FieldByName('workout_no').AsString;

  case QuestionDlg(SDlgDeleteWorkoutTitle, SDlgDeleteWorkoutMsg,
      mtCustom, [mrNo, SDlgBtnNo, 'IsDefault', mrYes, SDlgBtnYes], '') of
    mrYes:
    begin
      // Execute SQL query
      SQLQuery_Workout.Close;
      SQLQuery_Workout.SQL.Clear;
      SQLQuery_Workout.SQL.Text := SQL;
      SQLQuery_Workout.ExecSQL;
      SQLTransaction1.Commit;

      UpdateAllLayouts();
    end;
    mrNo:
    begin

    end;
    mrCancel:
    begin

    end;
  end;
end;

procedure TForm_Main.Button_EditMealClick(Sender: TObject);
var MealNo: Integer;
begin
  MealNo := SQLQuery_Meals.FieldByName('meal_no').AsLongint;
  CurrentMeal := TSSMeal.Create;
  CurrentMeal.DBSelect(DBConnection, MealNo);
  if Form_Meal.ShowModal = mrOK then
     UpdateAllLayouts();
  FreeAndNil(CurrentMeal);
end;

procedure TForm_Main.Button_EditWorkoutClick(Sender: TObject);
var
  WorkoutNo: longint;
begin
  WorkoutNo := DBGrid_Workouts.DataSource.DataSet.FieldByName('workout_no').AsLongint;
  CurrentWorkout := TSSWorkout.Create;
  DBSelectWorkout(CurrentWorkout, WorkoutNo);
  Form_Workout.ShowModal;
  FreeAndNil(CurrentWorkout);
  if Form_Workout.ModalResult = mrOK then
    UpdateAllLayouts();
end;

procedure TForm_Main.Button_Measurements_DeleteClick(Sender: TObject);
begin
  case QuestionDlg(SDlgDeleteMeasurementsTitle, SDlgDeleteMeasurementsMsg,
      mtCustom, [mrNo, SDlgBtnNo, 'IsDefault', mrYes, SDlgBtnYes], '') of
    mrYes:
    begin
      CurrentMeasurement.DBDelete(DBConnection, SQLTransaction1);
      UpdateAllLayouts();
    end;
    mrNo:
    begin

    end;
    mrCancel:
    begin

    end;
  end;
end;

procedure TForm_Main.Button_Measurements_EditClick(Sender: TObject);
begin
  Form_Measurements.ShowModal;
  UpdateAllLayouts();
end;

procedure TForm_Main.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction := caFree;
  DBConnection.Connected := False;
  SaveConfigurationFile(ApplicationSettings);
  MRUMenuManager_MostRecentFiles.SaveRecentFilesToIni();
end;

procedure TForm_Main.FormCreate(Sender: TObject);
var
  FullDatabasePath: string;
begin
  { ----------------- }
  { StaySharp version }
  { ----------------- }
  StaySharpVersion := '0.8.11'; // Set Application version
  StaySharpSchemaVersion := '2'; // Set supported schema version for user data migration
  StaySharpSchemaVersionCreationDate := '2018-09-21';

  { ---------------- }
  { Max picture size }
  { ---------------- }
  MaxPictureSize := 600 * 1024; // 600 ko

  { ------------------------------ }
  { Configuration files management }
  { ------------------------------ }
  ApplicationSettings := TSSApplicationSettings.Create;
  ReadConfigurationFile(ApplicationSettings);
  with MRUMenuManager_MostRecentFiles do begin
    IniFileName := ApplicationSettings.ConfigurationFile;
    LoadRecentFilesFromIni;
  end;
  ApplicationSettings.ActivitiesPanelHeight := 450;

  { ----------------------------------- }
  { StaySharp master database full path }
  { ----------------------------------- }
  {$IFDEF Win32}
  AppDataPath := '';
  SHGetSpecialFolderPath(0, AppDataPath, CSIDL_LOCAL_APPDATA, False);
  //FullDatabasePath := AppDataPath + '\StaySharp\' + 'staysharp.sqlite'; // for MS Windows deployment
  //FullDatabasePath := 'C:\Users\fabri\Projets\staysharp\src\gui\staysharp.sqlite'; // for MS Windows dev.
  //DBConnection.DatabaseName := FullDatabasePath;
  {$ELSE}
  AppDataFolder := ExtractFilePath(Application.ExeName);
  //ShowMessage(AppDataFolder);
  {$IFDEF DARWIN}
  SQLiteLibraryName := AppDataFolder + 'libsqlite3.dylib';
  {$ELSE}
  SQLiteLibraryName := AppDataFolder + 'libsqlite3.so';
  {$ENDIF}
  //ShowMessage(SQLiteLibraryName);
  //FullDatabasePath := AppDataFolder + 'staysharp.sqlite';
  //DBConnection.DatabaseName := FullDatabasePath;
  {$ENDIF}
end;

procedure TForm_Main.MenuItem_File_CloseAccountClick(Sender: TObject);
begin
  BusinessObjectsCleanUp();

  // Update layout
  MenuItem_File_CloseAccount.Enabled := False;
  MenuItem_Edit_Workouts.Enabled := False;
  MenuItem_Edit_Pictures.Enabled := False;
  ToolButton_Workouts.Enabled := False;
  ToolButton_Pictures.Enabled := False;
  Panel_Day_Summary.Enabled := False;

  UpdateAllLayouts();
end;

procedure TForm_Main.MenuItem_File_ExportClick(Sender: TObject);
var
  Doc: TXMLDocument;
  RootNode, AccountsNode, AccountNode, MealsNode, MealNode, MealItemsNode,
    MealItemNode, WorkoutsNode, WorkoutNode, FoodsNode, FoodNode, ItemNode,
    TextNode: TDOMNode;
var
  QuerySchema: TSQLQuery;
  QueryAccounts: TSQLQuery;
  QueryMeals: TSQLQuery;
  QueryMealItems: TSQLQuery;
  QueryWorkouts: TSQLQuery;
  QueryFoods: TSQLQuery;
  Msg: String;
begin
  if SaveDialog_Export.Execute then
  begin
    if FileExists(SaveDialog_Export.FileName) then
    begin
      { Ask the user if he wants to overwrite file if it exists }
      Msg := '''' + SaveDialog_Export.FileName + ''' ' + SDlgOverwriteFileMsg;
      case QuestionDlg(SDlgExportTitle, Msg, mtCustom, [mrNo, SDlgBtnNo,
          'IsDefault', mrYes, SDlgBtnYes], '') of
        mrYes:
        begin
          DeleteFile(SaveDialog_Export.FileName);
        end;
        mrNo:
        begin
          Exit;
        end;
        mrCancel:
        begin

        end;
      end;
    end;

    // Create a document
    Doc := TXMLDocument.Create;

    // Create a root node
    RootNode := Doc.CreateElement('staysharp_datbase');
    Doc.Appendchild(RootNode);
    RootNode := Doc.DocumentElement; // create a parent node

    { Get Schema version }
    QuerySchema := TSQLQuery.Create(nil);
    QuerySchema.DataBase := DBConnection;
    QuerySchema.Clear;
    QuerySchema.SQL.Text := 'SELECT schema_version FROM staysharpsys';
    QuerySchema.Open;
    QuerySchema.First;

    ItemNode := Doc.CreateElement('schema_version');
    TextNode := Doc.CreateTextNode(QuerySchema.FieldByName('schema_version').AsWideString); // insert a value to node
    ItemNode.Appendchild(TextNode); // save node
    RootNode.AppendChild(ItemNode); // insert child node in respective parent node

    QuerySchema.Close;
    FreeAndNil(QuerySchema);

    { -------- }
    { Accounts }
    { -------- }
    AccountsNode := Doc.CreateElement('accounts');
    RootNode.AppendChild(AccountsNode); // insert child node in respective parent node

    { Get accounts }
    QueryAccounts := TSQLQuery.Create(nil);
    QueryAccounts.DataBase := DBConnection;
    QueryAccounts.Clear;
    QueryAccounts.SQL.Text := 'SELECT * FROM account';
    QueryAccounts.Open;
    { Loop over accounts }
    QueryAccounts.First;
    while not QueryAccounts.EOF do
    begin
      AccountNode := Doc.CreateElement('account');
      AccountsNode.AppendChild(AccountNode);

      ItemNode := Doc.CreateElement('account_no'); // create a child node
      TextNode := Doc.CreateTextNode(QueryAccounts.FieldByName('account_no').AsWideString); // insert a value to node
      ItemNode.Appendchild(TextNode); // save node
      AccountNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('name');
      TextNode := Doc.CreateTextNode(QueryAccounts.FieldByName('name').AsWideString);
      ItemNode.Appendchild(TextNode);
      AccountNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('creation_date');
      TextNode := Doc.CreateTextNode(QueryAccounts.FieldByName('creation_date').AsWideString);
      ItemNode.Appendchild(TextNode);
      AccountNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('first_name');
      TextNode := Doc.CreateTextNode(QueryAccounts.FieldByName('first_name').AsWideString);
      ItemNode.Appendchild(TextNode);
      AccountNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('last_name');
      TextNode := Doc.CreateTextNode(QueryAccounts.FieldByName('last_name').AsWideString);
      ItemNode.Appendchild(TextNode);
      AccountNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('birth_date');
      TextNode := Doc.CreateTextNode(QueryAccounts.FieldByName('birth_date').AsWideString);
      ItemNode.Appendchild(TextNode);
      AccountNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('gender');
      TextNode := Doc.CreateTextNode(QueryAccounts.FieldByName('gender').AsWideString);
      ItemNode.Appendchild(TextNode);
      AccountNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('height');
      TextNode := Doc.CreateTextNode(QueryAccounts.FieldByName('height').AsWideString);
      ItemNode.Appendchild(TextNode);
      AccountNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('weight');
      TextNode := Doc.CreateTextNode(QueryAccounts.FieldByName('weight').AsWideString);
      ItemNode.Appendchild(TextNode);
      AccountNode.AppendChild(ItemNode);

      { ----- }
      { Meals }
      { ----- }
      MealsNode := Doc.CreateElement('meals');
      AccountNode.AppendChild(MealsNode);

      { Get meals for this account }
      QueryMeals := TSQLQuery.Create(nil);
      QueryMeals.DataBase := DBConnection;
      QueryMeals.Clear;
      QueryMeals.SQL.Text := 'SELECT * FROM meal WHERE account_no = ' + QueryAccounts.FieldByName('account_no').AsString;
      QueryMeals.Open;
      QueryMeals.First;
      { Loop over meals }
      while not QueryMeals.EOF do
      begin
        { ---- }
        { Meal }
        { ---- }
        MealNode := Doc.CreateElement('meal');
        MealsNode.AppendChild(MealNode);

        ItemNode := Doc.CreateElement('meal_no'); // create a child node
        TextNode := Doc.CreateTextNode(QueryMeals.FieldByName('meal_no').AsWideString); // insert a value to node
        ItemNode.Appendchild(TextNode); // save node
        MealNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('account_no');
        TextNode := Doc.CreateTextNode(QueryMeals.FieldByName('account_no').AsWideString);
        ItemNode.AppendChild(TextNode);
        MealNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('meal_type_no');
        TextNode := Doc.CreateTextNode(QueryMeals.FieldByName('meal_type_no').AsWideString);
        ItemNode.AppendChild(TextNode);
        MealNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('creation_date');
        TextNode := Doc.CreateTextNode(QueryMeals.FieldByName('creation_date').AsWideString);
        ItemNode.AppendChild(TextNode);
        MealNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('creation_time');
        TextNode := Doc.CreateTextNode(QueryMeals.FieldByName('creation_time').AsWideString);
        ItemNode.AppendChild(TextNode);
        MealNode.AppendChild(ItemNode);

        { ---------- }
        { Meal items }
        { ---------- }
        MealItemsNode := Doc.CreateElement('meal_items');
        MealNode.AppendChild(MealItemsNode);

        { Get meal items }
        QueryMealItems := TSQLQuery.Create(nil);
        QueryMealItems.DataBase := DBConnection;
        QueryMealItems.Clear;
        QueryMealItems.SQL.Text:='SELECT * FROM meal_item WHERE meal_no = ' + QueryMeals.FieldByName('meal_no').AsString;
        QueryMealItems.Open;
        { Loop over meal items }
        QueryMealItems.First;
        while not QueryMealItems.EOF do
        begin
          { --------- }
          { Meal item }
          { --------- }
          MealItemNode := Doc.CreateElement('meal_item');
          MealItemsNode.AppendChild(MealItemNode);

          ItemNode := Doc.CreateElement('meal_item_no');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('meal_item_no').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('meal_no');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('meal_no').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('quantity');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('quantity').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('food_no');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('food_no').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('name');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('name').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('energy');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('energy').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('protein');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('protein').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('carbohydrate');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('carbohydrate').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('sugar');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('sugar').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('alcohol');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('alcohol').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('fat');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('fat').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('cholesterol');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('cholesterol').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('water');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('water').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('salt');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('salt').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('calcium');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('calcium').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('iron');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('iron').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('magnesium');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('magnesium').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('phosphorus');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('phosphorus').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('potassium');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('potassium').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('sodium');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('sodium').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('zinc');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('zinc').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('beta_carotene');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('beta_carotene').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_d');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_d').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_e');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_e').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_k1');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_k1').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_k2');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_k2').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_c');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_c').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_b1');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_b1').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_b2');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_b2').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_b3');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_b3').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_b5');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_b5').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_b6');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_b6').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_b9');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_b9').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('vitamin_b12');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('vitamin_b12').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('fibres');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('fibres').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('polyols');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('polyols').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('organic_acids');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('organic_acids').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('unit_name');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('unit_name').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          ItemNode := Doc.CreateElement('unit_quantity');
          TextNode := Doc.CreateTextNode(QueryMealItems.FieldByName('unit_quantity').AsWideString);
          ItemNode.AppendChild(TextNode);
          MealItemNode.AppendChild(ItemNode);

          QueryMealItems.Next;
        end;
        QueryMealItems.Close;
        FreeAndNil(QueryMealItems);
        QueryMeals.Next;
      end;
      QueryMeals.Close;
      FreeAndNil(QueryMeals);

      { -------- }
      { Workouts }
      { -------- }
      WorkoutsNode := Doc.CreateElement('workouts');
      AccountNode.AppendChild(WorkoutsNode);

      { Get workouts for this account }
      QueryWorkouts := TSQLQuery.Create(nil);
      QueryWorkouts.DataBase := DBConnection;
      QueryWorkouts.Clear;
      QueryWorkouts.SQL.text := 'SELECT * FROM workout WHERE account_no = ' + QueryAccounts.FieldByName('account_no').AsString;
      QueryWorkouts.Open;
      { Loop over workouts }
      QueryWorkouts.First;
      while not QueryWorkouts.EOF do
      begin
        { ------- }
        { Workout }
        { ------- }
        WorkoutNode := Doc.CreateElement('workout');
        WorkoutsNode.AppendChild(WorkoutNode);

        ItemNode := Doc.CreateElement('workout_no');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('workout_no').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('account_no');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('account_no').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('creation_date');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('creation_date').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('start_time');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('start_time').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('duration');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('duration').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('min_cf');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('min_cf').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('mean_cf');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('mean_cf').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('max_cf');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('max_cf').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('calories');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('calories').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        ItemNode := Doc.CreateElement('note');
        TextNode := Doc.CreateTextNode(QueryWorkouts.FieldByName('note').AsWideString);
        ItemNode.AppendChild(TextNode);
        WorkoutNode.AppendChild(ItemNode);

        QueryWorkouts.Next;
      end;
      QueryWorkouts.Close;
      FreeAndNil(QueryWorkouts);
      QueryAccounts.Next;
    end;
    FreeAndNil(QueryAccounts);

    { ------------------ }
    { User defined foods }
    { ------------------ }
    FoodsNode := Doc.CreateElement('foods');
    RootNode.AppendChild(FoodsNode);

    { Get user defined foods }
    QueryFoods := TSQLQuery.Create(nil);
    QueryFoods.DataBase := DBConnection;
    QueryFoods.Clear;
    QueryFoods.SQL.Text := 'SELECT * FROM food WHERE user_defined = 1';
    QueryFoods.Open;
    { Loop over foods }
    QueryFoods.First;
    while not QueryFoods.EOF do
    begin
      { ---- }
      { Food }
      { ---- }
      FoodNode := Doc.CreateElement('food');
      FoodsNode.AppendChild(FoodNode);

      ItemNode := Doc.CreateElement('food_no');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('food_no').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('name');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('name').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('food_category');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('food_category').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('energy');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('energy').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('protein');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('protein').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('carbohydrate');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('carbohydrate').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('sugar');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('sugar').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('alcohol');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('alcohol').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('fat');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('fat').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('cholesterol');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('cholesterol').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('water');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('water').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('salt');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('salt').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('calcium');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('calcium').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('iron');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('iron').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('magnesium');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('magnesium').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('phosphorus');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('phosphorus').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('potassium');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('potassium').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('sodium');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('sodium').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('zinc');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('zinc').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('beta_carotene');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('beta_carotene').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_d');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_d').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_e');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_e').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_k1');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_k1').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_k2');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_k2').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_c');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_c').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_b1');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_b1').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_b2');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_b2').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_b3');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_b3').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_b5');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_b5').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_b6');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_b6').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_b9');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_b9').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('vitamin_b12');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('vitamin_b12').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('fibres');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('fibres').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('polyols');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('polyols').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('organic_acids');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('organic_acids').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('user_defined');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('user_defined').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('food_unit_no');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('food_unit_no').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      ItemNode := Doc.CreateElement('unit_quantity');
      TextNode := Doc.CreateTextNode(QueryFoods.FieldByName('unit_quantity').AsWideString);
      ItemNode.AppendChild(TextNode);
      FoodNode.AppendChild(ItemNode);

      QueryFoods.Next;
    end;
    QueryFoods.Close;
    FreeAndNil(QueryFoods);

    writeXMLFile(Doc, SaveDialog_Export.FileName);
  end;
end;

procedure TForm_Main.MenuItem_File_ImportClick(Sender: TObject);
begin
  ShowMessage(SDlgNotImplemented);
end;

procedure TForm_Main.MenuItem_File_OpenAccountClick(Sender: TObject);
var
  i: integer;
begin
  if CurrentAccount = nil then
  begin
    CurrentAccount := TSSAccount.Create;
    CurrentMeasurement := TSSMeasurement.Create;
    for i := Low(CurrentPictures) to High(CurrentPictures) do
    begin
      CurrentPictures[i] := TSSPicture.Create;
    end;
  end;
  UpdateAccountList();

  Form_AccountList.ShowModal;

  if CurrentAccount.FAccountNo = 0 then
  begin
    // No account opened. Destroy account object.
    FreeAndNil(CurrentAccount);
    FreeAndNil(CurrentMeasurement);
    for i := Low(CurrentPictures) to High(CurrentPictures) do
    begin
      FreeAndNil(CurrentPictures[i]);
    end;
  end
  else
  begin
    CurrentMeasurement.AccountNo := CurrentAccount.FAccountNo;
    CurrentMeasurement.CreationDate := FormatDateTime('yyyy-mm-dd', Form_Main.Calendar1.DateTime);
  end;

  // Controls management
  if CurrentAccount = nil then
  begin
    MenuItem_File_CloseAccount.Enabled := False;
    MenuItem_Edit_Workouts.Enabled := False;
    MenuItem_Edit_Pictures.Enabled := False;
    ToolButton_Workouts.Enabled := False;
    ToolButton_Pictures.Enabled := False;
    Panel_Day_Summary.Enabled := False;
  end
  else
  begin
    MenuItem_File_CloseAccount.Enabled := True;
    MenuItem_Edit_Workouts.Enabled := True;
    MenuItem_Edit_Pictures.Enabled := True;
    ToolButton_Workouts.Enabled := True;
    ToolButton_Pictures.Enabled := True;
    Panel_Day_Summary.Enabled := True;
    Calendar1.DateTime := Now;
    UpdateAllLayouts();
  end;
end;

procedure TForm_Main.MenuItem_File_QuitClick(Sender: TObject);
var
  aAction: TCloseAction;
begin
  aAction := caFree;
  FormClose(Sender, aAction);
  Application.Terminate;
end;

procedure TForm_Main.MenuItem_Edit_FoodsClick(Sender: TObject);
begin
  Form_FoodList.ShowModal;
end;

procedure TForm_Main.MenuItem_Help_AboutClick(Sender: TObject);
begin
  Form_About.ShowModal;
end;

procedure TForm_Main.MenuItem_File_NewClick(Sender: TObject);
var
  newFile: TSSFile;
  Msg: String;
begin
  // Create a new database
  if SaveDialog_StaySharpFile.Execute then
  begin
    if FileExists(SaveDialog_StaySharpFile.FileName) then
    begin
      { Ask the user if he wants to overwrite file if it exists }
      Msg := '''' + SaveDialog_StaySharpFile.FileName + ''' ' + SDlgOverwriteFileMsg;
      case QuestionDlg(SDlgNewStaySharpFileTitle, Msg, mtCustom, [mrNo, SDlgBtnNo,
          'IsDefault', mrYes, SDlgBtnYes], '') of
        mrYes:
        begin
          DeleteFile(SaveDialog_StaySharpFile.FileName);
        end;
        mrNo:
        begin
          Exit;
        end;
        mrCancel:
        begin

        end;
      end;
    end;

    newFile:= TSSFile.Create (SaveDialog_StaySharpFile.FileName);
    newFile.CreateFile(LanguageLongCode, False);
    FreeAndNil(newFile);

    // Disconnect from a previous database
    if DBConnection.DatabaseName <> '' then
    begin
      DBConnection.Close();
      DBConnection.Connected:=False;
    end;

    // Connect to the new database
    DBConnection.DatabaseName := SaveDialog_StaySharpFile.FileName; // Then open the user file
    DBConnection.Connected := True;

    MenuItem_File_OpenAccount.Enabled := True;
    MenuItem_File_Backup.Enabled := True;
    ToolButton_OpenAccount.Enabled := True;
    MenuItem_Edit_Foods.Enabled := True;
    ToolButton_Foods.Enabled := True;
    MenuItem_Edit_Exercises.Enabled := True;
    ToolButton_Exercises.Enabled := True;
    Caption:= 'StaySharp - ' + SaveDialog_StaySharpFile.FileName;
  end;
end;

procedure TForm_Main.MenuItem_File_OpenClick(Sender: TObject);
var
  BackupFileName: string;
  TimeStamp: string;
  UserFile: TSSFile;
  NewFile: TSSFile;
  SDlgUpdateFileTitle: string;
  SDlgUpdateFileMsg: string;
begin
  SDlgUpdateFileTitle := 'Update file';
  SDlgUpdateFileMsg := 'Your file version is not compatible. Do you want to update it?. If yes, a backup file will be created';

  // Open an existing database
  if OpenDialog_StaySharpFile.Execute then
  begin
    // Disconnect from a previous database
    if DBConnection.DatabaseName <> '' then
    begin
      DBConnection.Close();
      DBConnection.Connected := False;
    end;

    // Connect to the new database
    {DBConnection.DatabaseName := OpenDialog_StaySharpFile.Filename;}

    // Check if schema version is compatible and ask the user if he wants
    // to update his version
    UserFile := TSSFile.Create(OpenDialog_StaySharpFile.Filename);
    if UserFile.GetSchemaVersion() = '1.0.0' then
    begin
      case QuestionDlg(SDlgUpdateFileTitle,
          SDlgUpdateFileMsg,
          mtCustom, [mrNo, SDlgBtnNo, 'IsDefault', mrYes, SDlgBtnYes], '') of
        mrYes:
        begin
          { Create a backup file }
          TimeStamp := FormatDateTime('-yyymmdd-hhmmss', Now) + '.ssf';
          BackupFileName := ExtractFileName(OpenDialog_StaySharpFile.Filename);
          BackupFileName := StringReplace(OpenDialog_StaySharpFile.Filename, '.ssf', TimeStamp, [rfIgnoreCase]);
          if CopyFile(OpenDialog_StaySharpFile.Filename, BackupFileName) = True then
          begin
            { Overwrite old file }
            NewFile := TSSFile.Create(OpenDialog_StaySharpFile.Filename);
            NewFile.CreateFile(LanguageLongCode, True);
            { Update new file with old file content }
            NewFile.UpdateStaySharpVersion1(BackupFileName);
            NewFile.Close();
            FreeAndNil(NewFile);
          end;
        end;
        mrNo:
        begin
          DBConnection.DatabaseName := '';
          Exit;
        end;
        mrCancel: ;
      end;
    end;
    UserFile.Close();
    FreeAndNil(UserFile);

    // Connect to the new database
    DBConnection.DatabaseName := OpenDialog_StaySharpFile.Filename;
    DBConnection.Connected := True;
    MRUMenuManager_MostRecentFiles.AddToRecent(OpenDialog_StaySharpFile.FileName);

    MenuItem_File_OpenAccount.Enabled := True;
    MenuItem_File_Export.Enabled := True;
    MenuItem_File_Import.Enabled := True;
    ToolButton_OpenAccount.Enabled := True;
    MenuItem_Edit_Foods.Enabled := True;
    ToolButton_Foods.Enabled := True;
    MenuItem_Edit_Exercises.Enabled := True;
    ToolButton_Exercises.Enabled := True;
    Caption:= 'StaySharp - ' + OpenDialog_StaySharpFile.Filename;

    if ApplicationSettings.ShowAccountsAtStartup = True then
      MenuItem_File_OpenAccountClick(Sender);
  end;
end;

procedure TForm_Main.MenuItem_Help_HelpClick(Sender: TObject);
var URL: String;
begin
  URL := 'file:///' + AppDataFolder + 'help/' + LanguageLongCode + '/staysharp_help.html';
  OpenURL(URL);
end;

procedure TForm_Main.MRUMenuManager_MostRecentFilesRecentFile(Sender: TObject;
  const AFileName: String);
begin
  if FileExists(AFileName) then
  begin
    // Close account if opened
    MenuItem_File_CloseAccountClick(Sender);

    // Disconnect from a previous database
    if DBConnection.DatabaseName <> '' then
    begin
      DBConnection.Close();
      DBConnection.Connected := False;
    end;

    // Connect to the new database
    DBConnection.DatabaseName := AFileName;
    DBConnection.Connected := True;
    MRUMenuManager_MostRecentFiles.AddToRecent(AFileName);

    // Set layout
    MenuItem_File_OpenAccount.Enabled := True;
    MenuItem_File_Backup.Enabled := True;
    MenuItem_File_Export.Enabled := True;
    MenuItem_File_Import.Enabled := True;
    ToolButton_OpenAccount.Enabled := True;
    MenuItem_Edit_Foods.Enabled := True;
    ToolButton_Foods.Enabled := True;
    MenuItem_Edit_Exercises.Enabled := True;
    ToolButton_Exercises.Enabled := True;
    Caption:= 'StaySharp - ' + AFileName;

    if ApplicationSettings.ShowAccountsAtStartup = True then
      MenuItem_File_OpenAccountClick(Sender);
  end;
end;

procedure TForm_Main.Splitter2Moved(Sender: TObject);
begin
  ApplicationSettings.ActivitiesPanelHeight := Panel_DailyActivities.Height;
end;

procedure TForm_Main.ToolButton_AddWorkoutClick(Sender: TObject);
begin
  // Initialize workout types
  Form_Workout.ComboBox_WorkoutType.ItemIndex := 0;
  // Show workout form
  Form_Workout.ShowModal;
end;

end.
