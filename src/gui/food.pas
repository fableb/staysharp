{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit food;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Spin, ExtCtrls, SSFood, SSFile;

type

  { TForm_Food }

  TForm_Food = class(TForm)
    Button_Compute: TButton;
    Button_Save: TButton;
    Button_Close: TButton;
    ComboBox_FoodCategories: TComboBox;
    Edit_FoodName: TEdit;
    FloatSpinEdit_VitB1: TFloatSpinEdit;
    FloatSpinEdit_VitB12: TFloatSpinEdit;
    FloatSpinEdit_VitB2: TFloatSpinEdit;
    FloatSpinEdit_VitB3: TFloatSpinEdit;
    FloatSpinEdit_VitB5: TFloatSpinEdit;
    FloatSpinEdit_VitB6: TFloatSpinEdit;
    FloatSpinEdit_VitB9: TFloatSpinEdit;
    FloatSpinEdit_VitD: TFloatSpinEdit;
    FloatSpinEdit_Iron: TFloatSpinEdit;
    FloatSpinEdit_Cholesterol: TFloatSpinEdit;
    FloatSpinEdit_Calcium: TFloatSpinEdit;
    FloatSpinEdit_Magnesium: TFloatSpinEdit;
    FloatSpinEdit_Phosphorus: TFloatSpinEdit;
    FloatSpinEdit_Potassium: TFloatSpinEdit;
    FloatSpinEdit_BetaCarotene: TFloatSpinEdit;
    FloatSpinEdit_Sodium: TFloatSpinEdit;
    FloatSpinEdit_Salt: TFloatSpinEdit;
    FloatSpinEdit_Fibres: TFloatSpinEdit;
    FloatSpinEdit_Fat: TFloatSpinEdit;
    FloatSpinEdit_Polyols: TFloatSpinEdit;
    FloatSpinEdit_OrganicAcids: TFloatSpinEdit;
    FloatSpinEdit_VitE: TFloatSpinEdit;
    FloatSpinEdit_VitK1: TFloatSpinEdit;
    FloatSpinEdit_VitK2: TFloatSpinEdit;
    FloatSpinEdit_VitC: TFloatSpinEdit;
    FloatSpinEdit_Zinc: TFloatSpinEdit;
    FloatSpinEdit_Sugar: TFloatSpinEdit;
    FloatSpinEdit_Protein: TFloatSpinEdit;
    FloatSpinEdit_Energy: TFloatSpinEdit;
    FloatSpinEdit_Carbohydrate: TFloatSpinEdit;
    FloatSpinEdit_Alcohol: TFloatSpinEdit;
    FloatSpinEdit_UnitQuantity: TFloatSpinEdit;
    GroupBox_MacroNutrients: TGroupBox;
    GroupBox_Minerals: TGroupBox;
    GroupBox_Vitamins: TGroupBox;
    Label_Alcohol: TLabel;
    Label_BetaCarotene: TLabel;
    Label_Calcium: TLabel;
    Label_Carbohydrates: TLabel;
    Label_Cholesterol: TLabel;
    Label_Fats: TLabel;
    Label_Fibres: TLabel;
    Label_FoodCategory: TLabel;
    Label_Energy: TLabel;
    Label_Iron: TLabel;
    Label_Magnesium: TLabel;
    Label_OrganicAcids: TLabel;
    Label_Phosphorus: TLabel;
    Label_Polyols: TLabel;
    Label_Potassium: TLabel;
    Label_Proteins: TLabel;
    Label_Salt: TLabel;
    Label_Sodium: TLabel;
    Label_Sugar: TLabel;
    Label_UnitQuantity: TLabel;
    Label_FoodName: TLabel;
    Label_VitB1: TLabel;
    Label_VitB12: TLabel;
    Label_VitB2: TLabel;
    Label_VitB3: TLabel;
    Label_VitB5: TLabel;
    Label_VitB6: TLabel;
    Label_VitB9: TLabel;
    Label_VitC: TLabel;
    Label_VitD: TLabel;
    Label_VitE: TLabel;
    Label_VitK1: TLabel;
    Label_VitK2: TLabel;
    Label_Zinc: TLabel;
    Panel_Nutrients: TPanel;
    ScrollBox_Nutrients: TScrollBox;
    procedure Button_CloseClick(Sender: TObject);
    procedure Button_ComputeClick(Sender: TObject);
    procedure Button_SaveClick(Sender: TObject);
    procedure ComboBox_FoodCategoriesChange(Sender: TObject);
    procedure Edit_FoodNameChange(Sender: TObject);
    procedure FloatSpinEdit_AlcoholChange(Sender: TObject);
    procedure FloatSpinEdit_BetaCaroteneChange(Sender: TObject);
    procedure FloatSpinEdit_CalciumChange(Sender: TObject);
    procedure FloatSpinEdit_CarbohydrateChange(Sender: TObject);
    procedure FloatSpinEdit_CholesterolChange(Sender: TObject);
    procedure FloatSpinEdit_EnergyChange(Sender: TObject);
    procedure FloatSpinEdit_FatChange(Sender: TObject);
    procedure FloatSpinEdit_FibresChange(Sender: TObject);
    procedure FloatSpinEdit_IronChange(Sender: TObject);
    procedure FloatSpinEdit_MagnesiumChange(Sender: TObject);
    procedure FloatSpinEdit_OrganicAcidsChange(Sender: TObject);
    procedure FloatSpinEdit_PhosphorusChange(Sender: TObject);
    procedure FloatSpinEdit_PolyolsChange(Sender: TObject);
    procedure FloatSpinEdit_PotassiumChange(Sender: TObject);
    procedure FloatSpinEdit_ProteinChange(Sender: TObject);
    procedure FloatSpinEdit_SaltChange(Sender: TObject);
    procedure FloatSpinEdit_SodiumChange(Sender: TObject);
    procedure FloatSpinEdit_SugarChange(Sender: TObject);
    procedure FloatSpinEdit_UnitQuantityChange(Sender: TObject);
    procedure FloatSpinEdit_VitB12Change(Sender: TObject);
    procedure FloatSpinEdit_VitB1Change(Sender: TObject);
    procedure FloatSpinEdit_VitB2Change(Sender: TObject);
    procedure FloatSpinEdit_VitB3Change(Sender: TObject);
    procedure FloatSpinEdit_VitB5Change(Sender: TObject);
    procedure FloatSpinEdit_VitB6Change(Sender: TObject);
    procedure FloatSpinEdit_VitB9Change(Sender: TObject);
    procedure FloatSpinEdit_VitCChange(Sender: TObject);
    procedure FloatSpinEdit_VitDChange(Sender: TObject);
    procedure FloatSpinEdit_VitEChange(Sender: TObject);
    procedure FloatSpinEdit_VitK1Change(Sender: TObject);
    procedure FloatSpinEdit_VitK2Change(Sender: TObject);
    procedure FloatSpinEdit_ZincChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UpdateControls();
    procedure InitValues();
  private
    procedure UpdateFoodLayout(const Food: TSSFood);

  public

  end;

var
  Form_Food: TForm_Food;

implementation

{$R *.lfm}

uses main, food_list;

{ TForm_Food }

procedure TForm_Food.UpdateControls();
begin
  if Trim(Edit_FoodName.Text) = '' then
    Button_Save.Enabled := False
  else
    Button_Save.Enabled := True;
end;

procedure TForm_Food.UpdateFoodLayout(const Food: TSSFood);
begin
  // Form title
  if Food.FFoodNo = -1 then
    Form_Food.Caption := SDlgNewFoodTitle
  else
    Form_Food.Caption := SDlgModifyFoodTitle;

  Edit_FoodName.Caption := Food.FName;
  ComboBox_FoodCategories.ItemIndex := Food.FFoodCategory;
  FloatSpinEdit_UnitQuantity.Value := Food.FUnitQuantity;
  FloatSpinEdit_Energy.Value := Food.FNutrients.Energy;

  // Macro-nutrients
  FloatSpinEdit_Protein.Value := Food.FNutrients.Protein;
  FloatSpinEdit_Carbohydrate.Value := Food.FNutrients.Carbohydrate;
  FloatSpinEdit_Sugar.Value := Food.FNutrients.Sugar;
  FloatSpinEdit_Alcohol.Value := Food.FNutrients.Alcohol;
  FloatSpinEdit_Fibres.Value := Food.FNutrients.Fibres;
  FloatSpinEdit_Polyols.Value := Food.FNutrients.Polyols;
  FloatSpinEdit_OrganicAcids.Value := Food.FNutrients.OrganicAcids;
  FloatSpinEdit_Fat.Value := Food.FNutrients.Fat;
  FloatSpinEdit_Cholesterol.Value := Food.FNutrients.Cholesterol;
  FloatSpinEdit_Salt.Value := Food.FNutrients.Salt;
  // Vitamins
  FloatSpinEdit_BetaCarotene.Value := Food.FNutrients.BetaCarotene;
  FloatSpinEdit_VitD.Value := Food.FNutrients.VitD;
  FloatSpinEdit_VitE.Value := Food.FNutrients.VitE;
  FloatSpinEdit_VitK1.Value := Food.FNutrients.VitK1;
  FloatSpinEdit_VitK2.Value := Food.FNutrients.VitK2;
  FloatSpinEdit_VitC.Value := Food.FNutrients.VitC;
  FloatSpinEdit_VitB1.Value := Food.FNutrients.VitB1;
  FloatSpinEdit_VitB2.Value := Food.FNutrients.VitB2;
  FloatSpinEdit_VitB3.Value := Food.FNutrients.VitB3;
  FloatSpinEdit_VitB5.Value := Food.FNutrients.VitB5;
  FloatSpinEdit_VitB6.Value := Food.FNutrients.VitB6;
  FloatSpinEdit_VitB9.Value := Food.FNutrients.VitB9;
  FloatSpinEdit_VitB12.Value := Food.FNutrients.VitB12;
  // Minerals
  FloatSpinEdit_Calcium.Value := Food.FNutrients.Calcium;
  FloatSpinEdit_Iron.Value := Food.FNutrients.Iron;
  FloatSpinEdit_Magnesium.Value := Food.FNutrients.Magnesium;
  FloatSpinEdit_Phosphorus.Value := Food.FNutrients.Phosphorus;
  FloatSpinEdit_Potassium.Value := Food.FNutrients.Potassium;
  FloatSpinEdit_Sodium.Value := Food.FNutrients.Sodium;
  FloatSpinEdit_Zinc.Value := Food.FNutrients.Zinc;
end;

procedure TForm_Food.InitValues();
begin
  ComboBox_FoodCategories.Clear;
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_None);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Starters);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Fruits);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Cereals);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Meat);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Milk);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Beverages);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Sugar);
  ComboBox_FoodCategories.Items.Add(DBFoodCategory_IceCream);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Fats);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Miscellaneous);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Baby);
  ComboBox_FoodCategories.ItemIndex := 0;
end;

procedure TForm_Food.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_Food.Button_ComputeClick(Sender: TObject);
var
  ComputedEnergy: double;
  Msg: string;
begin
  ComputedEnergy := Form_FoodList.CurrentFood.ComputeEnergy();
  Msg := SDlgFoodEnergyMsg1 + sLineBreak + FloatToStr(ComputedEnergy) +
    ' kcal.' + sLineBreak + SDlgFoodEnergyMsg2;

  case QuestionDlg(SDlgFoodEnergyTitle, Msg, mtCustom,
      [mrNo, SDlgBtnNo, 'IsDefault', mrYes, SDlgBtnYes], '') of
    mrYes:
    begin
      FloatSpinEdit_Energy.Caption := FloatToStr(ComputedEnergy);
    end;
    mrNo:
    begin

    end;
    mrCancel:
    begin

    end;
  end;
end;

procedure TForm_Food.Button_SaveClick(Sender: TObject);
begin
  Form_FoodList.CurrentFood.DBInsertReplace(Form_Main.DBConnection,
    Form_Main.SQLTransaction1);
  Close;
end;

procedure TForm_Food.ComboBox_FoodCategoriesChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FFoodCategory := ComboBox_FoodCategories.ItemIndex;
end;

procedure TForm_Food.Edit_FoodNameChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FName := Trim(Edit_FoodName.Text);
  UpdateControls();
end;

procedure TForm_Food.FloatSpinEdit_AlcoholChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Alcohol := FloatSpinEdit_Alcohol.Value;
end;

procedure TForm_Food.FloatSpinEdit_BetaCaroteneChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.BetaCarotene := FloatSpinEdit_BetaCarotene.Value;
end;

procedure TForm_Food.FloatSpinEdit_CalciumChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Calcium := FloatSpinEdit_Calcium.Value;
end;

procedure TForm_Food.FloatSpinEdit_CarbohydrateChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Carbohydrate := FloatSpinEdit_Carbohydrate.Value;
end;

procedure TForm_Food.FloatSpinEdit_CholesterolChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Cholesterol := FloatSpinEdit_Cholesterol.Value;
end;

procedure TForm_Food.FloatSpinEdit_EnergyChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Energy := FloatSpinEdit_Energy.Value;
end;

procedure TForm_Food.FloatSpinEdit_FatChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Fat := FloatSpinEdit_Fat.Value;
end;

procedure TForm_Food.FloatSpinEdit_FibresChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Fibres := FloatSpinEdit_Fibres.Value;
end;

procedure TForm_Food.FloatSpinEdit_IronChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Iron := FloatSpinEdit_Iron.Value;
end;

procedure TForm_Food.FloatSpinEdit_MagnesiumChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Magnesium := FloatSpinEdit_Magnesium.Value;
end;

procedure TForm_Food.FloatSpinEdit_OrganicAcidsChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.OrganicAcids := FloatSpinEdit_OrganicAcids.Value;
end;

procedure TForm_Food.FloatSpinEdit_PhosphorusChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Phosphorus := FloatSpinEdit_Phosphorus.Value;
end;

procedure TForm_Food.FloatSpinEdit_PolyolsChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Polyols := FloatSpinEdit_Polyols.Value;
end;

procedure TForm_Food.FloatSpinEdit_PotassiumChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Potassium := FloatSpinEdit_Potassium.Value;
end;

procedure TForm_Food.FloatSpinEdit_ProteinChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Protein := FloatSpinEdit_Protein.Value;
end;

procedure TForm_Food.FloatSpinEdit_SaltChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Salt := FloatSpinEdit_Salt.Value;
end;

procedure TForm_Food.FloatSpinEdit_SodiumChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Sodium := FloatSpinEdit_Sodium.Value;
end;

procedure TForm_Food.FloatSpinEdit_SugarChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Sugar := FloatSpinEdit_Sugar.Value;
end;

procedure TForm_Food.FloatSpinEdit_UnitQuantityChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FUnitQuantity := FloatSpinEdit_UnitQuantity.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitB12Change(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitB12 := FloatSpinEdit_VitB12.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitB1Change(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitB1 := FloatSpinEdit_VitB1.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitB2Change(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitB2 := FloatSpinEdit_VitB2.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitB3Change(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitB3 := FloatSpinEdit_VitB3.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitB5Change(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitB5 := FloatSpinEdit_VitB5.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitB6Change(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitB6 := FloatSpinEdit_VitB6.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitB9Change(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitB9 := FloatSpinEdit_VitB9.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitCChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitC := FloatSpinEdit_VitC.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitDChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitD := FloatSpinEdit_VitD.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitEChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitE := FloatSpinEdit_VitE.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitK1Change(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitK1 := FloatSpinEdit_VitK1.Value;
end;

procedure TForm_Food.FloatSpinEdit_VitK2Change(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.VitK2 := FloatSpinEdit_VitK2.Value;
end;

procedure TForm_Food.FloatSpinEdit_ZincChange(Sender: TObject);
begin
  Form_FoodList.CurrentFood.FNutrients.Zinc := FloatSpinEdit_Zinc.Value;
end;

procedure TForm_Food.FormShow(Sender: TObject);
begin
  InitValues();
  UpdateControls();
  UpdateFoodLayout(Form_FoodList.CurrentFood);
end;

end.
