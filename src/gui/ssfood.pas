{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit SSFood;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb;

type
  { Nutrient record }

  TNutrients = record
    Quantity: double;
    Energy: double;
    Protein: double;
    Carbohydrate: double;
    Sugar: double;
    Alcohol: double;
    Fibres: double;
    Polyols: double;
    OrganicAcids: double;
    Fat: double;
    Cholesterol: double;
    Water: double;
    Salt: double;
    Calcium: double;
    Iron: double;
    Magnesium: double;
    Phosphorus: double;
    Potassium: double;
    Sodium: double;
    Zinc: double;
    BetaCarotene: double;
    VitD: double;
    VitE: double;
    VitK1: double;
    VitK2: double;
    VitC: double;
    VitB1: double;
    VitB2: double;
    VitB3: double;
    VitB5: double;
    VitB6: double;
    VitB9: double;
    VitB12: double;
  end;

  { TSSFood }

  TSSFood = class
  private

  public
    FFoodNo: integer;
    FName: string;
    FFoodCategory: integer;
    FFoodCategoryName: string;
    FNutrients: TNutrients;
    FUserDefined: integer;
    FFoodUnitNo: integer;
    FUnitQuantity: double;
    procedure DBSelect(const DBConnection: TSQLConnection; const FoodNo: integer);
    procedure DBInsertReplace(const DBConnection: TSQLConnection;
      const DBTransaction: TSQLTransaction);
    procedure DBDelete(const DBConnection: TSQLConnection;
      const DBTransaction: TSQLTransaction; const FoodNo: integer);
    function ScaleNutrients(const factor: double = 1): TNutrients;
    function ComputeEnergy(): double;
  end;

implementation

const
  { 1g Proteins => 4 kcal
    1g Carbohydrates => 4 kcal
    1g Fats => 9 kcal
    1g Alcohol => 7 kcal}
  ProteinEnergy: double = 4;
  CarbohydratEnergy: double = 4;
  FatEnergy: double = 9;
  AlcoholEnergy: double = 7;

procedure TSSFood.DBSelect(const DBConnection: TSQLConnection;
  const FoodNo: integer);
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'SELECT * FROM food WHERE food_no = ' + IntToStr(FoodNo);
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := DBConnection;
    Query.Clear;
    Query.SQL.Text := SQL;
    Query.Open;

    { Populate fields }
    FFoodNo := Query.FieldByName('food_no').AsInteger;
    FName := Query.FieldByName('name').AsString;
    FFoodCategory := Query.FieldByName('food_category_no').AsInteger;

    // Macro-nutrients
    FNutrients.Energy := Query.FieldByName('energy').AsFloat;
    FNutrients.Protein := Query.FieldByName('protein').AsFloat;
    FNutrients.Carbohydrate := Query.FieldByName('carbohydrate').AsFloat;
    FNutrients.Sugar := Query.FieldByName('sugar').AsFloat;
    FNutrients.Alcohol := Query.FieldByName('alcohol').AsFloat;
    FNutrients.Fat := Query.FieldByName('fat').AsFloat;
    FNutrients.Cholesterol := Query.FieldByName('cholesterol').AsFloat;
    FNutrients.Water := Query.FieldByName('water').AsFloat;
    FNutrients.Salt := Query.FieldByName('salt').AsFloat;
    // Minerals
    FNutrients.Calcium := Query.FieldByName('calcium').AsFloat;
    FNutrients.Iron := Query.FieldByName('iron').AsFloat;
    FNutrients.Magnesium := Query.FieldByName('magnesium').AsFloat;
    FNutrients.Phosphorus := Query.FieldByName('phosphorus').AsFloat;
    FNutrients.Potassium := Query.FieldByName('potassium').AsFloat;
    FNutrients.Sodium := Query.FieldByName('sodium').AsFloat;
    FNutrients.Zinc := Query.FieldByName('zinc').AsFloat;
    // Vitamins
    FNutrients.BetaCarotene := Query.FieldByName('beta_carotene').AsFloat;
    FNutrients.VitD := Query.FieldByName('vitamin_d').AsFloat;
    FNutrients.VitE := Query.FieldByName('vitamin_e').AsFloat;
    FNutrients.VitK1 := Query.FieldByName('vitamin_k1').AsFloat;
    FNutrients.VitK2 := Query.FieldByName('vitamin_k2').AsFloat;
    FNutrients.VitC := Query.FieldByName('vitamin_c').AsFloat;
    FNutrients.VitB1 := Query.FieldByName('vitamin_b1').AsFloat;
    FNutrients.VitB2 := Query.FieldByName('vitamin_b2').AsFloat;
    FNutrients.VitB3 := Query.FieldByName('vitamin_b3').AsFloat;
    FNutrients.VitB5 := Query.FieldByName('vitamin_b5').AsFloat;
    FNutrients.VitB6 := Query.FieldByName('vitamin_b6').AsFloat;
    FNutrients.VitB9 := Query.FieldByName('vitamin_b9').AsFloat;
    FNutrients.VitB12 := Query.FieldByName('vitamin_b12').AsFloat;
    // Others
    FNutrients.Fibres := Query.FieldByName('fibres').AsFloat;
    FNutrients.Polyols := Query.FieldByName('polyols').AsFloat;
    FNutrients.OrganicAcids := Query.FieldByName('organic_acids').AsFloat;
    FUserDefined := Query.FieldByName('user_defined').AsInteger;
    FFoodUnitNo := Query.FieldByName('food_unit_no').AsInteger;
    FUnitQuantity := Query.FieldByName('unit_quantity').AsInteger;

    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFood.DBInsertReplace(const DBConnection: TSQLConnection;
  const DBTransaction: TSQLTransaction);
{ Insert or update a food in the database }
var
  Query: TSQLQuery;
  SQL: string;
  FoodNoStr: string;
  FloatFormatSettings: TFormatSettings;
begin
  FloatFormatSettings.DecimalSeparator := '.';

  if FFoodNo = -1 then
    FoodNoStr := 'NULL' // insert food
  else
    FoodNoStr := IntToStr(FFoodNo); // update food

  SQL := 'INSERT OR REPLACE INTO food (' +
    'food_no,name,food_category_no,energy,protein,carbohydrate,' +
    'sugar,alcohol,fat,cholesterol,water,salt,calcium,iron,magnesium,phosphorus,' +
    'potassium,sodium,zinc,beta_carotene,vitamin_d,vitamin_e,vitamin_k1,vitamin_k2,' +
    'vitamin_c,vitamin_b1,vitamin_b2,vitamin_b3,vitamin_b5,vitamin_b6,vitamin_b9,' +
    'vitamin_b12,fibres,polyols,organic_acids,user_defined,food_unit_no,' +
    'unit_quantity';
  SQL := SQL + ') VALUES(';
  SQL := SQL + FoodNoStr + ',';
  SQL := SQL + QuotedStr(FName) + ',';
  SQL := SQL + IntToStr(FFoodCategory) + ',';
  SQL := SQL + FloatToStr(FNutrients.Energy, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Protein, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Carbohydrate, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Sugar, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Alcohol, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Fat, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Cholesterol, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Water, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Salt, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Calcium, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Iron, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Magnesium, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Phosphorus, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Potassium, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Sodium, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Zinc, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.BetaCarotene, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitD, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitE, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitK1, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitK2, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitC, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitB1, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitB2, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitB3, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitB5, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitB6, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitB9, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.VitB12, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Fibres, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.Polyols, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FNutrients.OrganicAcids, FloatFormatSettings) + ',';
  SQL := SQL + IntToStr(FUserDefined) + ',';
  SQL := SQL + IntToStr(FFoodUnitNo) + ',';
  SQL := SQL + FloatToStr(FUnitQuantity, FloatFormatSettings);
  SQL := SQL + ')';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := DBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    DBTransaction.Commit; // Commit changes to database
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSFood.DBDelete(const DBConnection: TSQLConnection;
  const DBTransaction: TSQLTransaction; const FoodNo: integer);
{ Delete a food from the database }
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'DELETE FROM food WHERE food_no = ' + IntToStr(FoodNo);
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := DBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    DBTransaction.Commit; // Commit changes to database
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

function TSSFood.ScaleNutrients(const factor: double = 1): TNutrients;
  { Scale the nutrients according to the factor value }
begin
  Result.Energy := FNutrients.Energy * factor;
  Result.Protein := FNutrients.Energy * factor;
  Result.Carbohydrate := FNutrients.Energy * factor;
  Result.Sugar := FNutrients.Energy * factor;
  Result.Alcohol := FNutrients.Energy * factor;
  Result.Fibres := FNutrients.Energy * factor;
  Result.Polyols := FNutrients.Energy * factor;
  Result.OrganicAcids := FNutrients.Energy * factor;
  Result.Fat := FNutrients.Energy * factor;
  Result.Cholesterol := FNutrients.Energy * factor;
  Result.Water := FNutrients.Energy * factor;
  Result.Salt := FNutrients.Energy * factor;
  Result.Calcium := FNutrients.Energy * factor;
  Result.Iron := FNutrients.Energy * factor;
  Result.Magnesium := FNutrients.Energy * factor;
  Result.Phosphorus := FNutrients.Energy * factor;
  Result.Potassium := FNutrients.Energy * factor;
  Result.Sodium := FNutrients.Energy * factor;
  Result.Zinc := FNutrients.Energy * factor;
  Result.BetaCarotene := FNutrients.Energy * factor;
  Result.VitD := FNutrients.Energy * factor;
  Result.VitE := FNutrients.Energy * factor;
  Result.VitK1 := FNutrients.Energy * factor;
  Result.VitK2 := FNutrients.Energy * factor;
  Result.VitC := FNutrients.Energy * factor;
  Result.VitB1 := FNutrients.Energy * factor;
  Result.VitB2 := FNutrients.Energy * factor;
  Result.VitB3 := FNutrients.Energy * factor;
  Result.VitB5 := FNutrients.Energy * factor;
  Result.VitB6 := FNutrients.Energy * factor;
  Result.VitB9 := FNutrients.Energy * factor;
  Result.VitB12 := FNutrients.Energy * factor;
end;

function TSSFood.ComputeEnergy(): double;
  { Compute the food calories (energy) }
begin
  Result := FNutrients.Protein * ProteinEnergy + FNutrients.Carbohydrate *
    CarbohydratEnergy + FNutrients.Fat * FatEnergy + FNutrients.Alcohol * AlcoholEnergy;
end;

end.
