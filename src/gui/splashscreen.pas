{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit splashscreen;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TForm_SplashScreen }

  TForm_SplashScreen = class(TForm)
    Image_Logo: TImage;
    Label_ApplicationName: TLabel;
    Label_Author: TLabel;
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure Label_AuthorClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private

  public
    Completed: boolean;

  end;

var
  Form_SplashScreen: TForm_SplashScreen;

implementation

{$R *.lfm}

{ TForm_SplashScreen }

procedure TForm_SplashScreen.FormShow(Sender: TObject);
begin
  Label_Author.Caption := '(C) 2018, Fabrice LEBEL';
  OnShow := nil;
  Completed := False;
  Timer1.Interval := 3000;
  Timer1.Enabled := True;
end;

procedure TForm_SplashScreen.Label_AuthorClick(Sender: TObject);
begin

end;

procedure TForm_SplashScreen.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  Completed := True;
end;

end.
