{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit SSAccount;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DateUtils;

type
  { TSSAccount }

  TSSAccount = class
  public
    FAccountNo: longint;
    FName: string;
    FCreationDate: string;
    FFirstName: string;
    FLastName: string;
    FBirthDate: string;
    FGender: integer;
    FHeight: integer;
    FWeight: real;
    //Class Constructor Create;
    function GetGenderStr(): string;
    function ComputeAge(): integer;
    function ComputeBMR(): real;

  private
  end;

implementation

{ TSSAccount }

function TSSAccount.GetGenderStr(): string;
begin
  Result := 'None';
  if FGender = 1 then
    Result := 'Female'
  else
  if FGender = 2 then
    Result := 'Male';
end;

function TSSAccount.ComputeAge(): integer;
begin
  if FBirthDate <> '' then
    Result := YearsBetween(Now, StrToDate(FBirthDate, 'yyyy-mm-dd', '-'))
  else
    Result := 0;
end;

function TSSAccount.ComputeBMR(): real;
var
  s: real;
begin
  // Basal Metabolic Rate:
  // Use the Mifflin St Jeor equation (https://en.wikipedia.org/wiki/Basal_metabolic_rate)
  if FGender = 1 then
    s := -161
  else if FGender = 2 then
    s := 5
  else
  begin
    Result := 0.0;
    Exit;
  end;
  Result := 10.0 * FWeight + 6.25 * FHeight - 5.0 * ComputeAge() + s;
end;

end.
