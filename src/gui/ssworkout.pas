{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit SSWorkout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  { TSSWorkout }

  TSSWorkout = class
  public
    FWorkoutNo: longint;
    FAccountNo: longint;
    FWorkoutType: integer;
    FCreationDate: string;
    FStartTime: string;
    FDuration: string;
    FMinCF: integer;
    FMeanCF: integer;
    FMaxCF: integer;
    FCalories: integer;
    FFeeleingType: integer;
    FNote: string;
  end;

implementation

end.

