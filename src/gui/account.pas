{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit account;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DateTimePicker, Forms, Controls, Graphics,
  Dialogs, StdCtrls, Spin, SSAccount, SSFile;

type

  { TForm_Account }

  TForm_Account = class(TForm)
    Button_Cancel: TButton;
    Button_Save: TButton;
    ComboBox_Gender: TComboBox;
    DateTimePicker_BirthDate: TDateTimePicker;
    Edit_LastName: TEdit;
    Edit_FirstName: TEdit;
    Edit_AccountName: TEdit;
    FloatSpinEdit_Weight: TFloatSpinEdit;
    Label_Gender: TLabel;
    Label_Weight: TLabel;
    Label_Height: TLabel;
    Label_BirthDate: TLabel;
    Label_AccountName: TLabel;
    Label_FirstName: TLabel;
    Label_LastName: TLabel;
    SpinEdit_Height: TSpinEdit;
    procedure Button_CancelClick(Sender: TObject);
    procedure Button_SaveClick(Sender: TObject);
    procedure ComboBox_GenderChange(Sender: TObject);
    procedure Edit_AccountNameChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    { My custom stuff }
    procedure UpdateControls();

  private

  public

  end;

var
  Form_Account: TForm_Account;

implementation

{$R *.lfm}

uses main, account_list;

{ TForm_Account }

procedure TForm_Account.UpdateControls();
begin
  if (Trim(Edit_AccountName.Text) <> '') and (ComboBox_Gender.ItemIndex > 0) then
    Button_Save.Enabled := True
  else
    Button_Save.Enabled := False;
end;

procedure TForm_Account.Button_CancelClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_Account.Button_SaveClick(Sender: TObject);
var
  SQL: string;
  AccountNoStr: string;
  FloatFormatSettings: TFormatSettings;
begin
  FloatFormatSettings.DecimalSeparator := '.';
  // Check if we create a new account or update an existing one
  if Form_AccountList.CurrentAccount = nil then
    AccountNoStr := 'NULL' // insert record
  else
    AccountNoStr := IntToStr(Form_AccountList.CurrentAccount.FAccountNo); // update existing account

  // SQL query
  SQL := 'INSERT OR REPLACE INTO account (account_no,name,creation_date,first_name,last_name,birth_date,gender,height,weight) ';
  SQL := SQL + 'VALUES(';
  SQL := SQL + AccountNoStr + ',';
  SQL := SQL + QuotedStr(Trim(Edit_AccountName.Text)) + ',';
  SQL := SQL + 'date(' + QuotedStr(FormatDateTime('yyyy-mm-dd', Now)) + '),';
  SQL := SQL + QuotedStr(Trim(Edit_FirstName.Text)) + ',';
  SQL := SQL + QuotedStr(Trim(Edit_LastName.Text)) + ',';
  SQL := SQL + 'date(' + QuotedStr(FormatDateTime('yyyy-mm-dd', DateTimePicker_BirthDate.DateTime)) + '),';
  SQL := SQL + IntToStr(ComboBox_Gender.ItemIndex) + ',';
  SQL := SQL + FloatToStr(SpinEdit_Height.Value) + ',';
  SQL := SQL + FloatToStr(FloatSpinEdit_Weight.Value, FloatFormatSettings);
  SQL := SQL + ')';

  // Execute SQL query
  Form_Main.SQLQuery_Account.Close;
  Form_Main.SQLQuery_Account.SQL.Clear;
  Form_Main.SQLQuery_Account.SQL.Text := SQL;
  Form_Main.SQLQuery_Account.ExecSQL;
  Form_Main.SQLTransaction1.Commit;

  { Update an existing account info data }
  if (Form_Main.CurrentAccount <> nil) and (AccountNoStr <> 'NULL') then
  begin
    Form_Main.CurrentAccount.FName := Trim(Edit_AccountName.Text);
    Form_Main.CurrentAccount.FFirstName := Trim(Edit_FirstName.Text);
    Form_Main.CurrentAccount.FLastName := Trim(Edit_LastName.Text);
    Form_Main.CurrentAccount.FBirthDate := FormatDateTime('yyyy-mm-dd', DateTimePicker_BirthDate.DateTime);
    Form_Main.CurrentAccount.FGender := ComboBox_Gender.ItemIndex;
    Form_Main.CurrentAccount.FHeight := SpinEdit_Height.Value;
    Form_Main.CurrentAccount.FWeight := FloatSpinEdit_Weight.Value;
    Form_Main.UpdateAccountInfo();
  end;
  Close;
end;

procedure TForm_Account.ComboBox_GenderChange(Sender: TObject);
begin
  UpdateControls();
end;

procedure TForm_Account.Edit_AccountNameChange(Sender: TObject);
begin
  UpdateControls();
end;

procedure TForm_Account.FormCreate(Sender: TObject);
begin

end;

procedure TForm_Account.FormShow(Sender: TObject);
var
  GenderLabels: TStringList;
begin
  // Initialize Gender labels
  GenderLabels := TStringList.Create;
  GenderLabels.Add(SDBGender_None); // Id. 0
  GenderLabels.Add(SDBGender_Female); // Id. 1
  GenderLabels.Add(SDBGender_Male); // Id. 2
  ComboBox_Gender.Items := GenderLabels;
  ComboBox_Gender.ItemIndex := 0;
  GenderLabels.Free;

  if Form_AccountList.CurrentAccount <> nil then
  begin
    // Load existing account
    Caption := SDlgModifyAccountTitle;
    Edit_AccountName.Text := Form_AccountList.CurrentAccount.FName;
    Edit_FirstName.Text := Form_AccountList.CurrentAccount.FFirstName;
    Edit_LastName.Text := Form_AccountList.CurrentAccount.FLastName;
    DateTimePicker_BirthDate.DateTime :=
      StrToDate(Form_AccountList.CurrentAccount.FBirthDate, 'yyyy-mm-dd', '-');
    ComboBox_Gender.ItemIndex := Form_AccountList.CurrentAccount.FGender;
    SpinEdit_Height.Value := Form_AccountList.CurrentAccount.FHeight;
    FloatSpinEdit_Weight.Value := Form_AccountList.CurrentAccount.FWeight;
  end
  else
  begin
    // Init fields for new account
    Caption := SDlgNewAccountTitle;
    Edit_AccountName.Text := '';
    Edit_FirstName.Text := '';
    Edit_LastName.Text := '';
    DateTimePicker_BirthDate.DateTime := Now;
    SpinEdit_Height.Value := 0;
    FloatSpinEdit_Weight.Value := 0.0;
  end;
  UpdateControls();
end;

end.



