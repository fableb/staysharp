{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit food_from_meal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, DBCtrls, SSFood, SSFile;

resourcestring
  SDlgFoodFromMealTitle = 'Food from meal';
  SDlgFoodFromMealErrorMsg = 'Meal items total quantity is equal to zero. We cannot create a new food from them.';

type

  { TForm_FoodFromMeal }

  TForm_FoodFromMeal = class(TForm)
    Button_Save: TButton;
    Button_Cancel: TButton;
    ComboBox_FoodCategories: TComboBox;
    Edit_FoodName: TEdit;
    Label_FoodName: TLabel;
    Label_Hint: TLabel;
    SQLQuery_Foods: TSQLQuery;
    procedure Button_CancelClick(Sender: TObject);
    procedure Button_SaveClick(Sender: TObject);
    procedure ComboBox_FoodCategoriesChange(Sender: TObject);
    procedure Edit_FoodNameChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure UpdateControls();

  public

  end;

var
  Form_FoodFromMeal: TForm_FoodFromMeal;

implementation

{$R *.lfm}

uses main, meal;

{ TForm_FoodFromMeal }

procedure TForm_FoodFromMeal.UpdateControls();
begin
  if (Trim(Edit_FoodName.Text) = '') or (ComboBox_FoodCategories.ItemIndex = 0) then
  begin
    Button_Save.Enabled := False;
  end
  else
  begin
    Button_Save.Enabled := True;
  end;
end;

procedure TForm_FoodFromMeal.Button_CancelClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_FoodFromMeal.Button_SaveClick(Sender: TObject);
var
  Nutrients: TNutrients;
  Factor: double;
  SQL: string;
  FloatFormatSettings: TFormatSettings;
begin
  FloatFormatSettings.DecimalSeparator := '.';

  { 3. Compute total nutrients }
  Nutrients := Form_Main.ComputeMealNutrients(Form_Meal.SQLQuery_MealItems);
  { 4. Insert the food in the food database with the user food flag = 1
    x grams ==> y nutrient
    w grams ==> y * (w/x) nutrient }
  if Nutrients.Quantity > 0 then
  begin
    Factor := 100.0 / Nutrients.Quantity;

    SQL := 'INSERT OR REPLACE INTO food (';
    SQL := SQL + 'food_no,name,food_category_no,';
    SQL := SQL + 'energy,protein,carbohydrate,sugar,alcohol,fat,cholesterol,water,';
    SQL := SQL + 'salt,calcium,iron,magnesium,phosphorus,potassium,sodium,';
    SQL := SQL + 'zinc,beta_carotene,vitamin_d,vitamin_e,vitamin_k1,vitamin_k2,';
    SQL := SQL + 'vitamin_c,vitamin_b1,vitamin_b2,vitamin_b3,vitamin_b5,';
    SQL := SQL + 'vitamin_b6,vitamin_b9,vitamin_b12,fibres,polyols,organic_acids,';
    SQL := SQL + 'user_defined,food_unit_no,unit_quantity';
    SQL := SQL + ') ';
    SQL := SQL + 'VALUES(';
    SQL := SQL + 'NULL,';
    SQL := SQL + QuotedStr(Trim(Edit_FoodName.Text)) + ',';
    SQL := SQL + IntToStr(ComboBox_FoodCategories.ItemIndex) + ',';
    SQL := SQL + FloatToStr(Nutrients.Energy * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Protein * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Carbohydrate * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Sugar * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Alcohol * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Fat * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Cholesterol * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Water * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Salt * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Calcium * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Iron * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Magnesium * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Phosphorus * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Potassium * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Sodium * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Zinc * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.BetaCarotene * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitD * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitE * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitK1 * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitK2 * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitC * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitB1 * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitB2 * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitB3 * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitB5 * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitB6 * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitB9 * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.VitB12 * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Fibres * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.Polyols * Factor, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(Nutrients.OrganicAcids * Factor, FloatFormatSettings) + ',';
    SQL := SQL + '1' + ','; // user_defined field: 1
    SQL := SQL + '1' + ','; // unit: Grams
    SQL := SQL + FloatToStr(100, FloatFormatSettings); // unit quantity : 100g
    SQL := SQL + ')';
    // Execute SQL query
    SQLQuery_Foods.SQL.Clear;
    SQLQuery_Foods.SQL.Text := SQL;
    SQLQuery_Foods.ExecSQL;
    SQLQuery_Foods.Close;
  end
  else
  begin
    { Inform the user that the quantity is zero }
    QuestionDlg(SDlgFoodFromMealTitle,
      SDlgFoodFromMealErrorMsg,
      mtCustom, [mrYes, SDlgBtnYes], '');
    //Exit;
  end;
  Close;
end;

procedure TForm_FoodFromMeal.ComboBox_FoodCategoriesChange(Sender: TObject);
begin
  UpdateControls();
end;

procedure TForm_FoodFromMeal.Edit_FoodNameChange(Sender: TObject);
begin
  UpdateControls();
end;

procedure TForm_FoodFromMeal.FormShow(Sender: TObject);
begin
  ComboBox_FoodCategories.Clear;
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_None);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Starters);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Fruits);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Cereals);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Meat);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Milk);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Beverages);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Sugar);
  ComboBox_FoodCategories.Items.Add(DBFoodCategory_IceCream);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Fats);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Miscellaneous);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Baby);
  ComboBox_FoodCategories.ItemIndex := 0;

  Edit_FoodName.Text := '';

  UpdateControls();
end;

end.

