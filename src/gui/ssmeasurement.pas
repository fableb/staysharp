{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit SSMeasurement;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb;

type

  { TSSMeasurement }

  TSSMeasurement = class
  private
    FMeasurementNo: integer;
    FAccountNo: integer;
    FCreationDate: string;
    FWeight: double;
    FBodyMassIndex: double;
    FSleep: double;
    FRestHeartRate: integer;
    FBloodPressureSystolic: integer;
    FBloodPressureDiastolic: integer;
    FNeck: double;
    FShoulders: double;
    FChest: double;
    FArmLeft: double;
    FArmRignt: double;
    FForearmLeft: double;
    FForearmRight: double;
    FMidSection: double;
    FHips: double;
    FThighLeft: double;
    FThighRight: double;
    FCalfLeft: double;
    FCalfRight: double;

  public
    constructor Create;
    property MeasurementNo: integer read FMeasurementNo write FMeasurementNo;
    property AccountNo: integer read FAccountNo write FAccountNo;
    property CreationDate: string read FCreationDate write FCreationDate;
    property Weight: double read FWeight write FWeight;
    property BodyMassIndex: double read FBodyMassIndex write FBodyMassIndex;
    property Sleep: double read FSleep write FSleep;
    property RestHeartRate: integer read FRestHeartRate write FRestHeartRate;
    property BloodPressureSystolic: integer read FBloodPressureSystolic write FBloodPressureSystolic;
    property BloodPressureDiastolic: integer read FBloodPressureDiastolic write FBloodPressureDiastolic;
    property Neck: double read FNeck write FNeck;
    property Shoulders: double read FShoulders write FShoulders;
    property Chest: double read FChest write FChest;
    property ArmLeft: double read FArmLeft write FArmLeft;
    property ArmRignt: double read FArmRignt write FArmRignt;
    property ForearmLeft: double read FForearmLeft write FForearmLeft;
    property ForearmRight: double read FForearmRight write FForearmRight;
    property MidSection: double read FMidSection write FMidSection;
    property Hips: double read FHips write FHips;
    property ThighLeft: double read FThighLeft write FThighLeft;
    property ThighRight: double read FThighRight write FThighRight;
    property CalfLeft: double read FCalfLeft write FCalfLeft;
    property CalfRight: double read FCalfRight write FCalfRight;
    procedure DBFind(const _DBConnection: TSQLConnection; _AccountNo: integer; _CreationDate: String);
    procedure DBInsertReplace(const DBConnection: TSQLConnection; const DBTransaction: TSQLTransaction);
    procedure DBDelete(const DBConnection: TSQLConnection; const DBTransaction: TSQLTransaction);
  end;

implementation

constructor TSSMeasurement.Create;
begin
  inherited Create;
  FMeasurementNo := -1;
  FAccountNo := -1;
end;

procedure TSSMeasurement.DBFind(const _DBConnection: TSQLConnection; _AccountNo: integer; _CreationDate: String);
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'SELECT * FROM measurement WHERE ';
  SQL := SQL + 'account_no = ' + IntToStr(_AccountNo) + ' AND ';
  SQL := SQL + 'creation_date = ' + 'date(''' + _CreationDate + ''')';
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := _DBConnection;
    Query.Clear;
    Query.SQL.Text := SQL;
    Query.Open;

    if Query.RecordCount > 0 then
    begin
      { Record found }
      MeasurementNo := Query.FieldByName('measurement_no').AsInteger;
      AccountNo := Query.FieldByName('account_no').AsInteger;
      CreationDate := Query.FieldByName('creation_date').AsString;
      Weight := Query.FieldByName('weight').AsFloat;
      BodyMassIndex := Query.FieldByName('body_mass_index').AsFloat;
      Sleep := Query.FieldByName('sleep').AsFloat;
      RestHeartRate := Query.FieldByName('rest_heart_rate').AsInteger;
      BloodPressureSystolic := Query.FieldByName('blood_pressure_systolic').AsInteger;
      BloodPressureDiastolic := Query.FieldByName('blood_pressure_diastolic').AsInteger;
      Neck := Query.FieldByName('neck').AsFloat;
      Shoulders := Query.FieldByName('shoulders').AsFloat;
      Chest := Query.FieldByName('chest').AsFloat;
      ArmLeft := Query.FieldByName('arm_left').AsFloat;
      ArmRignt := Query.FieldByName('arm_right').AsFloat;
      ForearmLeft := Query.FieldByName('forearm_left').AsFloat;
      ForearmRight := Query.FieldByName('forearm_right').AsFloat;
      MidSection := Query.FieldByName('mid_section').AsFloat;
      Hips := Query.FieldByName('hips').AsFloat;
      ThighLeft := Query.FieldByName('thigh_left').AsFloat;
      ThighRight := Query.FieldByName('thigh_right').AsFloat;
      CalfLeft := Query.FieldByName('calf_left').AsFloat;
      CalfRight := Query.FieldByName('calf_right').AsFloat;
    end
    else
    begin
      MeasurementNo := -1;
      AccountNo := _AccountNo;
      CreationDate := _CreationDate;
      Weight := 0.0;
      BodyMassIndex := 0.0;
      Sleep := 0.0;
      RestHeartRate := 0;
      BloodPressureSystolic := 0;
      BloodPressureDiastolic := 0;
      Neck := 0.0;
      Shoulders := 0.0;
      Chest := 0.0;
      ArmLeft := 0.0;
      ArmRignt := 0.0;
      ForearmLeft := 0.0;
      ForearmRight := 0.0;
      MidSection := 0.0;
      Hips := 0.0;
      ThighLeft := 0.0;
      ThighRight := 0.0;
      CalfLeft := 0.0;
      CalfRight := 0.0;
    end;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSMeasurement.DBInsertReplace(const DBConnection: TSQLConnection;
  const DBTransaction: TSQLTransaction);
{ Insert or update a measurement in the database }
var
  Query: TSQLQuery;
  SQL: string;
  MeasurementNoStr: string;
  FloatFormatSettings: TFormatSettings;
begin
  FloatFormatSettings.DecimalSeparator := '.';

  if FMeasurementNo = -1 then
    MeasurementNoStr := 'NULL' // insert food
  else
    MeasurementNoStr := IntToStr(FMeasurementNo); // update food

  SQL := 'INSERT OR REPLACE INTO measurement (' +
    'measurement_no,account_no,creation_date,weight,body_mass_index,sleep,rest_heart_rate,';
  SQL := SQL + 'blood_pressure_systolic,blood_pressure_diastolic,neck,shoulders,chest,';
  SQL := SQL + 'arm_left,arm_right,forearm_left,forearm_right,mid_section,hips,';
  SQL := SQL + 'thigh_left,thigh_right,calf_left,calf_right';
  SQL := SQL + ') VALUES(';
  SQL := SQL + MeasurementNoStr + ',';
  SQL := SQL + IntToStr(FAccountNo) + ',';
  SQL := SQL + 'date(''' + FCreationDate + '''),';
  SQL := SQL + FloatToStr(FWeight, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FBodyMassIndex, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FSleep, FloatFormatSettings) + ',';
  SQL := SQL + IntToStr(FRestHeartRate) + ',';
  SQL := SQL + IntToStr(FBloodPressureSystolic) + ',';
  SQL := SQL + IntToStr(FBloodPressureDiastolic) + ',';
  SQL := SQL + FloatToStr(FNeck, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FShoulders, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FChest, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FArmLeft, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FArmRignt, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FForearmLeft, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FForearmRight, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FMidSection, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FHips, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FThighLeft, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FThighRight, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FCalfLeft, FloatFormatSettings) + ',';
  SQL := SQL + FloatToStr(FCalfRight, FloatFormatSettings);
  SQL := SQL + ')';

  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := DBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    DBTransaction.Commit; // Commit changes to database
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSMeasurement.DBDelete(const DBConnection: TSQLConnection; const DBTransaction: TSQLTransaction);
{ Delete a measurement from the database }
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'DELETE FROM measurement WHERE measurement_no = ' + IntToStr(FMeasurementNo);
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := DBConnection;
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.ExecSQL;
    DBTransaction.Commit; // Commit changes to database
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

end.
