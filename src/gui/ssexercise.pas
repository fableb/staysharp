unit SSExercise;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, DB;

resourcestring
  { Exercise type }
  DBExerciceTypeName_Other = 'Other'; // 1
  DBExerciceTypeName_Cardio = 'Cardio'; // 2
  DBExerciceTypeName_Strength = 'Strength'; // 3
  DBExerciceTypeName_Stretching = 'Stretching'; // 4

  { Image type }
  DBPictureTypeNo_JPEG = 'jpg'; // 1
  DBPictureTypeNo_PNG = 'png'; // 2

type

  { TSSExercise }
  TSSExercise = class
  private
    FExerciseNo: integer;
    FExerciseTypeNo: integer;
    FExerciseTypeName: string;
    FName: string;
    FPicture1Blob: TMemoryStream;
    FPicture1TypeNo: integer;
    FPicture2Blob: TMemoryStream;
    FPicture2TypeNo: integer;
    FUserDefined: integer;
    function GetExerciseTypeName(): string;
    function GetPictureTypeName(): string;

  public
    constructor Create;
    property ExerciseNo: integer read FExerciseNo write FExerciseNo;
    property ExerciseTypeNo: integer read FExerciseTypeNo write FExerciseTypeNo;
    property ExerciseTypeName: string read GetExerciseTypeName;
    property Name: string read FName write FName;
    property Picture1BLOB: TMemoryStream read FPicture1Blob write FPicture1Blob;
    property Picture1TypeNo: integer read FPicture1TypeNo write FPicture1TypeNo;
    property Picture2BLOB: TMemoryStream read FPicture2Blob write FPicture2Blob;
    property Picture2TypeNo: integer read FPicture2TypeNo write FPicture2TypeNo;
    property UserDefined: integer read FUserDefined write FUserDefined;
    property PictureTypeName: string read GetPictureTypeName;
    procedure DBFind(const _DBConnection: TSQLConnection; _ExerciseNo: integer);
    procedure DBInsertReplace(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction);
    procedure DBDelete(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction);
    procedure DBDeletePicture(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction; PictureId: integer);

  end;

implementation

constructor TSSExercise.Create;
begin
  inherited Create;
  FExerciseNo := -1;
  FExerciseTypeNo := -1;
  FExerciseTypeName := '';
  FName := '';
  FPicture1Blob := TMemoryStream.Create;
  FPicture1Blob.Position := 0;
  FPicture2Blob := TMemoryStream.Create;
  FPicture2Blob.Position := 0;
  FUserDefined := 1;
end;


function TSSExercise.GetPictureTypeName(): string;
begin
  case FExerciseTypeNo of
    1: Result := DBPictureTypeNo_JPEG;
    2: Result := DBPictureTypeNo_PNG;
    else
      Result := '';
  end;
end;

function TSSExercise.GetExerciseTypeName(): string;
begin
  case FExerciseTypeNo of
    1: Result := DBExerciceTypeName_Other;
    2: Result := DBExerciceTypeName_Cardio;
    3: Result := DBExerciceTypeName_Strength;
    4: Result := DBExerciceTypeName_Stretching;
    else
      Result := '';
  end;
end;

procedure TSSExercise.DBFind(const _DBConnection: TSQLConnection; _ExerciseNo: integer);
var
  SQL: string;
  Query: TSQLQuery;
begin
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := _DBConnection;
    Query.Clear;

    SQL := 'SELECT * FROM exercise WHERE exercise_no = :ExerciseNo';
    Query.SQL.Text := SQL;
    Query.Params.ParamByName('ExerciseNo').AsInteger := _ExerciseNo;
    Query.Open;

    if Query.RecordCount > 0 then
    begin
      Query.First;
      FExerciseNo := Query.FieldByName('exercise_no').AsInteger;
      FExerciseTypeNo := Query.FieldByName('exercise_type_no').AsInteger;
      FExerciseTypeName := Query.FieldByName('exercise_type_name').AsString;
      FName := Query.FieldByName('name').AsString;
      FPicture1Blob.LoadFromStream(Query.CreateBlobStream(Query.FieldByName('picture1_blob'), bmRead));
      FPicture1TypeNo := Query.FieldByName('picture1_ext_no').AsInteger;
      FPicture2Blob.LoadFromStream(Query.CreateBlobStream(Query.FieldByName('picture2_blob'), bmRead));
      FPicture2TypeNo := Query.FieldByName('picture2_ext_no').AsInteger;
    end;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;

end;

procedure TSSExercise.DBInsertReplace(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction);
var
  SQL: string;
  Query: TSQLQuery;
begin
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := _DBConnection;
    Query.SQL.Clear;

    SQL := 'INSERT OR REPLACE INTO exercise(exercise_no,exercise_type_no,exercise_type_name,name,picture1_blob,picture1_ext_no,picture2_blob,picture2_ext_no,user_defined) ';
    SQL := SQL + 'VALUES(:ExerciseNo,:ExerciseTypeNo,:ExerciseTypeName,:Name,:Picture1Blob,:Picture1ExtNo,:Picture2Blob,:Picture2ExtNo,:UserDefined)';
    Query.SQL.Text := SQL;
    if FExerciseNo = -1 then
      Query.Params.ParamByName('ExerciseNo').Value := NULL
    else
      Query.Params.ParamByName('ExerciseNo').AsInteger := FExerciseNo;
    Query.Params.ParamByName('ExerciseTypeNo').AsInteger := FExerciseTypeNo;
    Query.Params.ParamByName('ExerciseTypeName').AsString := GetExerciseTypeName();
    Query.Params.ParamByName('Name').AsString := FName;
    Query.Params.ParamByName('Picture1Blob').LoadFromStream(FPicture1Blob, ftBlob);
    Query.Params.ParamByName('Picture1ExtNo').AsInteger := FPicture1TypeNo;
    Query.Params.ParamByName('Picture2Blob').LoadFromStream(FPicture2Blob, ftBlob);
    Query.Params.ParamByName('Picture2ExtNo').AsInteger := FPicture2TypeNo;
    Query.Params.ParamByName('UserDefined').AsInteger := FUserDefined;
    Query.ExecSQL;
    _DBTransaction.Commit; // Commit changes to database
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSExercise.DBDelete(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction);
begin

end;

procedure TSSExercise.DBDeletePicture(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction; PictureId: integer);
begin

end;

end.
