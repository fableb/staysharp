program staysharp;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, datetimectrls, tachartlazaruspkg, memdslaz, main, workout, about,
  account_list, account, SSAccount, SSWorkout, workout_list, food_list, meal,
  SSMeal, SSMealItem, food_from_meal, SSFood, food, SSFile, splashscreen,
  SSApplicationSettings, gettext, DefaultTranslator, LCLTranslator,
  measurements, ssmeasurement, sspicture, backup, picture_list, exercise_list,
  exercise, ssexercise;

{$R *.res}
{$IFDEF Win32}
  {$R project.rc}
{$ENDIF}

begin
  Application.Scaled:=True;
  RequireDerivedFormResource:=True;
  Application.Initialize;

  { ------------------------------------------------------------------ }
  { Language settings here because some controls does not set language }
  { in the main.pas file.                                              }
  {                                                                    }
  { Get operating system language                                      }
  { ------------------------------------------------------------------ }
  GetLanguageIDs(LanguageLongCode, LanguageShortCode); // defined in gettext
  {$IFDEF UNIX}
  Writeln('[StaySharp Info] 0. System language: ' + LanguageLongCode + '/' + LanguageShortCode);
  {$ENDIF}

  LanguageLongCode := Copy(LowerCase(LanguageLongCode), 1,5 );
  LanguageShortCode := LowerCase(LanguageShortCode);
  {$IFDEF UNIX}
  Writeln('[StaySharp Info] 1. System language: ' + LanguageLongCode + '/' + LanguageShortCode);
  {$ENDIF}

  { -------------------------------- }
  { Command line language management }
  { -------------------------------- }
  if Application.HasOption('l', '') then
  begin
    LanguageShortCode := Copy(LowerCase(Application.GetOptionValue('l', '')), 1, 2);
    LanguageLongCode := LowerCase(Application.GetOptionValue('l', ''));
  end;
  {$IFDEF UNIX}
  Writeln('[StaySharp Info] 2. Command line language option: ' + LanguageLongCode + '/' + LanguageShortCode);
  {$ENDIF}

  { -------------------------------- }
  { Global language codes management }
  { -------------------------------- }
  case LanguageShortCode of
    'fr':
      begin
        LanguageLongCode := 'fr_fr';
      end;
    'en':
      begin
        LanguageLongCode := 'en_en';
      end;
    else
      begin
        LanguageShortCode := 'en';
        LanguageLongCode := 'en_en';
      end;
  end;
  {$IFDEF UNIX}
  Writeln('[StaySharp Info] 3. Final language choice: ' + LanguageLongCode + '/' + LanguageShortCode);
  {$ENDIF}

  { Set default language }
  SetDefaultLang(LanguageLongCode);

  Form_SplashScreen := TForm_SplashScreen.Create(nil);
  Form_SplashScreen.Show;
  Form_SplashScreen.Update;

  Application.CreateForm(TForm_Main, Form_Main);
  Application.CreateForm(TForm_Workout, Form_Workout);
  Application.CreateForm(TForm_About, Form_About);
  Application.CreateForm(TForm_AccountList, Form_AccountList);
  Application.CreateForm(TForm_Account, Form_Account);
  Application.CreateForm(TForm_WorkoutList, Form_WorkoutList);
  Application.CreateForm(TForm_FoodList, Form_FoodList);
  Application.CreateForm(TForm_Meal, Form_Meal);
  Application.CreateForm(TForm_FoodFromMeal, Form_FoodFromMeal);
  Application.CreateForm(TForm_Food, Form_Food);

  while not Form_SplashScreen.Completed do
    Application.ProcessMessages;
  Form_SplashScreen.Hide;
  Form_SplashScreen.Free;
  Application.CreateForm(TForm_Measurements, Form_Measurements);
  Application.CreateForm(TForm_Backup, Form_Backup);
  Application.CreateForm(TForm_PictureList, Form_PictureList);
  Application.CreateForm(TForm_ExerciseList, Form_ExerciseList);
  Application.CreateForm(TForm_Exercise, Form_Exercise);
  Application.Run;
end.

