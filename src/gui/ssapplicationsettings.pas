unit SSApplicationSettings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IniFiles;

type

  { SSApplicationSettings }

  { TSSApplicationSettings }

  TSSApplicationSettings = class
  private
    FConfigurationFile: string;
    FConfigurationPath: string;
    FIsMaximized: boolean;
    FPositionHeight: integer;
    FPositionLeft: integer;
    FPositionTop: integer;
    FPositionWidth: integer;
    FActivitiesPanelHeight: integer;
    FShowAccountsAtStartup: boolean;
    FBackupDirectory: string;
  public
    constructor Create;
    property ConfigurationPath: string read FConfigurationPath;
    property ConfigurationFile: string read FConfigurationFile;
    property IsMaximized: boolean read FIsMaximized write FIsMaximized;
    property PositionTop: integer read FPositionTop write FPositionTop;
    property PositionLeft: integer read FPositionLeft write FPositionLeft;
    property PositionHeight: integer read FPositionHeight write FPositionHeight;
    property PositionWidth: integer read FPositionWidth write FPositionWidth;
    property ActivitiesPanelHeight: integer read FActivitiesPanelHeight write FActivitiesPanelHeight;
    property ShowAccountsAtStartup: boolean read FShowAccountsAtStartup write FShowAccountsAtStartup; // Show account list at startup
    property Backupdirectory: string read FBackupDirectory write FBackupDirectory; // Backup directory
    procedure SaveConfigurationFile();

  end;

implementation

const
  C_INI_GENERAL = 'general';

constructor TSSApplicationSettings.Create;
var
  INI: TIniFile;
  TmpStr: string;
begin
  inherited Create;

  { ------------------------------ }
  { Create user settings directory }
  { ------------------------------ }
  FConfigurationPath := GetAppConfigDir(False);
  if DirectoryExists(FConfigurationPath) = False then
  begin
    CreateDir(FConfigurationPath);
  end;

  FConfigurationFile := FConfigurationPath + 'staysharp.ini';
  INI := TINIFile.Create(FConfigurationFile);
  if FileExists(FConfigurationFile) = False then
  begin
    { --------------------------------------------- }
    { Create configuration file with default values }
    { --------------------------------------------- }
    INI.WriteString(C_INI_GENERAL, 'is_maximized', 'no');
    INI.WriteString(C_INI_GENERAL, 'position_top', '40');
    INI.WriteString(C_INI_GENERAL, 'position_left', '200');
    INI.WriteString(C_INI_GENERAL, 'position_height', '840');
    INI.WriteString(C_INI_GENERAL, 'position_width', '1080');
    INI.WriteString(C_INI_GENERAL, 'activities_panel_height', '450');
    INI.WriteString(C_INI_GENERAL, 'show_accounts_at_startup', 'no');
    INI.WriteString(C_INI_GENERAL, 'backup_directory', '');
  end;

  { ------------------------------ }
  { Read configuration file values }
  { ------------------------------ }
  FPositionTop := StrToInt(INI.ReadString(C_INI_GENERAL, 'position_top', '40'));
  FPositionLeft := StrToInt(INI.ReadString(C_INI_GENERAL, 'position_left', '200'));
  FPositionHeight := StrToInt(INI.ReadString(C_INI_GENERAL, 'position_height', '840'));
  FPositionWidth := StrToInt(INI.ReadString(C_INI_GENERAL, 'position_width', '1080'));
  FActivitiesPanelHeight := StrToInt(INI.ReadString(C_INI_GENERAL, 'activities_panel_height', '450'));
  FBackupDirectory := INI.ReadString(C_INI_GENERAL, 'backup_directory', '');

  TmpStr := INI.ReadString(C_INI_GENERAL, 'is_maximized', 'no');
  if TmpStr = 'yes' then
    FIsMaximized := True
  else
    FIsMaximized := False;

  TmpStr := INI.ReadString(C_INI_GENERAL, 'show_accounts_at_startup', 'no');
  if TmpStr = 'yes' then
    FShowAccountsAtStartup := True
  else
    FShowAccountsAtStartup := False;

  FreeAndNil(INI);
end;

procedure TSSApplicationSettings.SaveConfigurationFile();
var
  INI: TIniFile;
begin
  INI := TINIFile.Create(FConfigurationFile);
  if FIsMaximized = True then
    INI.WriteString(C_INI_GENERAL, 'is_maximized', 'yes')
  else
    INI.WriteString(C_INI_GENERAL, 'is_maximized', 'no');
  INI.WriteString(C_INI_GENERAL, 'position_top', IntToStr(FPositionTop));
  INI.WriteString(C_INI_GENERAL, 'position_left', IntToStr(FPositionLeft));
  INI.WriteString(C_INI_GENERAL, 'position_height', IntToStr(FPositionHeight));
  INI.WriteString(C_INI_GENERAL, 'position_width', IntToStr(FPositionWidth));
  INI.WriteString(C_INI_GENERAL, 'activities_panel_height', IntToStr(FActivitiesPanelHeight));
  INI.WriteString(C_INI_GENERAL, 'backup_directory', FBackupDirectory);
  if FShowAccountsAtStartup = True then
    INI.WriteString(C_INI_GENERAL, 'show_accounts_at_startup', 'yes')
  else
    INI.WriteString(C_INI_GENERAL, 'show_accounts_at_startup', 'no');
  FreeAndNil(INI);
end;

end.
