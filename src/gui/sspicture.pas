unit SSPicture;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, DB;

type

  { TSSPicture }
  TSSPicture = class
  private
    FPictureNo: integer;
    FAccountNo: integer;
    FCreationDate: string;
    FLayoutId: integer;
    FDataBLOB: TMemoryStream;
  public
    constructor Create;
    property PictureNo: integer read FPictureNo write FPictureNo;
    property AccountNo: integer read FAccountNo write FAccountNo;
    property CreationDate: string read FCreationDate write FCreationDate;
    property LayoutId: integer read FLayoutId write FLayoutId;
    property DataBLOB: TMemoryStream read FDataBLOB write FDataBLOB;
    procedure Clear();
    procedure DBFind(const _DBConnection: TSQLConnection; _AccountNo: integer; _CreationDate: string; _LayoutId: integer);
    procedure DBInsertReplace(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction);
    procedure DBDelete(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction);
  end;

implementation

constructor TSSPicture.Create;
begin
  inherited Create;
  FPictureNo := -1;
  FAccountNo := -1;
  FCreationDate := '';
  FLayoutId := -1;
  FDataBLOB := TMemoryStream.Create;
  FDataBLOB.Position := 0;
end;

procedure TSSPicture.Clear();
begin
  FPictureNo := -1;
  FAccountNo := -1;
  FCreationDate := '';
  FLayoutId := -1;
  FDataBLOB.Clear;
  FDataBLOB.Position := 0;
end;

procedure TSSPicture.DBFind(const _DBConnection: TSQLConnection; _AccountNo: integer; _CreationDate: string; _LayoutId: integer);
var
  SQL: string;
  Query: TSQLQuery;
begin
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := _DBConnection;
    Query.Clear;

    SQL := 'SELECT * FROM picture WHERE account_no = :AccountNo AND creation_date = :CreationDate AND layout_id = :LayoutId';
    Query.SQL.Text := SQL;
    Query.Params.ParamByName('AccountNo').AsInteger := _AccountNo;
    Query.Params.ParamByName('CreationDate').AsString := _CreationDate;
    Query.Params.ParamByName('LayoutId').AsInteger := _LayoutId;
    Query.Open;

    if Query.RecordCount > 0 then
    begin
      Query.First;
      FPictureNo := Query.FieldByName('picture_no').AsInteger;
      FAccountNo := Query.FieldByName('account_no').AsInteger;
      FCreationDate := Query.FieldByName('creation_date').AsString;
      FLayoutId := Query.FieldByName('layout_id').AsInteger;
      FDataBLOB.LoadFromStream(Query.CreateBlobStream(Query.FieldByName('dataBLOB'), bmRead));
    end;
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSPicture.DBInsertReplace(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction);
var
  SQL: string;
  Query: TSQLQuery;
begin
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := _DBConnection;
    Query.SQL.Clear;

    SQL := 'INSERT OR REPLACE INTO picture(picture_no,account_no,creation_date,layout_id,dataBLOB) ';
    SQL := SQL + 'VALUES(:PictureNo,:AccountNo,:CreationDate,:LayoutId,:DataBLOB)';
    Query.SQL.Text := SQL;
    if FPictureNo = -1 then
      Query.Params.ParamByName('PictureNo').Value := NULL
    else
      Query.Params.ParamByName('PictureNo').AsInteger := FPictureNo;
    Query.Params.ParamByName('AccountNo').AsInteger := FAccountNo;
    Query.Params.ParamByName('CreationDate').AsString := FCreationDate;
    Query.Params.ParamByName('LayoutId').AsInteger := FLayoutId;
    Query.Params.ParamByName('DataBLOB').LoadFromStream(FDataBLOB, ftBlob);
    Query.ExecSQL;
    _DBTransaction.Commit; // Commit changes to database
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

procedure TSSPicture.DBDelete(const _DBConnection: TSQLConnection; const _DBTransaction: TSQLTransaction);
var
  SQL: string;
  Query: TSQLQuery;
begin
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := _DBConnection;
    Query.SQL.Clear;

    SQL := 'DELETE FROM picture WHERE picture_no = :PictureNo';
    Query.SQL.Text := SQL;
    Query.Params.ParamByName('PictureNo').AsInteger := FPictureNo;
    Query.ExecSQL;
    _DBTransaction.Commit; // Commit changes to database
    Query.Close;
  finally
    FreeAndNil(Query);
  end;
end;

end.


