{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit workout_list;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, sqldb, FileUtil, Forms, Controls, Graphics, Dialogs,
  DBGrids, StdCtrls;

type

  { TForm_WorkoutList }

  TForm_WorkoutList = class(TForm)
    Button_Close: TButton;
    calories: TLongintField;
    creation_date: TStringField;
    DataSource_Workouts: TDataSource;
    DBGrid1: TDBGrid;
    duration: TStringField;
    feeling_name: TStringField;
    Label_AmountTotalNumberOfWorkouts: TLabel;
    Label_TotalNumberOfWorkouts: TLabel;
    max_cf: TLongintField;
    mean_cf: TLongintField;
    min_cf: TLongintField;
    note: TStringField;
    SQLQuery_Workouts: TSQLQuery;
    start_time: TStringField;
    workout_name: TStringField;
    procedure Button_CloseClick(Sender: TObject);
    procedure DataSource_WorkoutsDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    { My custom stuff }
    procedure SetDBGridColumns();
  private

  public

  end;

var
  Form_WorkoutList: TForm_WorkoutList;

implementation

{$R *.lfm}

uses main;

{ TForm_WorkoutList }

procedure TForm_WorkoutList.SetDBGridColumns();
begin
  {DBGrid1.DataSource.DataSet.FieldByName('workout_no').Visible := False;
  DBGrid1.DataSource.DataSet.FieldByName('account_no').Visible := False;
  DBGrid1.DataSource.DataSet.FieldByName('workout_type_no').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('workout_type_no').DisplayLabel := SDbFieldWorkoutType;
  DBGrid1.DataSource.DataSet.FieldByName('creation_date').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('creation_date').DisplayLabel := SDbFieldDate;
  DBGrid1.DataSource.DataSet.FieldByName('start_time').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('start_time').DisplayLabel := SDFieldTime;
  DBGrid1.DataSource.DataSet.FieldByName('duration').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('duration').DisplayLabel := SDbFieldDuration;
  DBGrid1.DataSource.DataSet.FieldByName('min_cf').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('min_cf').DisplayLabel := SDbFieldMinCF;
  DBGrid1.DataSource.DataSet.FieldByName('mean_cf').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('mean_cf').DisplayLabel := SDbFieldMeanCF;
  DBGrid1.DataSource.DataSet.FieldByName('max_cf').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('max_cf').DisplayLabel := SDbFieldMaxCF;
  DBGrid1.DataSource.DataSet.FieldByName('calories').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('calories').DisplayLabel := SDbFieldCalories;
  DBGrid1.DataSource.DataSet.FieldByName('feeling_type_no').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('feeling_type_no').DisplayLabel := SDbFieldFeelingType;
  DBGrid1.DataSource.DataSet.FieldByName('note').Visible := True;
  DBGrid1.DataSource.DataSet.FieldByName('note').DisplayLabel := SDbFieldNote;}
end;

procedure TForm_WorkoutList.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_WorkoutList.DataSource_WorkoutsDataChange(Sender: TObject;
  Field: TField);
begin

end;

procedure TForm_WorkoutList.FormShow(Sender: TObject);
var SQL: String;
begin
    if Form_Main.CurrentAccount <> nil then
  begin
    SQL := 'SELECT w.*, wt.name as workout_name, ft.name as feeling_name FROM workout as w, workout_type as wt, feeling_type as ft WHERE ';
    SQL := SQL + 'w.account_no = ' + IntToStr(Form_Main.CurrentAccount.FAccountNo) + ' ';
    SQL := SQL + 'AND w.workout_type_no = wt.workout_type_no ';
    SQL := SQL + 'AND w.feeling_type_no = ft.feeling_type_no';

    SQLQuery_Workouts.Close;
    SQLQuery_Workouts.SQL.Text := SQL;
    SQLQuery_Workouts.Open;
    SQLQuery_Workouts.Last;

    SetDBGridColumns();
    Label_AmountTotalNumberOfWorkouts.Caption := IntToStr(SQLQuery_Workouts.RecordCount);
  end;
end;

end.

