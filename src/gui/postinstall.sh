#!/bin/bash

# Get extract/current directory
appdir=$(pwd)
echo $appdir

# ~/.local/share/applications/ directory
destdir=$HOME/.local/share/applications/
echo $destdir

# Create staysharp.desktop file
echo "[Desktop Entry]" > $destdir/staysharp.desktop 
echo "Name=StaySharp" >> $destdir/staysharp.desktop 
echo "Exec=$appdir/staysharp"  >> $destdir/staysharp.desktop 
echo "Icon=$appdir/staysharp.png" >> $destdir/staysharp.desktop 
echo "Type=Application" >> $destdir/staysharp.desktop 
echo "Categories=Utility;" >> $destdir/staysharp.desktop
