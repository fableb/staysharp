{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit SSMeal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, SSMealItem;

type
  { TSSMeal }

  TSSMeal = class
  public
    FMealNo: longint;
    FAccountNo: longint;
    FMealTypeNo: longint;
    FCreationDate: string;
    FCreationTime: string;
    FFoods: array of TSSMealItem;
    procedure DBSelect(const DBConnection: TSQLConnection; const MealNo: integer);
  private
  end;

implementation

procedure TSSMeal.DBSelect(const DBConnection: TSQLConnection;
  const MealNo: integer);
var
  Query: TSQLQuery;
  SQL: string;
begin
  SQL := 'SELECT * FROM meal WHERE meal_no = ' + IntToStr(MealNo);
  Query := TSQLQuery.Create(nil);
  try
    Query.DataBase := DBConnection;
    Query.Clear;
    Query.SQL.Text := SQL;
    Query.Open;

    { Populate fields }
    FMealNo := Query.FieldByName('meal_no').AsInteger;
    FAccountNo := Query.FieldByName('account_no').AsInteger;
    FMealTypeNo := Query.FieldByName('meal_type_no').AsInteger;
    FCreationDate := Query.FieldByName('creation_date').AsString;
    FCreationTime := Query.FieldByName('creation_time').AsString;

    Query.Close;
  finally
    FreeAndNil(Query);
  end;

end;

end.

