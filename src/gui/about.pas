{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit about;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, ComCtrls;

type

  { TForm_About }

  TForm_About = class(TForm)
    Button_Close: TButton;
    Image_Logo: TImage;
    Label_ApplicationName: TLabel;
    Label_Author: TLabel;
    Label_Email: TLabel;
    Label_Version: TLabel;
    Memo_License: TMemo;
    PageControl1: TPageControl;
    Panel1: TPanel;
    TabSheet_License: TTabSheet;
    TabSheet_About: TTabSheet;
    procedure Button_CloseClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form_About: TForm_About;

implementation

{$R *.lfm}

uses main;

{ TForm_About }

procedure TForm_About.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_About.FormShow(Sender: TObject);
begin
  Label_Version.Caption := 'Version ' + StaySharpVersion;
  Label_Author.Caption := '(C) 2018, Fabrice LEBEL';
  Label_Email.Caption := 'fabrice.lebel.pro@outlook.com';
  TabSheet_About.Show;
end;

end.

