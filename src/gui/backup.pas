unit backup;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons;

resourcestring
  SDlgBackupError = 'Error. Cannot perform the backup.';
  SDlgBackupSuccess1 = 'File ';
  SDlgBackupSuccess2 = ' correctly copied.';

type

  { TForm_Backup }

  TForm_Backup = class(TForm)
    Button_Close: TButton;
    Button_Backup: TButton;
    CheckBox_SaveBackupDirectory: TCheckBox;
    Edit_BackupDirectory: TEdit;
    Label_BackupDirectory: TLabel;
    SelectDirectoryDialog1: TSelectDirectoryDialog;
    SpeedButton1: TSpeedButton;
    procedure Button_BackupClick(Sender: TObject);
    procedure Button_CloseClick(Sender: TObject);
    procedure CheckBox_SaveBackupDirectoryChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private

  public

  end;

var
  Form_Backup: TForm_Backup;

implementation

{$R *.lfm}

uses main;

{ TForm_Backup }

procedure TForm_Backup.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_Backup.CheckBox_SaveBackupDirectoryChange(Sender: TObject);
begin
  if CheckBox_SaveBackupDirectory.Checked = True then
    Form_Main.ApplicationSettings.Backupdirectory := Edit_BackupDirectory.Text
  else
    Form_Main.ApplicationSettings.Backupdirectory := '';
end;

procedure TForm_Backup.FormShow(Sender: TObject);
begin
  if Form_Main.ApplicationSettings.Backupdirectory = '' then
  begin
    Edit_BackupDirectory.Text := '';
    CheckBox_SaveBackupDirectory.Checked := False;
  end
  else
  begin
    Edit_BackupDirectory.Text := Form_Main.ApplicationSettings.Backupdirectory;
    CheckBox_SaveBackupDirectory.Checked := True;
  end;
end;

procedure TForm_Backup.Button_BackupClick(Sender: TObject);
var
  BackupFileName: string;
  TimeStamp: string;
begin
  TimeStamp := FormatDateTime('-yyymmdd-hhmmss', Now) + '.ssf';
  BackupFileName := Edit_BackupDirectory.Text + '/' + ExtractFileName(Form_Main.DBConnection.DatabaseName);
  BackupFileName := StringReplace(BackupFileName, '.ssf', TimeStamp, [rfIgnoreCase]);
  BackupFileName := ExpandFileName(BackupFileName);
  if CopyFile(Form_Main.DBConnection.DatabaseName, BackupFileName) = True then
  begin
    ShowMessage(SDlgBackupSuccess1 + BackupFileName + SDlgBackupSuccess2);
    Close;
  end
  else
  begin
    ShowMessage(SDlgBackupError);
    Close;
  end;
end;

procedure TForm_Backup.SpeedButton1Click(Sender: TObject);
var
  BackupDirectoryName: string;
begin
  if SelectDirectoryDialog1.Execute = True then
  begin
    BackupDirectoryName := ExpandFileName(SelectDirectoryDialog1.FileName);
    Edit_BackupDirectory.Text := BackupDirectoryName;
  end;
end;

end.

