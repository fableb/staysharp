{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit food_list;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, DB, FileUtil, Forms, Controls, Graphics, Dialogs,
  DBGrids, StdCtrls, ExtCtrls, Spin, SSFood, SSFile;

type

  { TForm_FoodList }

  TForm_FoodList = class(TForm)
    Button_AddToMeal: TButton;
    Button_ClearSearch: TButton;
    Button_DeleteUserFood: TButton;
    Button_EditUserFood: TButton;
    Button_NewUserFood: TButton;
    Button_Close: TButton;
    CheckBox_ShowUserFoods: TCheckBox;
    ComboBox_FoodCategories: TComboBox;
    DataSource_Foods: TDataSource;
    DBGrid_Foods: TDBGrid;
    Edit_SearchInName: TEdit;
    FloatSpinEdit_FoodQuantity: TFloatSpinEdit;
    Label_FoodQuantity: TLabel;
    Label_SearchInName: TLabel;
    Label_FoodCategories: TLabel;
    SQLQuery_Foods: TSQLQuery;
    procedure Button_NewUserFoodClick(Sender: TObject);
    procedure Button_ClearSearchClick(Sender: TObject);
    procedure Button_CloseClick(Sender: TObject);
    procedure Button_DeleteUserFoodClick(Sender: TObject);
    procedure Button_EditUserFoodClick(Sender: TObject);
    procedure Button_AddToMealClick(Sender: TObject);
    procedure CheckBox_ShowUserFoodsChange(Sender: TObject);
    procedure ComboBox_FoodCategoriesChange(Sender: TObject);
    procedure Edit_SearchInNameChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    { My stuff }
    procedure SelectFoods();
    procedure UpdateControls();
  private

  public
    { My stuff }
    CurrentFood: TSSFood;

  end;

var
  Form_FoodList: TForm_FoodList;

implementation

{$R *.lfm}

uses main, meal, food;

{ TForm_FoodList }

procedure TForm_FoodList.SelectFoods();
var
  SQL: string;
begin
  SQL := 'SELECT f.*, fu.name as unit_name FROM food as f, food_unit as fu WHERE f.food_unit_no = fu.food_unit_no ';

  if ComboBox_FoodCategories.ItemIndex <> 0 then
  begin
    // Select food category
    if ComboBox_FoodCategories.ItemIndex > 0 then
      SQL := SQL + 'AND f.food_category = ' + IntToStr(ComboBox_FoodCategories.ItemIndex) + ' ';
  end;

  if Trim(Edit_SearchInName.Text) <> '' then
  begin
    // Search with keyword in names
    SQL := SQL + 'AND f.name LIKE ' + QuotedStr('%' + Edit_SearchInName.Text + '%') + ' ';
  end;

  if CheckBox_ShowUserFoods.Checked = True then
  begin
    // Show only user foods
    SQL := SQL + 'AND f.user_defined = 1';
  end;
  SQLQuery_Foods.Clear;
  SQLQuery_Foods.SQL.Text := SQL;
  SQLQuery_Foods.Open;

  UpdateControls();
end;

procedure TForm_FoodList.UpdateControls();
begin
  // Manage edit and delete buttons
  if SQLQuery_Foods.RecordCount = 0 then
  begin
    Button_EditUserFood.Enabled := False;
    Button_DeleteUserFood.Enabled := False;

    if Button_AddToMeal.Visible = True then // Adding meal item
      Button_AddToMeal.Enabled := False;
  end
  else
  begin
    if Button_AddToMeal.Visible = True then // Adding meal item
      Button_AddToMeal.Enabled := True;

    if DBGrid_Foods.DataSource.DataSet.FieldByName('user_defined').AsInteger = 0 then
    begin
      // System foods
      Button_EditUserFood.Enabled := False;
      Button_DeleteUserFood.Enabled := False;
    end
    else
    begin
      // User foods
      Button_EditUserFood.Enabled := True;
      Button_DeleteUserFood.Enabled := True;
    end;
  end;
end;

procedure TForm_FoodList.Button_CloseClick(Sender: TObject);
begin
  Close;
  if Form_Main.CurrentAccount <> nil then
  begin
    { An account is opened }
    if Form_Main.CurrentMeal <> nil then
    begin
      { An account is opened and we were in the meal form }
      Form_Meal.UpdateTotalNutrients();
      Form_Meal.UpdateControls();
    end;
    Form_Main.UpdateDailyWorkouts();
    Form_Main.UpdateStatistics();
    Form_Main.UpdateMealList();
    Form_Main.UpdateMealItemList();
  end;
end;

procedure TForm_FoodList.Button_DeleteUserFoodClick(Sender: TObject);
begin
  case QuestionDlg(SDlgDeleteFoodTitle, SDlgDeleteFoodMsg, mtCustom, [mrNo, SDlgBtnNo,
      'IsDefault', mrYes, SDlgBtnYes], '') of
    mrYes:
    begin
      CurrentFood := TSSFood.Create;
      CurrentFood.DBDelete(Form_Main.DBConnection, Form_Main.SQLTransaction1,
        DBGrid_Foods.DataSource.DataSet.FieldByName('food_no').AsInteger);
      FreeAndNil(CurrentFood); // Cleanup
      SelectFoods(); // Update food list
    end;
    mrNo:
    begin

    end;
    mrCancel:
    begin

    end;
  end;

end;

procedure TForm_FoodList.Button_EditUserFoodClick(Sender: TObject);
begin
  CurrentFood := TSSFood.Create;
  CurrentFood.DBSelect(Form_Main.DBConnection,
    DBGrid_Foods.DataSource.DataSet.FieldByName('food_no').AsInteger);
  Form_Food.ShowModal;
  FreeAndNil(CurrentFood); // Cleanup
  SelectFoods(); // Update food list
end;

procedure TForm_FoodList.Button_AddToMealClick(Sender: TObject);
begin
  Form_Meal.SQLQuery_MealItems.Append;
  Form_Meal.MealItemQuantityChange(SQLQuery_Foods.FieldByName('food_no').AsInteger, FloatSpinEdit_FoodQuantity.Value);
end;

procedure TForm_FoodList.CheckBox_ShowUserFoodsChange(Sender: TObject);
begin
  SelectFoods();
end;

procedure TForm_FoodList.ComboBox_FoodCategoriesChange(Sender: TObject);
begin
  SelectFoods();
end;

procedure TForm_FoodList.Edit_SearchInNameChange(Sender: TObject);
begin
  SelectFoods();
end;

procedure TForm_FoodList.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  SQLQuery_Foods.Close;
  ComboBox_FoodCategories.Clear;
end;

procedure TForm_FoodList.Button_NewUserFoodClick(Sender: TObject);
begin
  { We add a new food... }
  CurrentFood := TSSFood.Create;
  CurrentFood.FFoodNo := -1; // Food creation flag
  CurrentFood.FUserDefined := 1; // It is a user defined food
  CurrentFood.FUnitQuantity := 100.0; // Default weight
  CurrentFood.FFoodUnitNo := 1; // Default weight unit: grams
  Form_Food.ShowModal;
  FreeAndNil(CurrentFood); // Cleanup
  SelectFoods(); // Update food list
end;

procedure TForm_FoodList.Button_ClearSearchClick(Sender: TObject);
begin
  Edit_SearchInName.Text := '';
end;

procedure TForm_FoodList.FormShow(Sender: TObject);
begin
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_None);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Starters);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Fruits);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Cereals);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Meat);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Milk);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Beverages);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Sugar);
  ComboBox_FoodCategories.Items.Add(DBFoodCategory_IceCream);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Fats);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Miscellaneous);
  ComboBox_FoodCategories.Items.Add(SDBFoodCategory_Baby);
  ComboBox_FoodCategories.ItemIndex := 0;
  Button_ClearSearchClick(Sender); // Clear search in name
  CheckBox_ShowUserFoods.Checked := False;
  SelectFoods(); // Default food list

  // We come from the 'Daily Meals' form
  if Form_Meal.CurrentMealItem <> nil then
  begin
    { Add a new meal item from food list }
    Label_FoodQuantity.Visible := True;
    FloatSpinEdit_FoodQuantity.Visible := True;
    Button_AddToMeal.Visible := True;
  end
  else
  begin
    Label_FoodQuantity.Visible := False;
    FloatSpinEdit_FoodQuantity.Visible := False;
    Button_AddToMeal.Visible := False;
  end;
end;

end.
