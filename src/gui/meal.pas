{ StaySharp fitness and health software.

  Copyright (C) 2018 Fabrice LEBEL (fabrice.lebel.pro@outlook.com)

  This source is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This code is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  A copy of the GNU General Public License is available on the World Wide Web
  at <http://www.gnu.org/copyleft/gpl.html>. You can also obtain it by writing
  to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
  Boston, MA 02110-1335, USA.
}

unit meal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DB, sqldb, FileUtil, DateTimePicker, Forms, Controls,
  Graphics, Dialogs, StdCtrls, ValEdit, DBCtrls, DBGrids, BufDataset, memds, SSMealItem;

type

  { TForm_Meal }

  TForm_Meal = class(TForm)
    Button_CreateFoodFromMeal: TButton;
    Button_DeleteMealItem: TButton;
    Button_Foods: TButton;
    Button_Save: TButton;
    Button_Cancel: TButton;
    ComboBox_MealTypes: TComboBox;
    DataSource_MealItems: TDataSource;
    DateTimePicker_CreationTime: TDateTimePicker;
    DateTimePicker_CreationDate: TDateTimePicker;
    DBGrid_FoodList: TDBGrid;
    GroupBox_MainNutrients: TGroupBox;
    GroupBox_FoodList: TGroupBox;
    Label_TotalProtein: TLabel;
    Label_TotalCarbohydrate: TLabel;
    Label_TotalSugar: TLabel;
    Label_TotalFat: TLabel;
    Label_TotalEnergy: TLabel;
    Label_TotalSalt: TLabel;
    Label_TotalCholesterol: TLabel;
    Label_Energy: TLabel;
    Label_Salt: TLabel;
    Label_Cholesterol: TLabel;
    Label_Fats: TLabel;
    Label_Sugar: TLabel;
    Label_Carbohydrates: TLabel;
    Label_Proteins: TLabel;
    Label_CreationTime: TLabel;
    Label_CreationDate: TLabel;
    Label_MealType: TLabel;
    SQLQuery_Foods: TSQLQuery;
    SQLQuery_Foodsalcohol: TFloatField;
    SQLQuery_Foodsbeta_carotene: TFloatField;
    SQLQuery_Foodscalcium: TFloatField;
    SQLQuery_Foodscarbohydrate: TFloatField;
    SQLQuery_Foodscholesterol: TFloatField;
    SQLQuery_Foodsenergy: TFloatField;
    SQLQuery_Foodsfat: TFloatField;
    SQLQuery_Foodsfibres: TFloatField;
    SQLQuery_Foodsfood_no: TLongintField;
    SQLQuery_Foodsiron: TFloatField;
    SQLQuery_Foodsmagnesium: TFloatField;
    SQLQuery_Foodsmeal_item_no: TLongintField;
    SQLQuery_Foodsmeal_no: TLongintField;
    SQLQuery_Foodsname: TStringField;
    SQLQuery_Foodsorganic_acids: TFloatField;
    SQLQuery_Foodsphosphorus: TFloatField;
    SQLQuery_Foodspolyols: TFloatField;
    SQLQuery_Foodspotassium: TFloatField;
    SQLQuery_Foodsprotein: TFloatField;
    SQLQuery_Foodsquantity: TFloatField;
    SQLQuery_Foodssalt: TFloatField;
    SQLQuery_Foodssodium: TFloatField;
    SQLQuery_Foodssugar: TFloatField;
    SQLQuery_Foodsunit_name: TStringField;
    SQLQuery_Foodsunit_quantity: TFloatField;
    SQLQuery_Foodsvitamin_b1: TFloatField;
    SQLQuery_Foodsvitamin_b12: TFloatField;
    SQLQuery_Foodsvitamin_b2: TFloatField;
    SQLQuery_Foodsvitamin_b3: TFloatField;
    SQLQuery_Foodsvitamin_b5: TFloatField;
    SQLQuery_Foodsvitamin_b6: TFloatField;
    SQLQuery_Foodsvitamin_b9: TFloatField;
    SQLQuery_Foodsvitamin_c: TFloatField;
    SQLQuery_Foodsvitamin_d: TFloatField;
    SQLQuery_Foodsvitamin_e: TFloatField;
    SQLQuery_Foodsvitamin_k1: TFloatField;
    SQLQuery_Foodsvitamin_k2: TFloatField;
    SQLQuery_Foodswater: TFloatField;
    SQLQuery_Foodszinc: TFloatField;
    SQLQuery_Mealaccount_no: TLongintField;
    SQLQuery_Mealcreation_date: TStringField;
    SQLQuery_Mealcreation_time: TStringField;
    SQLQuery_MealItems: TSQLQuery;
    SQLQuery_Meal: TSQLQuery;
    SQLQuery_MealItemsalcohol: TFloatField;
    SQLQuery_MealItemsbeta_carotene: TFloatField;
    SQLQuery_MealItemscalcium: TFloatField;
    SQLQuery_MealItemscarbohydrate: TFloatField;
    SQLQuery_MealItemscholesterol: TFloatField;
    SQLQuery_MealItemsenergy: TFloatField;
    SQLQuery_MealItemsfat: TFloatField;
    SQLQuery_MealItemsfibres: TFloatField;
    SQLQuery_MealItemsfood_no: TLongintField;
    SQLQuery_MealItemsiron: TFloatField;
    SQLQuery_MealItemsmagnesium: TFloatField;
    SQLQuery_MealItemsmeal_item_no: TAutoIncField;
    SQLQuery_MealItemsmeal_no: TLongintField;
    SQLQuery_MealItemsname: TStringField;
    SQLQuery_MealItemsorganic_acids: TFloatField;
    SQLQuery_MealItemsphosphorus: TFloatField;
    SQLQuery_MealItemspolyols: TFloatField;
    SQLQuery_MealItemspotassium: TFloatField;
    SQLQuery_MealItemsprotein: TFloatField;
    SQLQuery_MealItemsquantity: TFloatField;
    SQLQuery_MealItemssalt: TFloatField;
    SQLQuery_MealItemssodium: TFloatField;
    SQLQuery_MealItemssugar: TFloatField;
    SQLQuery_MealItemsunit_name: TStringField;
    SQLQuery_MealItemsunit_quantity: TFloatField;
    SQLQuery_MealItemsvitamin_b1: TFloatField;
    SQLQuery_MealItemsvitamin_b12: TFloatField;
    SQLQuery_MealItemsvitamin_b2: TFloatField;
    SQLQuery_MealItemsvitamin_b3: TFloatField;
    SQLQuery_MealItemsvitamin_b5: TFloatField;
    SQLQuery_MealItemsvitamin_b6: TFloatField;
    SQLQuery_MealItemsvitamin_b9: TFloatField;
    SQLQuery_MealItemsvitamin_c: TFloatField;
    SQLQuery_MealItemsvitamin_d: TFloatField;
    SQLQuery_MealItemsvitamin_e: TFloatField;
    SQLQuery_MealItemsvitamin_k1: TFloatField;
    SQLQuery_MealItemsvitamin_k2: TFloatField;
    SQLQuery_MealItemswater: TFloatField;
    SQLQuery_MealItemszinc: TFloatField;
    SQLQuery_Mealmeal_no: TLongintField;
    SQLQuery_Mealmeal_type_no: TLongintField;
    SQLQuery_MealType: TSQLQuery;
    SQLQuery_MealTypemeal_type_no: TLongintField;
    SQLQuery_MealTypename: TStringField;
    procedure Button_CancelClick(Sender: TObject);
    procedure Button_CreateFoodFromMealClick(Sender: TObject);
    procedure Button_DeleteMealItemClick(Sender: TObject);
    procedure Button_FoodsClick(Sender: TObject);
    procedure Button_SaveClick(Sender: TObject);
    procedure ComboBox_MealTypesChange(Sender: TObject);
    procedure DataSource_MealItemsDataChange(Sender: TObject; Field: TField);
    procedure DataSource_MealItemsStateChange(Sender: TObject);
    procedure DBGrid_FoodListCellClick(Column: TColumn);
    procedure DBGrid_FoodListEditingDone(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MealItemQuantityChange(const FoodNo: integer; const Quantity: double);
  private
    procedure SetMealTypesComboList(ItemIndex: integer);

  public
    CurrentMealItem: TSSMealItem;
    procedure UpdateControls();
    procedure UpdateTotalNutrients();

  end;

var
  Form_Meal: TForm_Meal;
  SelectedMealItemNo: Integer;
  SelectedFoodNo: Integer;

implementation

{$R *.lfm}

uses main, food_list, food_from_meal;

{ TForm_Meal }

procedure TForm_Meal.SetMealTypesComboList(ItemIndex: integer);
var
  SQL: string;
begin
  if ComboBox_MealTypes.Items.Count = 0 then
  begin
    SQL := 'SELECT meal_type_no, name FROM meal_type';
    SQLQuery_MealType.Close;
    SQLQuery_MealType.SQL.Text := SQL;
    SQLQuery_MealType.Open;
    SQLQuery_MealType.First;
    while (not SQLQuery_MealType.EOF) do
    begin
      // do something with the current record
      ComboBox_MealTypes.Items.Add(SQLQuery_MealType.FieldByName('name').AsString);
      // move to the next record
      SQLQuery_MealType.Next;
    end;
  end;
  ComboBox_MealTypes.ItemIndex := ItemIndex;
end;

procedure TForm_Meal.UpdateControls();
begin
  if (DataSource_MealItems.DataSet.RecordCount > 0) then
  begin
    Button_DeleteMealItem.Enabled := True;
    Button_CreateFoodFromMeal.Enabled := True;
  end
  else
  begin
    Button_DeleteMealItem.Enabled := False;
    Button_CreateFoodFromMeal.Enabled := False;
  end;

  if (DataSource_MealItems.DataSet.RecordCount > 0) and (ComboBox_MealTypes.ItemIndex > 0) then
  begin
    Button_Save.Enabled := True;
  end
  else
  begin
    Button_Save.Enabled := False;
  end;
end;

procedure TForm_Meal.Button_CancelClick(Sender: TObject);
begin
  SQLQuery_MealItems.Cancel;
  SQLQuery_MealItems.Close;
  ModalResult := mrCancel;
end;

procedure TForm_Meal.Button_CreateFoodFromMealClick(Sender: TObject);
begin
  if DataSource_MealItems.DataSet.RecordCount > 0 then
  begin
    // 1. Check if all foods measure units are in grams
    // 2. Ask for a food BufDataset_MealItemsName
    Form_FoodFromMeal.ShowModal;
  end;
end;

procedure TForm_Meal.Button_DeleteMealItemClick(Sender: TObject);
var
  SQL: string;
begin
  if SelectedMealItemNo = 0 then
  begin
    { Delete meal item not saved in the database }
    if SQLQuery_MealItems.Locate('food_no',SelectedFoodNo,[]) then
      SQLQuery_MealItems.Delete;
  end
  else
  begin
    { Delete meal item saved in the database }
    SQL := 'DELETE FROM meal_item WHERE meal_item_no = ' + IntToStr(SelectedMealItemNo);
    SQLQuery_MealItems.Close;
    SQLQuery_MealItems.SQL.Clear;
    SQLQuery_MealItems.SQL.Text := SQL;
    SQLQuery_MealItems.ExecSQL;
    Form_Main.SQLTransaction1.Commit;

    { Reload meal items for the current meal }
    SQLQuery_MealItems.Close;
    SQLQuery_MealItems.SQL.Clear;
    SQLQuery_MealItems.SQL.Text :=
      'SELECT * FROM meal_item WHERE meal_no = ' + IntToStr(Form_Main.CurrentMeal.FMealNo);
    SQLQuery_MealItems.Open;
  end;
  UpdateTotalNutrients();
end;

procedure TForm_Meal.MealItemQuantityChange(const FoodNo: integer; const Quantity: double);
var
  SQL: string;
  UnitQuantity, Factor: double;
begin
  { Search for the food in the database }
  SQL := 'SELECT food.*, food_unit.name as unit_name FROM food, food_unit ';
  SQL := SQL + 'WHERE food.food_no = ' + IntToStr(FoodNo) + ' ';
  SQL := SQL + 'AND food.food_unit_no = food_unit.food_unit_no';
  SQLQuery_Foods.Clear;
  SQLQuery_Foods.SQL.Text := SQL;
  SQLQuery_Foods.Open;

  { Recompute values }
  UnitQuantity := SQLQuery_Foods.FieldByName('unit_quantity').AsFloat;
  Factor := Quantity / UnitQuantity;

  SQLQuery_MealItems.Edit;
  SQLQuery_MealItems.FieldByName('food_no').AsInteger := SQLQuery_Foods.FieldByName('food_no').AsInteger;
  SQLQuery_MealItems.FieldByName('name').AsString := SQLQuery_Foods.FieldByName('name').AsString;
  SQLQuery_MealItems.FieldByName('energy').AsFloat := SQLQuery_Foods.FieldByName('energy').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('protein').AsFloat := SQLQuery_Foods.FieldByName('protein').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('carbohydrate').AsFloat := SQLQuery_Foods.FieldByName('carbohydrate').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('sugar').AsFloat := SQLQuery_Foods.FieldByName('sugar').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('alcohol').AsFloat := SQLQuery_Foods.FieldByName('alcohol').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('fat').AsFloat := SQLQuery_Foods.FieldByName('fat').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('cholesterol').AsFloat := SQLQuery_Foods.FieldByName('cholesterol').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('water').AsFloat := SQLQuery_Foods.FieldByName('water').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('salt').AsFloat := SQLQuery_Foods.FieldByName('salt').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('calcium').AsFloat := SQLQuery_Foods.FieldByName('calcium').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('iron').AsFloat := SQLQuery_Foods.FieldByName('iron').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('magnesium').AsFloat := SQLQuery_Foods.FieldByName('magnesium').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('phosphorus').AsFloat := SQLQuery_Foods.FieldByName('phosphorus').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('potassium').AsFloat := SQLQuery_Foods.FieldByName('potassium').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('sodium').AsFloat := SQLQuery_Foods.FieldByName('sodium').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('zinc').AsFloat := SQLQuery_Foods.FieldByName('zinc').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('beta_carotene').AsFloat := SQLQuery_Foods.FieldByName('beta_carotene').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_d').AsFloat := SQLQuery_Foods.FieldByName('vitamin_d').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_e').AsFloat := SQLQuery_Foods.FieldByName('vitamin_e').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_k1').AsFloat := SQLQuery_Foods.FieldByName('vitamin_k1').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_k2').AsFloat := SQLQuery_Foods.FieldByName('vitamin_k2').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_c').AsFloat := SQLQuery_Foods.FieldByName('vitamin_c').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_b1').AsFloat := SQLQuery_Foods.FieldByName('vitamin_b1').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_b2').AsFloat := SQLQuery_Foods.FieldByName('vitamin_b2').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_b3').AsFloat := SQLQuery_Foods.FieldByName('vitamin_b3').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_b5').AsFloat := SQLQuery_Foods.FieldByName('vitamin_b5').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_b6').AsFloat := SQLQuery_Foods.FieldByName('vitamin_b6').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_b9').AsFloat := SQLQuery_Foods.FieldByName('vitamin_b9').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('vitamin_b12').AsFloat := SQLQuery_Foods.FieldByName('vitamin_b12').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('fibres').AsFloat := SQLQuery_Foods.FieldByName('fibres').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('polyols').AsFloat := SQLQuery_Foods.FieldByName('polyols').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('organic_acids').AsFloat := SQLQuery_Foods.FieldByName('organic_acids').AsFloat * Factor;
  SQLQuery_MealItems.FieldByName('unit_name').AsString := SQLQuery_Foods.FieldByName('unit_name').AsString;
  SQLQuery_MealItems.FieldByName('unit_quantity').AsFloat := SQLQuery_Foods.FieldByName('unit_quantity').AsFloat;
  SQLQuery_MealItems.FieldByName('quantity').AsFloat := Quantity;

  UpdateTotalNutrients();
end;

procedure TForm_Meal.UpdateTotalNutrients();
var
  TotalProtein, TotalCarbohydrate, TotalSugar, TotalFat, TotalCholesterol, TotalSalt, TotalEnergy: double;
begin
  try
    TotalProtein := 0.0;
    TotalCarbohydrate := 0.0;
    TotalSugar := 0.0;
    TotalFat := 0.0;
    TotalCholesterol := 0.0;
    TotalSalt := 0.0;
    TotalEnergy := 0.0;
    SQLQuery_MealItems.First;
    while not SQLQuery_MealItems.EOF do
    begin
      TotalProtein := TotalProtein + SQLQuery_MealItems.FieldByName('protein').AsFloat;
      TotalCarbohydrate := TotalCarbohydrate + SQLQuery_MealItems.FieldByName('carbohydrate').AsFloat;
      TotalSugar := TotalSugar + SQLQuery_MealItems.FieldByName('sugar').AsFloat;
      TotalFat := TotalFat + SQLQuery_MealItems.FieldByName('fat').AsFloat;
      TotalCholesterol := TotalCholesterol + SQLQuery_MealItems.FieldByName('cholesterol').AsFloat;
      TotalSalt := TotalSalt + SQLQuery_MealItems.FieldByName('salt').AsFloat;
      TotalEnergy := TotalEnergy + SQLQuery_MealItems.FieldByName('energy').AsFloat;
      SQLQuery_MealItems.Next;
    end;
    Label_TotalProtein.Caption := FormatFloat('0.00', TotalProtein);
    Label_TotalCarbohydrate.Caption := FormatFloat('0.00', TotalCarbohydrate);
    Label_TotalSugar.Caption := FormatFloat('0.00', TotalSugar);
    Label_TotalFat.Caption := FormatFloat('0.00', TotalFat);
    Label_TotalCholesterol.Caption := FormatFloat('0.00', TotalCholesterol);
    Label_TotalSalt.Caption := FormatFloat('0.00', TotalSalt);
    Label_TotalEnergy.Caption := FormatFloat('0.00', TotalEnergy);
  finally
  end;
end;

procedure TForm_Meal.Button_FoodsClick(Sender: TObject);
begin
  CurrentMealItem := TSSMealItem.Create;
  CurrentMealItem.FMealItemNo := -1;
  Form_FoodList.ShowModal;
  FreeAndNil(CurrentMealItem);
end;

procedure UpdateMealItem();
begin

end;

procedure TForm_Meal.Button_SaveClick(Sender: TObject);
var
  Query: TSQLQuery;
  SQL: string;
  MealNoStr: string;
  MealNo: integer;
  FloatFormatSettings: TFormatSettings;
begin
  FloatFormatSettings.DecimalSeparator := '.';
  { Check if we create a new meal or update an existing one }
  if Form_Main.CurrentMeal.FMealNo = -1 then
    MealNoStr := 'NULL' // insert record
  else
    MealNoStr := IntToStr(Form_Main.CurrentMeal.FMealNo);

  { Save meal into 'meal' table }
  SQL := 'INSERT OR REPLACE INTO meal (meal_no,account_no,meal_type_no,creation_date,creation_time) ';
  SQL := SQL + 'VALUES(';
  SQL := SQL + MealNoStr + ',';
  SQL := SQL + IntToStr(Form_Main.CurrentMeal.FAccountNo) + ',';
  SQL := SQL + IntToStr(ComboBox_MealTypes.ItemIndex) + ',';
  SQL := SQL + 'date(''' + FormatDateTime('yyyy-mm-dd', DateTimePicker_CreationDate.DateTime) + '''),';
  SQL := SQL + 'time(''' + FormatDateTime('hh:mm:ss', DateTimePicker_CreationTime.DateTime) + ''')';
  SQL := SQL + ')';
  SQLQuery_Meal.SQL.Clear;
  SQLQuery_Meal.SQL.Text := SQL;
  SQLQuery_Meal.ExecSQL;
  SQLQuery_Meal.Close;

  { Retrieve newly created 'meal_no' }
  if Form_Main.CurrentMeal.FMealNo = -1 then
  begin
    Query := TSQLQuery.Create(nil);
    Query.DataBase := Form_Main.DBConnection;
    SQL := 'SELECT LAST_INSERT_ROWID() AS last_key';
    Query.SQL.Clear;
    Query.SQL.Text := SQL;
    Query.Open;
    Query.Last;
    MealNoStr := IntToStr(Query.FieldByName('last_key').AsInteger);
    Query.Close;
    FreeAndNil(Query);
  end;

  { Insert of update all the meal items with the 'meal_no' value }
  SQL := 'INSERT OR REPLACE INTO meal_item(';
  SQL := SQL + 'meal_item_no,meal_no,quantity,food_no,name,energy,protein,carbohydrate,sugar,alcohol,fat,cholesterol,water,salt,calcium,iron,magnesium,phosphorus,potassium,sodium,zinc,beta_carotene,vitamin_d,vitamin_e,vitamin_k1,vitamin_k2,vitamin_c,vitamin_b1,vitamin_b2,vitamin_b3,vitamin_b5,vitamin_b6,vitamin_b9,vitamin_b12,fibres,polyols,organic_acids,unit_name,unit_quantity';
  SQL := SQL + ') ';
  SQL := SQL + 'VALUES';
  SQLQuery_MealItems.First;
  while not SQLQuery_MealItems.EOF do
  begin
    SQL := SQL + '(';
    if SQLQuery_MealItems.FieldByName('meal_item_no').AsString = '' then
      SQL := SQL + 'NULL,'
    else
      SQL := SQL + SQLQuery_MealItems.FieldByName('meal_item_no').AsString + ',';
    SQL := SQL + MealNoStr + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('quantity').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + SQLQuery_MealItems.FieldByName('food_no').AsString + ',';
    SQL := SQL + QuotedStr(SQLQuery_MealItems.FieldByName('name').AsString) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('energy').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('protein').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('carbohydrate').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('sugar').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('alcohol').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('fat').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('cholesterol').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('water').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('salt').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('calcium').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('iron').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('magnesium').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('phosphorus').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('potassium').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('sodium').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('zinc').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('beta_carotene').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_d').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_e').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_k1').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_k2').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_c').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_b1').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_b2').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_b3').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_b5').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_b6').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_b9').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('vitamin_b12').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('fibres').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('polyols').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('organic_acids').AsFloat, FloatFormatSettings) + ',';
    SQL := SQL + QuotedStr(SQLQuery_MealItems.FieldByName('unit_name').AsString) + ',';
    SQL := SQL + FloatToStr(SQLQuery_MealItems.FieldByName('unit_quantity').AsFloat, FloatFormatSettings);
    SQL := SQL + '),';
    SQLQuery_MealItems.Next;
  end;
  SQL := Copy(SQL, 1, SQL.Length - 1); // Remove the last ','
  SQLQuery_MealItems.Close;
  SQLQuery_MealItems.Clear;
  SQLQuery_MealItems.SQL.Text := SQL;
  SQLQuery_MealItems.ExecSQL;
  Form_Main.SQLTransaction1.Commit;
  ModalResult:=mrOK;
  //Close;
end;

procedure TForm_Meal.ComboBox_MealTypesChange(Sender: TObject);
begin
  UpdateControls();
end;

procedure TForm_Meal.DataSource_MealItemsDataChange(Sender: TObject; Field: TField);
begin
  UpdateControls();
end;

procedure TForm_Meal.DataSource_MealItemsStateChange(Sender: TObject);
var
  S: string;
begin
  case DataSource_MealItems.State of
    dsInactive: S := 'Inactive';
    dsBrowse: S := 'Browse';
    dsEdit: S := 'Edit';
    dsInsert: S := 'Insert';
    dsSetKey: S := 'SetKey';
    dsCalcFields: S := 'CalcFields';
  end;
  //Form_Main.StatusBar1.Panels[0].Text := S;
end;

procedure TForm_Meal.DBGrid_FoodListCellClick(Column: TColumn);
begin
  SelectedMealItemNo := SQLQuery_MealItems.FieldByName('meal_item_no').AsInteger;
  SelectedFoodNo := SQLQuery_MealItems.FieldByName('food_no').AsInteger;
end;

procedure TForm_Meal.DBGrid_FoodListEditingDone(Sender: TObject);
begin
  MealItemQuantityChange(SQLQuery_MealItems.FieldByName('food_no').AsInteger,
    SQLQuery_MealItems.FieldByName('quantity').AsFloat);
end;

procedure TForm_Meal.FormShow(Sender: TObject);
begin
  if Form_Main.CurrentMeal.FMealNo = -1 then
  begin
    { --------------- }
    { Create new meal }
    { --------------- }
    DateTimePicker_CreationDate.DateTime := Form_Main.Calendar1.DateTime;
    DateTimePicker_CreationTime.DateTime := Now;
    SetMealTypesComboList(0);

    SQLQuery_MealItems.Close;
    SQLQuery_MealItems.SQL.Text :=
      'SELECT * FROM meal_item where meal_no = ' + IntToStr(Form_Main.CurrentMeal.FMealNo);
    SQLQuery_MealItems.Open;

    Label_TotalProtein.Caption := FormatFloat('0.00', 0);
    Label_TotalCarbohydrate.Caption := FormatFloat('0.00', 0);
    Label_TotalSugar.Caption := FormatFloat('0.00', 0);
    Label_TotalFat.Caption := FormatFloat('0.00', 0);
    Label_TotalCholesterol.Caption := FormatFloat('0.00', 0);
    Label_TotalSalt.Caption := FormatFloat('0.00', 0);
    Label_TotalEnergy.Caption := FormatFloat('0.00', 0);
  end
  else
  begin
    { ------------------ }
    { Edit existing meal }
    { ------------------ }
    DateTimePicker_CreationDate.DateTime :=
      StrToDate(Form_Main.CurrentMeal.FCreationDate, 'yyyy-mm-dd', '-');
    DateTimePicker_CreationTime.DateTime :=
      StrToTime(Form_Main.CurrentMeal.FCreationTime);
    SetMealTypesComboList(Form_Main.CurrentMeal.FMealTypeNo);

    SQLQuery_MealItems.Close;
    SQLQuery_MealItems.SQL.Clear;
    SQLQuery_MealItems.SQL.Text :=
      'SELECT * FROM meal_item WHERE meal_no = ' + IntToStr(Form_Main.CurrentMeal.FMealNo);
    SQLQuery_MealItems.Open;

    UpdateTotalNutrients(); // Update total nutrients for this meal
  end;
  UpdateControls();
end;

end.
