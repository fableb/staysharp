# StaySharp

![](website_demo/pictures/staysharp_en_logo.png)

### Description

StaySharp is a fitness application that helps you follow key indicators about your health.

You can

* add and follow your daily fitness activities
* add and follow you daily meal intakes
* add foods to the provided food database
* have statistics about your daily, weekly, monthly food intakes
* have statistics about your daily, weekly and monthly fitness activities
* create and follow several accounts. So coaches can follow their students activities
* ...

StaySharp is available on GNU/Linux and Microsoft Windows. Mac OS platform is in test.

Current supported languages are

* English
* French.

### Author

Fabrice LEBEL, fabrice.lebel.pro@outlook.com, [LinkedIn](https://www.linkedin.com/in/fabrice-lebel-b850674?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BbqGekl60S9W3uZDa6jyWkg%3D%3D)

### Repo

https://bitbucket.org/fableb/staysharp