#!/bin/bash

SRC_DIR="../../src/gui"

# Create build name
BUILD_VERSION="0-8-11"
BUILD_NAME=staysharp-$BUILD_VERSION\_$(date '+%Y%m%d_%H%M%S')
echo $BUILD_NAME

# Create build directories
$(mkdir $BUILD_NAME)
$(mkdir $BUILD_NAME/languages)

# Copy files
$(cp $SRC_DIR/staysharp $BUILD_NAME/)
$(cp $SRC_DIR/staysharp.sqlite $BUILD_NAME/)
$(cp $SRC_DIR/staysharp.png $BUILD_NAME/)
$(cp $SRC_DIR/staysharp.ico $BUILD_NAME/)
$(cp $SRC_DIR/libsqlite3.so $BUILD_NAME/)
$(cp $SRC_DIR/postinstall.sh $BUILD_NAME/)
$(cp $SRC_DIR/languages/staysharp.fr_fr.mo $BUILD_NAME/languages/)

# Compress build directory into .tgz file
$(tar -zcvf "$BUILD_NAME.tar.gz" "$BUILD_NAME")
